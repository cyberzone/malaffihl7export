﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MalaffiHL7Export.DAL;
using System.Data;

namespace MalaffiHL7Export.BAL
{
    public class CommonBAL
    {

        dbOperation objDB = new dbOperation();
        public DataSet fnGetFieldValue(string FieldNames, string TableName, string Criteria, string OrderBy)
        {
            DataSet DS = new DataSet();
            string strSQL;

            if (OrderBy != "")
            {
                strSQL = " SELECT " + FieldNames + "   FROM " + TableName + " WHERE " + Criteria + " ORDER BY " + OrderBy;
            }
            else
            {
                strSQL = " SELECT " + FieldNames + "   FROM " + TableName + " WHERE " + Criteria;
            }


            DS = objDB.ExecuteReader(strSQL);

            return DS;
        }

        public void fnInsertTableData(string FieldName, string Values, string TableName)
        {

            string strSQL;

            strSQL = " INSERT INTO " + TableName + "(" + FieldName + " ) VALUES( " + Values + ")";


            objDB.ExecuteNonQuery(strSQL);


        }


        public void fnUpdateTableData(string FieldNameWithValues, string TableName, string Criteria)
        {

            string strSQL;

            strSQL = " UPDATE   " + TableName + " SET " + FieldNameWithValues + " WHERE " + Criteria;


            objDB.ExecuteNonQuery(strSQL);


        }


        public void fnDeleteTableData(string TableName, string Criteria)
        {

            string strSQL;

            strSQL = " DELETE  FROM " + TableName + " WHERE " + Criteria;


            objDB.ExecuteNonQuery(strSQL);


        }

        public string fnGetDate(String formatstring)
        {
            DateTime dtDate;
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT getdate() ";

            DS = objDB.ExecuteReader(strSQL);

            dtDate = Convert.ToDateTime(DS.Tables[0].Rows[0][0]);


            return Convert.ToString(dtDate.ToString(formatstring));
        }

        public Int32 fnGetDateDiff(String Date1, string Date2)
        {
            Int32 intDateDiff;
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT dbo.GetDateDiff ('" + Date1 + "','" + Date2 + "')";

            DS = objDB.ExecuteReader(strSQL);

            intDateDiff = Convert.ToInt32(DS.Tables[0].Rows[0][0]);


            return intDateDiff;
        }

        public string fnGetDateddMMyyyy()
        {
            string strDate;
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " select convert(varchar,getdate(),103) ";

            DS = objDB.ExecuteReader(strSQL);

            strDate = Convert.ToString(DS.Tables[0].Rows[0][0]);


            return strDate;
        }

        public DataSet HoursGet(string v24Hours)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("24Hours", v24Hours);
            DS = objDB.ExecuteReader("HMS_SP_HoursGet", Param);

            return DS;



        }

        public DataSet  HL7MessageStatusVisitWise(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_HL7MessageStatusVisitWise", Param);



            return DS;

        }

        public DataSet MinutesGet(string Interval)
        {
            DataSet DS = new DataSet();
            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Interval", Interval);
            DS = objDB.ExecuteReader("HMS_SP_MinutesGet", Param);

            return DS;
        }

         

    }
}
