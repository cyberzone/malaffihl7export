﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MalaffiHL7Export.DAL;
using System.Data;

namespace MalaffiHL7Export.BAL
{
   public  class PhyCommonBAL
    {
        dbOperationPhy objDB = new dbOperationPhy();

        public DataSet CustomerGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("INV_SP_CustomerGet", Param);



            return DS;

        }

        public DataSet DoctorGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("INV_SP_DoctorGet", Param);



            return DS;

        }
        public DataSet ScreenCustomizationGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("INV_SP_ScreenCustomizationGet", Param);



            return DS;

        }


        public string CompanyBalanceGet(string CompanyId)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = "Select sum(BalanceAmount) AS TotalBalanceAmount  from IvCustBalHist where CustomerCode='" + CompanyId + "'";

            DS = objDB.ExecuteReader(strSQL);

            string strBalance = "0";

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("TotalBalanceAmount") == false && DS.Tables[0].Rows[0]["TotalBalanceAmount"].ToString() != "")
                {
                    strBalance = DS.Tables[0].Rows[0]["TotalBalanceAmount"].ToString();
                }
            }

            return strBalance;

        }

        public string InitialNoGet()
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = "SELECT   CONVERT(VARCHAR,ISNULL(IvReceipt,0)+ 1) as ReceiptNo	FROM GeneralConfig";

            DS = objDB.ExecuteReader(strSQL);

            string strReceiptNo = "0";

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("ReceiptNo") == false && DS.Tables[0].Rows[0]["ReceiptNo"].ToString() != "")
                {
                    strReceiptNo = DS.Tables[0].Rows[0]["ReceiptNo"].ToString();
                }
            }

            return strReceiptNo;

        }

        public DataSet UsersGet(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = "SELECT *  FROM [USER] WHERE  " + Criteria + " ORDER BY Name";

            DS = objDB.ExecuteReader(strSQL);


            return DS;

        }

        public DataSet BranchGet(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = "SELECT *  FROM Branch_Master WHERE  " + Criteria;

            DS = objDB.ExecuteReader(strSQL);


            return DS;

        }

        public DataSet fnGetFieldValue(string FieldNames, string TableName, string Criteria, string OrderBy)
        {
            DataSet DS = new DataSet();
            string strSQL;

            if (OrderBy != "")
            {
                strSQL = " SELECT " + FieldNames + "   FROM " + TableName + " WHERE " + Criteria + " ORDER BY " + OrderBy;
            }
            else
            {
                strSQL = " SELECT " + FieldNames + "   FROM " + TableName + " WHERE " + Criteria;
            }


            DS = objDB.ExecuteReader(strSQL);

            return DS;
        }

        public void fnInsertTableData(string FieldName, string Values, string TableName)
        {

            string strSQL;

            strSQL = " INSERT INTO " + TableName + "(" + FieldName + " ) VALUES( " + Values + ")";


            objDB.ExecuteNonQuery(strSQL);


        }


        public void fnUpdateTableData(string FieldNameWithValues, string TableName, string Criteria)
        {

            string strSQL;

            strSQL = " UPDATE   " + TableName + " SET " + FieldNameWithValues + " WHERE " + Criteria;


            objDB.ExecuteNonQuery(strSQL);


        }


        public void fnDeleteTableData(string TableName, string Criteria)
        {

            string strSQL;

            strSQL = " DELETE  FROM " + TableName + " WHERE " + Criteria;


            objDB.ExecuteNonQuery(strSQL);


        }

        public string fnGetDate(String formatstring)
        {
            DateTime dtDate;
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT getdate() ";

            DS = objDB.ExecuteReader(strSQL);

            dtDate = Convert.ToDateTime(DS.Tables[0].Rows[0][0]);


            return Convert.ToString(dtDate.ToString(formatstring));
        }

        public Int32 fnGetDateDiff(String Date1, string Date2)
        {
            Int32 intDateDiff;
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " SELECT dbo.GetDateDiff ('" + Date1 + "','" + Date2 + "')";

            DS = objDB.ExecuteReader(strSQL);

            intDateDiff = Convert.ToInt32(DS.Tables[0].Rows[0][0]);


            return intDateDiff;
        }

        public string fnGetDateddMMyyyy()
        {
            string strDate;
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = " select convert(varchar,getdate(),103) ";

            DS = objDB.ExecuteReader(strSQL);

            strDate = Convert.ToString(DS.Tables[0].Rows[0][0]);


            return strDate;
        }


        public Boolean fnCheckduplicate(string TableName, string Criteria)
        {
            DataSet DS = new DataSet();

            string strSQL;

            strSQL = " SELECT *  FROM " + TableName + " WHERE " + Criteria;
            DS = objDB.ExecuteReader(strSQL);

            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        public DataSet PatientMasterGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("INV_SP_PatientMasterGet", Param);



            return DS;

        }



        public DataSet PatientVisitGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("INV_SP_HMSPatientVisitGet", Param);



            return DS;

        }


        public DataSet EMRPTDiagnosisGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("INV_SP_EMRPTDiagnosisGet", Param);



            return DS;

        }




        public DataSet EMRPTPharmacyGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("INV_SP_EMRPTPharmacyGet", Param);



            return DS;

        }


        public DataSet PBMWaitingListGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("INV_SP_PBMWaitingListGet", Param);



            return DS;

        }

        public DataSet HMSStaffMaster(string Criteria)
        {
            DataSet DS = new DataSet();
            string strSQL;

            strSQL = "select *,(HSFM_FNAME +' '+ ISNULL(HSFM_MNAME,' ') +' '+ ISNULL(HSFM_LNAME,' ')) as FullName FROM Hospital.dbo.HMS_STAFF_MASTER WHERE  " + Criteria;

            DS = objDB.ExecuteReader(strSQL);


            return DS;

        }

        public DataSet HMSStaffPhotoGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("INV_SP_HMSStaffPhotoGet", Param);



            return DS;

        }

        public DataSet EclaimXMLDataMasterGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("INV_SP_EclaimXMLDataMasterGet", Param);



            return DS;

        }

        public DataSet EMRPTMasterGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("INV_SP_EMRPTMasterGet", Param);



            return DS;

        }


        public DataSet EMRPTVitalsGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("INV_SP_EMRPTVitalsGet", Param);



            return DS;

        }

        public DataSet AuthorizationCredentialGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("HMS_SP_AuthorizationCredentialGet", Param);



            return DS;

        }

        public DataSet PharmacyDrugListGet(string Criteria, string SearchType)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            Param.Add("SearchType", SearchType);

            DS = objDB.ExecuteReader("INV_SP_PharmacyDrugListGet", Param);



            return DS;

        }

        public DataSet EMRPharmacyDrugFavoritesGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);

            DS = objDB.ExecuteReader("INV_SP_EMRPharmacyDrugFavoritesGet", Param);



            return DS;

        }


        public DataSet PharmacyIvItemGet(string Criteria, string SearchType)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            Param.Add("SearchType", SearchType);

            DS = objDB.ExecuteReader("INV_SP_PharmacyIvItemGet", Param);



            return DS;

        }




        public DataSet EMRPharmacyIvItemFavoritesGet(string Criteria)
        {
            DataSet DS = new DataSet();

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("Criteria", Criteria);
            DS = objDB.ExecuteReader("INV_SP_EMRPharmacyIvItemFavoritesGet", Param);



            return DS;

        }

    }
}
