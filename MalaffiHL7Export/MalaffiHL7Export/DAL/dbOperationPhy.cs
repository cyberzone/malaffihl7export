﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace MalaffiHL7Export.DAL
{
   public  class dbOperationPhy
    {

        public DataSet ExecuteReader(string Procedure, IDictionary<string, string> Param)
        {

            DataSet DS = new DataSet();

            SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStrPhy"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = Procedure;
            cmd.CommandTimeout = 864000;
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (KeyValuePair<string, string> _entry in Param)
            {
                if (_entry.Value != "")
                {
                    cmd.Parameters.AddWithValue("@" + _entry.Key, _entry.Value);

                }
                else
                {
                    cmd.Parameters.AddWithValue("@" + _entry.Key, DBNull.Value);
                }

            }
            cmd.ExecuteNonQuery();


            SqlDataAdapter ADPT = new SqlDataAdapter(cmd);
            ADPT.Fill(DS);

            cmd.Dispose();
            ADPT.Dispose();
            con.Close();

            return DS;



        }

        public DataSet ExecuteReader(string SQLQuery)
        {
            //string strSQL;

            //strSQL = " SELECT HCM_PAYERID,HCM_NAME,HCM_BILL_CODE,HCM_REM_RCPTN,HCM_PAYERID from HMS_COMPANY_MASTER WHERE " + Criteria + " ORDER BY HCM_NAME";

            SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStrPhy"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter ADPT = new SqlDataAdapter();
            DataSet DS = new DataSet();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = SQLQuery;
            cmd.CommandTimeout = 864000;
            cmd.ExecuteNonQuery();


            ADPT.SelectCommand = cmd;
            ADPT.Fill(DS);
            cmd.Dispose();
            ADPT.Dispose();
            con.Close();
            return DS;


        }

        public string ExecuteNonQuery(string SQLQuery)
        {
            //string strSQL;

            //strSQL = " SELECT HCM_PAYERID,HCM_NAME,HCM_BILL_CODE,HCM_REM_RCPTN,HCM_PAYERID from HMS_COMPANY_MASTER WHERE " + Criteria + " ORDER BY HCM_NAME";

            SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStrPhy"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand();
            DataSet DS = new DataSet();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandTimeout = 864000;
            cmd.CommandText = SQLQuery;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();

            return "";


        }


        public string ExecuteNonQuery(string Procedure, IDictionary<string, string> Param)
        {
            SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStrPhy"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = Procedure;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 864000;
            foreach (KeyValuePair<string, string> _entry in Param)
            {
                if (_entry.Value != "")
                {
                    cmd.Parameters.AddWithValue("@" + _entry.Key, _entry.Value);

                }
                else
                {
                    cmd.Parameters.AddWithValue("@" + _entry.Key, DBNull.Value);
                }

            }

            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
            return ""; //Convert.ToString(returnValue.Value);


        }

        public string ExecuteNonQueryReturn(string Procedure, IDictionary<string, string> Param)
        {
            SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStrPhy"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = Procedure;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 864000;
            foreach (KeyValuePair<string, string> _entry in Param)
            {
                if (_entry.Value != "")
                {
                    cmd.Parameters.AddWithValue("@" + _entry.Key, _entry.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@" + _entry.Key, DBNull.Value);
                }

            }

            SqlParameter returnValue = new SqlParameter("@ReturnValue", SqlDbType.VarChar, 50);
            returnValue.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(returnValue);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
            string strReturnId;
            strReturnId = Convert.ToString(returnValue.Value);
            return strReturnId;


        }


        public string SQLExecuteNonQueryReturn(params object[] obj)
        {
            SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["ConStrPhy"].ToString());
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = Convert.ToString(obj[0]);
            cmd.CommandType = CommandType.StoredProcedure;

            for (int i = 1; i < obj.Length - 1; i++)
            {
                cmd.Parameters.Add(obj[i]);
            }


            int k = cmd.Parameters.Count;
            for (int i = 0; i < k; i++)
            {
                if (cmd.Parameters[i].Value == "")
                {
                    cmd.Parameters[i].Value = DBNull.Value;
                }
            }
            //cmd.Parameters.Add(new SqlParameter("@EMR_DR_DEP", SqlDbType.Image)).Value = DBNull.Value;


            SqlParameter returnValue = new SqlParameter(Convert.ToString(obj[obj.Length - 1]), SqlDbType.VarChar, 15);
            returnValue.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(returnValue);

            cmd.ExecuteNonQuery();

            string strReturnId;
            strReturnId = Convert.ToString(returnValue.Value);

            con.Close();

            return strReturnId;// Convert.ToString(returnValue.Value);


        }
    }
}
