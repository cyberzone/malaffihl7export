﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Data;
using System.Data.SqlClient;


namespace MalaffiHL7Export.DAL
{
    public class dbOperation
    {

        public static string ConStr = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["ConStr"]);

        public SqlConnection con = new SqlConnection(ConStr);

        public void conopen()
        {
            con.Open();

        }
        public void conclose()
        {


            con.Close();



        }


        public DataSet ExecuteReader(string Procedure, IDictionary<string, string> Param)
        {
            DataSet DS = new DataSet();
            conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = Procedure;
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (KeyValuePair<string, string> _entry in Param)
            {
                if (_entry.Value != "")
                {
                    cmd.Parameters.AddWithValue("@" + _entry.Key, _entry.Value);

                }
                else
                {
                    cmd.Parameters.AddWithValue("@" + _entry.Key, DBNull.Value);
                }

            }
            cmd.ExecuteNonQuery();
            conclose();

            SqlDataAdapter ADPT = new SqlDataAdapter(cmd);
            ADPT.Fill(DS);

            return DS;


        }

        public DataSet ExecuteReader(string SQLQuery)
        {
            //string strSQL;

            //strSQL = " SELECT HCM_PAYERID,HCM_NAME,HCM_BILL_CODE,HCM_REM_RCPTN,HCM_PAYERID from HMS_COMPANY_MASTER WHERE " + Criteria + " ORDER BY HCM_NAME";

            conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet DS = new DataSet();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = SQLQuery;
            cmd.ExecuteNonQuery();
            conclose();

            adpt.SelectCommand = cmd;
            adpt.Fill(DS);

            return DS;


        }

        public DataSet ExecuteQuery(string SQLQuery)
        {
            //string strSQL;

            //strSQL = " SELECT HCM_PAYERID,HCM_NAME,HCM_BILL_CODE,HCM_REM_RCPTN,HCM_PAYERID from HMS_COMPANY_MASTER WHERE " + Criteria + " ORDER BY HCM_NAME";

            conopen();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adpt = new SqlDataAdapter();
            DataSet DS = new DataSet();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = SQLQuery;
            cmd.ExecuteNonQuery();
            conclose();

            adpt.SelectCommand = cmd;
            adpt.Fill(DS);

            return DS;


        }
        public string ExecuteNonQuery(string SQLQuery)
        {
            //string strSQL;

            //strSQL = " SELECT HCM_PAYERID,HCM_NAME,HCM_BILL_CODE,HCM_REM_RCPTN,HCM_PAYERID from HMS_COMPANY_MASTER WHERE " + Criteria + " ORDER BY HCM_NAME";

            conopen();
            SqlCommand cmd = new SqlCommand();
            DataSet DS = new DataSet();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            // cmd.CommandTimeout = 864000;
            cmd.CommandText = SQLQuery;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            conclose();

            return "";


        }

        public string ExecuteNonQuery(string Procedure, IDictionary<string, string> Param)
        {
            conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = Procedure;
            cmd.CommandTimeout = 864000;
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (KeyValuePair<string, string> _entry in Param)
            {
                if (_entry.Value != "")
                {
                    cmd.Parameters.AddWithValue("@" + _entry.Key, _entry.Value);

                }
                else
                {
                    cmd.Parameters.AddWithValue("@" + _entry.Key, DBNull.Value);
                }

            }

            cmd.ExecuteNonQuery();
            conclose();

            return ""; //Convert.ToString(returnValue.Value);


        }

        public string ExecuteNonQueryReturn(string Procedure, IDictionary<string, string> Param)
        {
            conopen();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = Procedure;
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (KeyValuePair<string, string> _entry in Param)
            {
                if (_entry.Value != "")
                {
                    cmd.Parameters.AddWithValue("@" + _entry.Key, _entry.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@" + _entry.Key, DBNull.Value);
                }

            }

            SqlParameter returnValue = new SqlParameter("@RetuenValue", SqlDbType.Int);
            returnValue.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnValue);
            cmd.ExecuteNonQuery();
            conclose();
            string strReturnId;
            strReturnId = Convert.ToString(returnValue.Value);
            return strReturnId;


        }
    }
}
