﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using MalaffiHL7Export.BAL;



namespace MalaffiHL7Export
{
    public partial class HL7MessageDetails : Form
    {
        public HL7MessageDetails()
        {
            InitializeComponent();
        }

        static string BranchName = "", Branch_ProviderID = "";
        System.Threading.Timer timer;

        void TextErrorFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                //strDate = DateTime.Now.ToString("dd/MM/yyyy");
                //string[] arrDate;
                //arrDate = strDate.Split('/');
                // string strFileName =   @"\Log\" + "ErrorLog_" + arrDate[2] + arrDate[1] + ".txt";
                string strFileName = "ErrorLog.txt";

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Name + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindBranchDtls()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "1=1";
            DS = objCom.fnGetFieldValue("*", "HMS_BRANCH_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                BranchName = Convert.ToString(DS.Tables[0].Rows[0]["HBM_NAME"]);
                Branch_ProviderID = Convert.ToString(DS.Tables[0].Rows[0]["HBM_COMM_LIC_NO"]);

            }
        }

        ////void BindTime()
        ////{
        ////    CommonBAL objCom = new CommonBAL();
        ////    DataSet DSHours = new DataSet();

        ////    DSHours = objCom.HoursGet("True");



        ////    drpSTHour.DataSource = DSHours.Tables[0];
        ////    drpSTHour.DisplayMember = "Name";
        ////    drpSTHour.ValueMember = "Code";


        ////    DataSet DSMin = new DataSet();

        ////    DSMin = objCom.MinutesGet("1");





        ////    drpSTMin.DataSource = DSMin.Tables[0];
        ////    drpSTMin.DisplayMember = "Name";
        ////    drpSTMin.ValueMember = "Code";

        ////}

        void BindVisitDetails()
        {
            CommonBAL objCom = new CommonBAL();

            string Criteria = " 1=1 ";
            Criteria += "  AND HPV_STATUS <>'D'   ";

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }


            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= '" + strForStartDate + "'";
            }

            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <= '" + strForToDate + "'";
            }


            if (txtSrcFileNo.Text.Trim() != "")
            {
                Criteria += " AND HPV_PT_ID like '%" + txtSrcFileNo.Text.Trim() + "%'";

            }

            DataSet DS = new DataSet();


            DS = objCom.HL7MessageStatusVisitWise(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvVisitDetails.Visible = true;
                gvVisitDetails.DataSource = DS.Tables[0];

                gvVisitDetails.Width = this.Width - 50;

                gvVisitDetails.Columns[2].Width = 150;
                gvVisitDetails.Columns[3].Width = 150;
            }

            else
            {
                gvVisitDetails.Visible = false;
            }
        }

        Boolean CheckA03Available(string VISIT_ID)
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " HMHM_MESSAGE_TYPE='ADT' AND HMHM_MESSAGE_CODE='ADT-A03' AND HMHM_VISIT_ID = " + VISIT_ID;
            DS = objCom.fnGetFieldValue("top 1 HMHM_VISIT_ID", "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        void HL7MessageProcess_ADT_A03()
        {

            try
            {

                CommonBAL objCom = new CommonBAL();
                DataSet DS = new DataSet();

                string A03_UploadAfterHour = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["A03_UPLOAD_TIME_PERIOD"]);
                string A03_UploadStartPeriod = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["A03_UPLOAD_TIME_START_PERIOD"]);


                if (A03_UploadAfterHour == "")
                {
                    A03_UploadAfterHour = "1440";
                }

                if (A03_UploadStartPeriod == "")
                {
                    A03_UploadStartPeriod = "4320";
                }


                string Criteria = "HPV_EMR_STATUS='Y' AND HPV_EMR_ID IS NOT NULL AND HPV_EMR_ID <> '' ";
                Criteria += " AND DATEDIFF(MI,CONVERT(DATETIME,isnull(HPV_DATE,getdate()),101),CONVERT(DATETIME,GETDATE(),101)) > " + A03_UploadAfterHour;
                Criteria += " AND DATEDIFF(MI,CONVERT(DATETIME,isnull(HPV_DATE,getdate()),101),CONVERT(DATETIME,GETDATE(),101)) < " + A03_UploadStartPeriod;



                DS = objCom.fnGetFieldValue("TOP 1000 HPV_SEQNO,HPV_PT_ID,HPV_EMR_ID,HPV_BRANCH_ID", "HMS_PATIENT_VISIT", Criteria, "");


                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (CheckA03Available(Convert.ToString(DR["HPV_SEQNO"])) == true)
                    {
                        goto LoopEnd;
                    }

                    HL7MessageGenerator objHL7Mess = new HL7MessageGenerator();
                    objHL7Mess.PT_ID = Convert.ToString(DR["HPV_PT_ID"]);
                    objHL7Mess.VISIT_ID = Convert.ToString(DR["HPV_SEQNO"]);
                    objHL7Mess.EMR_ID = Convert.ToString(DR["HPV_EMR_ID"]);
                    objHL7Mess.BRANCH_ID = Convert.ToString(DR["HPV_BRANCH_ID"]);
                    objHL7Mess.BRANCH_NAME = BranchName;
                    objHL7Mess.PROVIDER_ID = Branch_ProviderID;
                    objHL7Mess.FileDescription = "";
                    objHL7Mess.UserID = "HL7Export";

                    DataSet DSMalaffi = new DataSet();
                    Criteria = " HMHM_UPLOAD_STATUS ='PENDING' ";
                    Criteria += " AND HMHM_BRANCH_ID='" + Convert.ToString(DR["HPV_BRANCH_ID"]) + "' AND HMHM_PT_ID='" + Convert.ToString(DR["HPV_PT_ID"]) + "' AND HMHM_EMR_ID='" + Convert.ToString(DR["HPV_EMR_ID"]) + "'";
                    Criteria += " AND HMHM_MESSAGE_TYPE='ADT' and HMHM_MESSAGE_CODE='ADT-A03' ";

                    DSMalaffi = objCom.fnGetFieldValue(" TOP 1 *", "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria, "");


                    if (DSMalaffi.Tables[0].Rows.Count <= 0)
                    {
                        objHL7Mess.MESSAGE_TYPE = "ADT";
                        objHL7Mess.UPLOAD_STATUS = "PENDING";
                        objHL7Mess.MessageCode = "ADT-A03";
                        objHL7Mess.MessageDesc = "Discharge/End Visit";
                        objHL7Mess.MalaffiHL7MessageMasterAdd();
                    }

                LoopEnd: ;
                }


            }
            catch (Exception ex)
            {
                TextErrorFileWriting("-----------------------------------------------");
                TextErrorFileWriting(System.DateTime.Now.ToString() + "      HL7MessageProcess_ADT_A03()");
                TextErrorFileWriting(ex.Message.ToString());

            }
        }

        void UploadPendingHL7Messages()
        {
            string UPLOAD_MESSAGE_TYPE = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["UPLOAD_MESSAGE_TYPE"]);


            string strMessageType = "";
            string[] arrMessageTypes = UPLOAD_MESSAGE_TYPE.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrMessageTypes.Length; intSegType++)
            {
                strMessageType += "'" + arrMessageTypes[intSegType] + "',";
            }
            strMessageType = strMessageType.Substring(0, strMessageType.Length - 1);

            CommonBAL objCom = new CommonBAL();
            DataSet Ds = new DataSet();
            string Criteria = " HMHM_UPLOAD_STATUS ='PENDING'  AND HMHM_PT_ID IS NOT NULL   AND HMHM_MESSAGE_CODE IN (" + strMessageType + ")"; //HMHM_MESSAGE_TYPE
            Ds = objCom.fnGetFieldValue("*", "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria, " HMHM_CREATED_DATE");

            foreach (DataRow DR in Ds.Tables[0].Rows)
            {
                try
                {
                    string BRANCH_ID = Convert.ToString(DR["HMHM_BRANCH_ID"]);
                    string PT_ID = Convert.ToString(DR["HMHM_PT_ID"]);
                    string VISIT_ID = Convert.ToString(DR["HMHM_VISIT_ID"]);
                    string EMR_ID = Convert.ToString(DR["HMHM_EMR_ID"]);

                    string MESSAGE_TYPE = Convert.ToString(DR["HMHM_MESSAGE_TYPE"]);
                    string MESSAGE_CODE = Convert.ToString(DR["HMHM_MESSAGE_CODE"]);

                    string SERV_ID = "", RAD_TEST_REPORT_ID = "", LAB_TEST_REPORT_ID = "", IP_ADMISSION_NO="";

                    if (DR.IsNull("HMHM_RAD_TEST_REPORT_ID") == false && Convert.ToString(DR["HMHM_RAD_TEST_REPORT_ID"]) != "")
                    {
                        RAD_TEST_REPORT_ID = Convert.ToString(DR["HMHM_RAD_TEST_REPORT_ID"]);
                    }

                    if (DR.IsNull("HMHM_LAB_TEST_REPORT_ID") == false && Convert.ToString(DR["HMHM_LAB_TEST_REPORT_ID"]) != "")
                    {
                        LAB_TEST_REPORT_ID = Convert.ToString(DR["HMHM_LAB_TEST_REPORT_ID"]);
                    }


                    if (DR.IsNull("HMHM_SERV_ID") == false && Convert.ToString(DR["HMHM_SERV_ID"]) != "")
                    {
                        SERV_ID = Convert.ToString(DR["HMHM_SERV_ID"]);
                    }


                    if (DR.IsNull("HMHM_IP_ADMISSION_NO") == false && Convert.ToString(DR["HMHM_IP_ADMISSION_NO"]) != "")
                    {
                        IP_ADMISSION_NO = Convert.ToString(DR["HMHM_IP_ADMISSION_NO"]);
                    }



                    HL7MessageGenerator objHL7Mess = new HL7MessageGenerator();
                    objHL7Mess.PT_ID = PT_ID;
                    objHL7Mess.VISIT_ID = VISIT_ID;
                    objHL7Mess.EMR_ID = EMR_ID;

                    objHL7Mess.BRANCH_ID = BRANCH_ID;
                    objHL7Mess.BRANCH_NAME = BranchName;
                    objHL7Mess.PROVIDER_ID = Branch_ProviderID;
                    objHL7Mess.SERV_ID = SERV_ID;
                    objHL7Mess.RAD_TEST_REPORT_ID = RAD_TEST_REPORT_ID;
                    objHL7Mess.LAB_TEST_REPORT_ID = LAB_TEST_REPORT_ID;
                    objHL7Mess.IP_ADMISSION_NO = IP_ADMISSION_NO;
                    switch (MESSAGE_CODE)
                    {
                        case "ADT-A28":
                            objHL7Mess.AddPatientInfoMessageGenerate_ADT_A28();
                            break;

                        case "ADT-A31":
                            objHL7Mess.UpdatePatientInfoMessageGenerate_ADT_A31();
                            break;
                        //case "ADT-A11":
                        ////objHL7Mess.CancelAdmit_visitMessageGenerate_ADT_A11();
                        ////break;

                        case "ADT-A08":
                            objHL7Mess.UpdatePatientEncounterInfoMessageGenerate_ADT_A08();
                            break;


                        case "ADT-A04":
                            objHL7Mess.PTRegisterMessageGenerate_ADT_A04();
                            break;

                        case "ADT-A03":
                            if (EMR_ID != "")
                            {
                                objHL7Mess.DischargeEndVisitMessageGenerate_ADT_A03();
                            }
                            break;
                        case "PPR-PC1":
                            objHL7Mess.AddProblemMessageGenerate_PPR_PC1();
                            break;

                        case "PPR-PC2":
                            objHL7Mess.UpdateProblemMessageGenerate_PPR_PC2();
                            break;

                        case "PPR-PC3":
                            objHL7Mess.DeleteProblemMessageGenerate_PPR_PC3();
                            break;
                        case "OMP-O09":
                            objHL7Mess.PhyOrderMessagGenerate_OMP_O09();
                            break;
                        case "RAS-O17":
                            objHL7Mess.PhyAdministrationMessagGenerate_RAS_O17();
                            break;
                        case "RDS-O13":
                            objHL7Mess.PhyDispenseMessagGenerate_RDS_O13();
                            break;
                        case "ORU-R01":
                            if (RAD_TEST_REPORT_ID != "")
                            {
                                objHL7Mess.RadMessageGenerate_ORU_R01();
                            }

                            if (LAB_TEST_REPORT_ID != "")
                            {
                                objHL7Mess.LabMessageGenerate_ORU_R01();
                            }

                            break;

                        case "ADT-A01":
                            objHL7Mess.Admit_VisitMessageGenerate_ADT_A01();
                            break;
                        case "ADT-A06":
                            objHL7Mess.ChangeOutpatientToInpatientMessageGenerate_ADT_A06();
                            break;



                    }

                }
                catch (Exception ex)
                {
                    TextErrorFileWriting(ex.Message.ToString());
                }





            }

        }

        private void HL7MessageDetails_Load(object sender, EventArgs e)
        {
            BindBranchDtls();
            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");
            // txtSheduleDate.Text = strFromDate.ToString("dd/MM/yyyy");

            //  BindTime();

            //string SheduleStartHour = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["SheduleStartHour"]);
            //string SheduleStartMinutes = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["SheduleStartMinutes"]);



            ////drpSTHour.SelectedValue = SheduleStartHour;
            ////drpSTMin.SelectedValue = SheduleStartMinutes;


            ////int intStHour = Convert.ToInt32(drpSTHour.SelectedValue);
            ////int intStMin = Convert.ToInt32(drpSTMin.SelectedValue);

            ////int intStHour = Convert.ToInt32(SheduleStartHour);
            ////int intStMin = Convert.ToInt32(SheduleStartMinutes);

            ////SetUpTimer(new TimeSpan(intStHour, intStMin, 00));


            BindVisitDetails();




        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            BindVisitDetails();
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            BindBranchDtls();
            BindVisitDetails();

            HL7MessageProcess_ADT_A03();
        }


        private void SetUpTimer(TimeSpan alertTime)
        {
            //////DateTime current = DateTime.Now;
            //////TimeSpan timeToGo = alertTime - current.TimeOfDay;

            //////if (timeToGo < TimeSpan.Zero)
            //////{
            //////    return;//time already passed
            //////}
            //////this.timer = new System.Threading.Timer(x =>
            //////{
            //////    BindBranchDtls();
            //////    HL7MessageProcess_ADT_A03();
            //////    BindVisitDetails();
            //////}, null, timeToGo, Timeout.InfiniteTimeSpan);


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            BindBranchDtls();
            UploadPendingHL7Messages();
            HL7MessageProcess_ADT_A03();

            BindVisitDetails();

        }
    }
}
