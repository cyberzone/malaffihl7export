﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MalaffiHL7Export.DAL;
using System.Net.Sockets;
using System.IO;
using System.Data;
using System.Threading;
using System.Text;
using System.Text.RegularExpressions;

namespace MalaffiHL7Export.BAL
{
    class HL7MessageGenerator
    {
        dbOperation objDB = new dbOperation();
        StringBuilder testHl7MessageToTransmit = new StringBuilder();

        string PHARMACY_DB_NAME = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["PHARMACY_DB_NAME"]);
        string UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI"]);


        public string PT_ID { set; get; }
        public string VISIT_ID { set; get; }
        public string PT_TYPE { set; get; }
        public string COMP_ID { set; get; }
        public string PROVIDER_ID { set; get; }
        public string EMR_ID { set; get; }
        public string FileDescription { set; get; }


        public string BRANCH_ID { set; get; }
        public string BRANCH_NAME { set; get; }

        public string MessageID { set; get; }
        public string MessageCode { set; get; }
        public string MessageDesc { set; get; }
        public string HL7FileName { set; get; }
        public string MESSAGE_TYPE { set; get; }
        public string RETURN_MESSAGE { set; get; }
        public string UPLOAD_STATUS { set; get; }

        public string UserID { set; get; }

        public string LAB_TEST_REPORT_ID { set; get; }

        public string RAD_TEST_REPORT_ID { set; get; }
        public string RAD_TEST_REPORT_DATE_TIME { set; get; }
        public string IP_ADMISSION_NO { set; get; }


        public string SERV_ID { set; get; }


        #region Property

        string EMR_DATE = "";
        string EMR_START_DATE = "";
        string EMR_START_DATE_TIME = "";

        string EMR_CC = "";

        string EMR_END_DATE = "";
        string EMR_END_DATE_TIME = "";

        string VISIT_START_DATE = "";
        string VISIT_START_DATE_TIME = "";

        string strNewLineChar = "\r";
        string FieldSeparator = "|";
        string EncodingCharacters = @"^~\&";
        string SystemCode = "";  // GlobalValues.Malaffi_SystemCode;  // "SYSTEMCODE";
        string ReceivingApplication = "Rhapsody^MALAFFI";
        string ReceivingFacility = "ADHIE";
        string DateTimeOfMessage = "";

        string RecordedDateTime = "";

        string MessageEvent = "";

        string Security = "|";
        string MessageType { set; get; }
        string MessageControlID = "";// "6863722d-1e80-4375-bcf1-bb061871234";
        string ProcessingID = "P";
        string VersionID = "2.5.1";

        string SetID = "1";

        string PatientID = "";
        string PatientIDAlter = "";
        string PatientName = "";
        string MotherName = "";
        string DOB = "";//19880807
        string Gender = "M";
        string PatientAlias = "";
        string Race = "";
        string PatientAddress = "";
        string CountyCode = "";
        string PTHomePhone = "";
        string PTMobile = "";
        string PrimaryLanguage = "";//"ARA Arabic";
        string MaritalStatus = "";
        string Religion = "";
        string PatientAccountNumber = "";
        string EmiratesID = "";
        string Nationality = "";
        string NationalityCode = "";

        string KinName = "";
        string KinRelationship = "";
        string KinAddress = "";
        string KinPhoneNumber = "";

        string PatientClass = "O";
        string FacilityID = "";
        string FacilityName = "";

        string ReferringDr = "";

        string DR_ID = "";
        string ConsultingDr = "";
        string ConsultingDrName = "";
        string Specialty = "";
        string SpecialityName = "";
        string AdmitSource = "";
        string VIPIndicator = "";
        string VisitNumber = "";
        //string AdmitDateTime = "";

        string ResultInterpreter = "";


        string DiagnosisCode = "";
        string DiagnosisDescription = "";
        string DiagnosisPriority = "";


        string ProceduresCode = "";
        string ProceduresDescription = "";
        string ProcedurePriority = "";


        string InsurancePlanID = "";
        string PayerID = "";
        string InsuranceCompanyName = "";
        string InsdPolicyNumber = "";
        string InsCardNumber = "";

        string Malaffi_UploadFilePath = "";

        string AdmittingDr = "";
        string AdmittingDrName = "";

        string IP_WARD_NO = "";
        string AdmittingDrSpecialty = "";
        string AdmittingDrSpecialtyName = "";

        string ADMISSION_START_DATE = "";
        string ADMISSION_START_DATE_TIME = "";

        string OBSERVATION_CODE = "";  // Modified by Naveen for Vital_Sign



        #endregion

        #region Methods

        void LogFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');


                string strFileName = Malaffi_UploadFilePath + @"\Log\" + "MalaffiLog_" + arrDate[2] + arrDate[1] + arrDate[0] + ".txt";

                if (!Directory.Exists(Malaffi_UploadFilePath + @"\Log\"))
                {
                    Directory.CreateDirectory(Malaffi_UploadFilePath + @"\Log\");
                }

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindMalaffiConfig()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            DS = objCom.fnGetFieldValue("*", "HMS_MALAFFI_CONFIGURATION", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                SystemCode = Convert.ToString(DS.Tables[0].Rows[0]["HMC_ISSUER"]);
                Malaffi_UploadFilePath = Convert.ToString(DS.Tables[0].Rows[0]["HMC_HL7_FILE_PATH"]);

            }
        }

        public HL7MessageGenerator()
        {
            BindMalaffiConfig();
        }

        void MalaffiMessageWriting(string strContent, string Type, string FileName)
        {
            try
            {
                string strFileName = Malaffi_UploadFilePath + Type + @"\" + FileName + ".hl7";

                StreamWriter oWrite;

                if (!Directory.Exists(Malaffi_UploadFilePath))
                {
                    Directory.CreateDirectory(Malaffi_UploadFilePath);
                }

                if (!Directory.Exists(Malaffi_UploadFilePath + Type))
                {
                    Directory.CreateDirectory(Malaffi_UploadFilePath + Type);
                }



                oWrite = File.CreateText(strFileName);



                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        string GetNationalityCode(string strName)
        {
            string strCode = "";
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "HNM_NAME='" + strName + "'";
            DS = objCom.fnGetFieldValue("HNM_MALAFFI_CODE", "HMS_NATIONALITY_MASTER", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                strCode = Convert.ToString(DS.Tables[0].Rows[0]["HNM_MALAFFI_CODE"]);
            }

            return strCode;
        }

        string GetCountryCode(string strName)
        {
            string strCode = "";
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "HCM_NAME='" + strName + "'";
            DS = objCom.fnGetFieldValue("HCM_MALAFFI_CODE", "HMS_COUNTRY_MASTER", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HCM_MALAFFI_CODE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_MALAFFI_CODE"]) != "")
                {
                    strCode = Convert.ToString(DS.Tables[0].Rows[0]["HCM_MALAFFI_CODE"]).Trim();
                }
            }

            return strCode;
        }

        string GetKinCode(string strName)
        {
            string strCode = "";
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "HCM_TYPE='Relationship' and HCM_DESC='" + strName + "'";
            DS = objCom.fnGetFieldValue("HCM_CODE", "HMS_COMMON_MASTERS", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                strCode = Convert.ToString(DS.Tables[0].Rows[0]["HCM_CODE"]);
            }

            return strCode;
        }

        void GetRouteCode(string strPhyRoute, out string MalaffiCode, out string MalaffiName)
        {
            MalaffiCode = "";
            MalaffiName = "";
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "EPR_CODE='" + strPhyRoute + "' AND EPR_ACTIVE=1 ";
            DS = objCom.fnGetFieldValue("EPR_MALAFFI_CODE,EPR_NAME", "EMR_PHY_ROUTE", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                MalaffiCode = Convert.ToString(DS.Tables[0].Rows[0]["EPR_MALAFFI_CODE"]);
                MalaffiName = Convert.ToString(DS.Tables[0].Rows[0]["EPR_NAME"]);
            }


        }

        void GetPatientDetails()
        {
            CommonBAL objCom = new CommonBAL();

            DataSet DS = new DataSet();
            string Criteria = "HPM_PT_ID='" + PT_ID + "'";

            DS = objCom.fnGetFieldValue("TOP 1 *, CONVERT(VARCHAR, HPM_DOB,103) AS HPM_DOBDesc", "HMS_PATIENT_MASTER", Criteria, "");

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                PatientID = Convert.ToString(DR["HPM_PT_ID"]);
                PatientIDAlter = "";

                string PTLastName, PTFirstName, PTMiddleName;

                PTLastName = Convert.ToString(DR["HPM_PT_LNAME"]);
                if (PTLastName.Length > 20)
                {
                    PTLastName = PTLastName.Substring(0, 20);
                }

                PTFirstName = Convert.ToString(DR["HPM_PT_FNAME"]);
                if (PTFirstName.Length > 20)
                {
                    PTFirstName = PTFirstName.Substring(0, 20);
                }

                PTMiddleName = Convert.ToString(DR["HPM_PT_MNAME"]);
                if (PTMiddleName.Length > 20)
                {
                    PTMiddleName = PTMiddleName.Substring(0, 20);
                }



                PatientName = PTLastName + "^" + Convert.ToString(DR["HPM_PT_FNAME"]) + "^" + Convert.ToString(DR["HPM_PT_MNAME"]);
                PT_TYPE = Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_TYPE"]);

                COMP_ID = Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                InsuranceCompanyName = Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);

                MotherName = "";
                DOB = "";//19880807


                string strDOB = Convert.ToString(DR["HPM_DOBDesc"]);


                string[] arrDOB = strDOB.Split('/');
                if (strDOB.Length > 1)
                {
                    DOB = Convert.ToString(arrDOB[2]);
                    DOB += Convert.ToString(arrDOB[1]);
                    DOB += Convert.ToString(arrDOB[0]);
                }

                string strSex = Convert.ToString(DR["HPM_SEX"]);


                switch (strSex)
                {
                    case "Male":
                        Gender = "M";
                        break;
                    case "Female":
                        Gender = "F";
                        break;

                }
                PatientAlias = "";
                Race = "";

                string strCountryCode = GetCountryCode(Convert.ToString(DR["HPM_COUNTRY"]));
                CountyCode = strCountryCode;// "ARE"; //United Arab Emirates

                PatientAddress = Convert.ToString(DR["HPM_ADDR"]).Trim();
                PatientAddress += "^" + Convert.ToString(DR["HPM_AREA"]).Trim();
                PatientAddress += "^" + Convert.ToString(DR["HPM_CITY"]).Trim();

                if (DR.IsNull("HPM_EMIRATES_CODE") == false && Convert.ToString(DR["HPM_EMIRATES_CODE"]) != "")
                {
                    PatientAddress += "^" + Convert.ToString(DR["HPM_EMIRATES_CODE"]).Trim();
                }
                else
                {
                    PatientAddress += "^";
                }

                if (DR.IsNull("HPM_POBOX") == false && Convert.ToString(DR["HPM_POBOX"]) != "")
                {
                    PatientAddress += "^" + Convert.ToString(DR["HPM_POBOX"]).Trim();
                }
                else
                {
                    PatientAddress += "^";
                }


                PatientAddress += "^" + CountyCode;


                if (DR.IsNull("HPM_PHONE1") == false && Convert.ToString(DR["HPM_PHONE1"]) != "")
                {
                    PTHomePhone = Convert.ToString(DR["HPM_PHONE1"]).Trim() + "^^CH";
                }

                if (DR.IsNull("HPM_MOBILE") == false && Convert.ToString(DR["HPM_MOBILE"]) != "")
                {
                    PTMobile = Convert.ToString(DR["HPM_MOBILE"]).Trim() + "^^CC";
                }


                if (DR.IsNull("HPM_MARITAL") == false && Convert.ToString(DR["HPM_MARITAL"]) != "")
                {
                    string strMaritalStatus;
                    strMaritalStatus = Convert.ToString(DR["HPM_MARITAL"]).Trim();

                    switch (strMaritalStatus)
                    {
                        case "Separated":
                            MaritalStatus = "A^Separated^MALAFFI";
                            break;
                        case "Divorced":
                            MaritalStatus = "D^Divorced^MALAFFI";
                            break;
                        case "Married":
                            MaritalStatus = "M^Married^MALAFFI";
                            break;
                        case "Domestic Partner":
                            MaritalStatus = "P^Domestic Partner^MALAFFI";
                            break;
                        case "Single":
                            MaritalStatus = "S^Single^MALAFFI";
                            break;
                        case "Unknown":
                            MaritalStatus = "U^Unknown^MALAFFI";
                            break;
                        case "Widowed":
                            MaritalStatus = "W^Widowed^MALAFFI";
                            break;
                        case "Not Available":
                            MaritalStatus = "NA^Not Available^MALAFFI";
                            break;
                        default:
                            MaritalStatus = "NA^Not Available^MALAFFI";
                            break;

                    }

                }
                else
                {
                    MaritalStatus = "NA^Not Available^MALAFFI";
                }


                if (DR.IsNull("HPM_RELIGION_CODE") == false && Convert.ToString(DR["HPM_RELIGION_CODE"]) != "")
                {
                    Religion = Convert.ToString(DR["HPM_RELIGION_CODE"]).Trim() + "^" + Convert.ToString(DR["HPM_RELIGION"]).Trim() + "^MALAFFI";
                }
                else
                {
                    Religion = "OTH^Other^MALAFFI";


                }

                if (Convert.ToString(DR["HPM_ID_CAPTION"]) == "PassportNo")
                {
                    EmiratesID = "000-0000-0000000-0".Replace("-", "");
                }
                else
                {

                    if (DR.IsNull("HPM_IQAMA_NO") == false && Convert.ToString(DR["HPM_IQAMA_NO"]) != "")
                    {
                        EmiratesID = Convert.ToString(DR["HPM_IQAMA_NO"]).Replace("-", "");
                    }


                }

                if (DR.IsNull("HPM_NATIONALITY") == false && Convert.ToString(DR["HPM_NATIONALITY"]) != "")
                {
                    NationalityCode = GetNationalityCode(Convert.ToString(DR["HPM_NATIONALITY"]).Trim());
                    Nationality = NationalityCode + "^" + Convert.ToString(DR["HPM_NATIONALITY"]).Trim() + "^MALAFFI";
                }

                if (DR.IsNull("HPM_KIN_NAME") == false && Convert.ToString(DR["HPM_KIN_NAME"]) != "")
                {
                    KinName = Convert.ToString(DR["HPM_KIN_NAME"]);
                }


                if (DR.IsNull("HPM_KIN_RELATION") == false && Convert.ToString(DR["HPM_KIN_RELATION"]) != "")
                {
                    string strKinCode = GetKinCode(Convert.ToString(DR["HPM_KIN_RELATION"]));
                    KinRelationship = strKinCode + "^" + Convert.ToString(DR["HPM_KIN_RELATION"]) + "^MALAFFI";
                }


                if (DR.IsNull("HPM_KIN_MOBILE") == false && Convert.ToString(DR["HPM_KIN_MOBILE"]) != "")
                {
                    KinPhoneNumber = Convert.ToString(DR["HPM_KIN_MOBILE"]) + "^^CC";
                }

                if (DR.IsNull("HPM_KNOW_FROM") == false && Convert.ToString(DR["HPM_KNOW_FROM"]) != "")
                {
                    AdmitSource = Convert.ToString(DR["HPM_KNOW_FROM"]);
                }


                if (DR.IsNull("HPM_POLICY_NO") == false && Convert.ToString(DR["HPM_POLICY_NO"]) != "")
                {
                    InsdPolicyNumber = Convert.ToString(DR["HPM_POLICY_NO"]);
                }

                if (DR.IsNull("HPM_ID_NO") == false && Convert.ToString(DR["HPM_ID_NO"]) != "")
                {
                    InsCardNumber = Convert.ToString(DR["HPM_ID_NO"]);
                }
                else
                {
                    InsCardNumber = Convert.ToString(DR["HPM_POLICY_NO"]);
                }

            }


        }

        void GetPatientVisitDtls()
        {
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            string Criteria = "HPV_PT_ID='" + PT_ID + "' ";
            Criteria += " AND HPV_SEQNO='" + VISIT_ID + "' ";

            //string strTotDate = HPV_DATE;
            //string[] arrToDate = strTotDate.Split('/');
            //string strForToDate = "";

            //if (arrToDate.Length > 1)
            //{
            //    strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            //}
            //Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= '" + strForToDate + "'";

            //Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <= '" + strForToDate + "'";


            DS = objCom.fnGetFieldValue(" TOP 1 *", "HMS_PATIENT_VISIT", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("HPV_PATIENT_CLASS") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPV_PATIENT_CLASS"]) != "")
                {

                    PatientClass = Convert.ToString(DS.Tables[0].Rows[0]["HPV_PATIENT_CLASS"]);
                }


                string strRefDrID = "";


                VisitNumber = Convert.ToString(DS.Tables[0].Rows[0]["HPV_SEQNO"]);
                DR_ID = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_ID"]).Trim();

                string DrFirstName, DrLastName, DrMiddleName, LicenseNo, strSpecialityName;
                GetStaffDetails(DR_ID, out   DrFirstName, out DrMiddleName, out   DrLastName, out   LicenseNo, out strSpecialityName);


                if (DrFirstName.Length > 20)
                {
                    DrFirstName = DrFirstName.Substring(0, 15);
                }
                if (DrLastName.Length > 20)
                {
                    DrLastName = DrLastName.Substring(0, 15);
                }


                // if (DrLastName != "")
                //{
                ConsultingDrName = DrLastName + "^" + DrFirstName + "^" + DrMiddleName;  // DrFirstName + "^" + DrLastName;
                ResultInterpreter = DrLastName + "&" + DrFirstName + "&" + DrMiddleName; // DrFirstName + "^" + DrLastName;
                //}
                //else
                //{
                //    ConsultingDrName = DrFirstName + "^" + DrFirstName;
                //    ResultInterpreter = DrFirstName + "&" + DrFirstName;
                //}



                strRefDrID = Convert.ToString(DS.Tables[0].Rows[0]["HPV_REFERRAL_TO"]);
                // SpecialtyName = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DEP_NAME"]);
                //PT_TYPE = Convert.ToString(DS.Tables[0].Rows[0]["HPV_PT_TYPE"]);

                // COMP_ID = Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_ID"]);


                ReferringDr = strRefDrID;
                ConsultingDr = LicenseNo;

                SpecialityName = strSpecialityName;
                Specialty = GetCommnMasterCode(strSpecialityName, "Specialty");




                if (DS.Tables[0].Rows[0].IsNull("HPV_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPV_DATE"]) != "")
                {
                    DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["HPV_DATE"]);

                    string strMonth = dtStartDate.Month.ToString();
                    if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                    string strDay = dtStartDate.Day.ToString();
                    if (strDay.Length == 1) { strDay = "0" + strDay; }

                    string strHour = dtStartDate.Hour.ToString();
                    if (strHour.Length == 1) { strHour = "0" + strHour; }

                    string strMinute = dtStartDate.Minute.ToString();
                    if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                    VISIT_START_DATE = dtStartDate.Year.ToString() + strMonth + strDay;
                    VISIT_START_DATE_TIME = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                }
            }


        }

        void GetIPAdmissionSummary()
        {
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            string Criteria = "IAS_PT_ID='" + PT_ID + "' ";
            Criteria += " AND IAS_ADMISSION_NO='" + IP_ADMISSION_NO + "' ";

            DS = objCom.fnGetFieldValue(" TOP 1 *", "IP_ADMISSION_SUMMARY", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                string IP_WARD_NO = Convert.ToString(DS.Tables[0].Rows[0]["IAS_WARD_NO"]).Trim();
                string ADMISSION_DR_ID = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_ID"]).Trim();

                string DrFirstName, DrLastName, DrMiddleName, LicenseNo, strSpecialityName;
                GetStaffDetails(ADMISSION_DR_ID, out   DrFirstName, out DrMiddleName, out   DrLastName, out   LicenseNo, out strSpecialityName);


                if (DrFirstName.Length > 20)
                {
                    DrFirstName = DrFirstName.Substring(0, 15);
                }
                if (DrLastName.Length > 20)
                {
                    DrLastName = DrLastName.Substring(0, 15);
                }



                AdmittingDrName = DrLastName + "^" + DrFirstName + "^" + DrMiddleName;  // DrFirstName + "^" + DrLastName;



                AdmittingDr = LicenseNo;

                AdmittingDrSpecialtyName = strSpecialityName;
                AdmittingDrSpecialty = GetCommnMasterCode(strSpecialityName, "Specialty");




                if (DS.Tables[0].Rows[0].IsNull("IAS_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_DATE"]) != "")
                {
                    DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["IAS_DATE"]);

                    string strMonth = dtStartDate.Month.ToString();
                    if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                    string strDay = dtStartDate.Day.ToString();
                    if (strDay.Length == 1) { strDay = "0" + strDay; }

                    string strHour = dtStartDate.Hour.ToString();
                    if (strHour.Length == 1) { strHour = "0" + strHour; }

                    string strMinute = dtStartDate.Minute.ToString();
                    if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                    ADMISSION_START_DATE = dtStartDate.Year.ToString() + strMonth + strDay;
                    ADMISSION_START_DATE_TIME = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                }
            }

        }

        void BindInsurance_IN1()
        {
            if (PayerID != "")
            {
                testHl7MessageToTransmit.Append(strNewLineChar);
                testHl7MessageToTransmit.Append("IN1" + FieldSeparator);//  
                testHl7MessageToTransmit.Append(SetID + FieldSeparator);// Set ID
                testHl7MessageToTransmit.Append(InsurancePlanID + FieldSeparator);// Insurance Plan ID
                testHl7MessageToTransmit.Append(PayerID + FieldSeparator);// Insurance Company ID // InsuranceCompanyID
                testHl7MessageToTransmit.Append(InsuranceCompanyName + FieldSeparator);// Insurance Company Name
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Insurance Company Address
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Insurance Company Contact Person
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Insurance Company Phone Number
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Group Number
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Group Name
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Insured Group Emp ID
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Insured Group Emp Name
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Plan Effective Date
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Plan Expiration Date
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Authorization Information
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Plan Type
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Name of Insured
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Insured's Relationship to Patient
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Insured's Date of Birth
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Insured's Address
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Assignment of Benefits
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Coordination of Benefits
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Coordination of Benefits Priority
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Notice of Admission Flag
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Notice of Admission Date
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Report of Eligibility Flag
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Report of Eligibility Date
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Release Information Code
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Pre-admit Cert
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Verification Date/Time
                testHl7MessageToTransmit.Append("" + FieldSeparator);// Verification by
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  Type of Agreement Code
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  Billing Status
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  Lifetime Reserve Days
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  Delay Before LR Day
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  Company Plan Code

                testHl7MessageToTransmit.Append(InsdPolicyNumber + FieldSeparator);//  Policy Number
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  Policy Deductible
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  Policy Amount Limit
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  Policy Limit Days
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  Room Semi-private Rate
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  Room Private Rate
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  Insured's Employment Status
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  Insured's Administrative Sex
                testHl7MessageToTransmit.Append("" + FieldSeparator);//   Insured's Employer Address
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  Verification Status
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  Prior Insurance Plan ID
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  Coverage Type
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  Handicap

                if (InsCardNumber != "")
                {
                    testHl7MessageToTransmit.Append(InsCardNumber + FieldSeparator);//  Insured's ID Number
                }
                else
                {
                    testHl7MessageToTransmit.Append(InsdPolicyNumber + FieldSeparator);//  Insured's ID Number
                }

            }
        }

        void GetCompanyDtls(string CompanyID, out string PayerID)
        {
            PayerID = "";
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            string Criteria = "1=1 AND HCM_PAYERID   NOT LIKE '@%' AND HCM_COMP_ID='" + CompanyID + "'";


            DS = objCom.fnGetFieldValue(" TOP 1 *", "HMS_COMPANY_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                PayerID = Convert.ToString(DS.Tables[0].Rows[0]["HCM_PAYERID"]);

            }
        }

        void GetStaffDetails(string StaffCode, out string DrFirstName, out string DrMiddleName, out string DrLastName, out string LicenseNo, out string strSpecialityName)
        {
            DrFirstName = "";
            DrMiddleName = "";
            DrLastName = "";
            LicenseNo = "";
            strSpecialityName = "";
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            string Criteria = "1=1 AND HSFM_STAFF_ID='" + StaffCode + "'";



            DS = objCom.fnGetFieldValue("*", "HMS_STAFF_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                DrFirstName = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_FNAME"]);
                //DrMiddleName = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MNAME"]);

                if (DS.Tables[0].Rows[0].IsNull("HSFM_LNAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_LNAME"]) != "")
                {
                    DrLastName = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_LNAME"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HSFM_MNAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MNAME"]) != "")
                {
                    DrMiddleName = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MNAME"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HSFM_MOH_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOH_NO"]) != "")
                {
                    LicenseNo = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOH_NO"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSFM_SPECIALITY") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SPECIALITY"]) != "")
                {
                    strSpecialityName = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SPECIALITY"]);
                }
            }


        }

        void GetRefDrfDetails(string StaffCode, out string DrFirstName, out string DrMiddleName, out string DrLastName, out string LicenseNo, out string strSpecialityName)
        {
            DrFirstName = "";
            DrMiddleName = "";
            DrLastName = "";
            LicenseNo = "";
            strSpecialityName = "";
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            string Criteria = "1=1 AND HRDM_REF_ID='" + StaffCode + "'";



            DS = objCom.fnGetFieldValue("*", "HMS_REF_DOCTOR_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                DrFirstName = Convert.ToString(DS.Tables[0].Rows[0]["HRDM_FNAME"]);
                //DrMiddleName = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MNAME"]);

                if (DS.Tables[0].Rows[0].IsNull("HRDM_LNAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HRDM_LNAME"]) != "")
                {
                    DrLastName = Convert.ToString(DS.Tables[0].Rows[0]["HRDM_LNAME"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HRDM_MNAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HRDM_MNAME"]) != "")
                {
                    DrMiddleName = Convert.ToString(DS.Tables[0].Rows[0]["HRDM_MNAME"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HRDM_REF_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["HRDM_REF_ID"]) != "")
                {
                    LicenseNo = Convert.ToString(DS.Tables[0].Rows[0]["HRDM_REF_ID"]);
                }

                //if (DS.Tables[0].Rows[0].IsNull("HSFM_SPECIALITY") == false && Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SPECIALITY"]) != "")
                //{
                //    strSpecialityName = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_SPECIALITY"]);
                //}
            }


        }

        string GetCommnMasterCode(string Name, string Type)
        {
            string strSpecialtyCode = "";
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            string Criteria = " HCM_TYPE='" + Type + "' AND HCM_DESC='" + Name + "'";



            DS = objCom.fnGetFieldValue("*", "HMS_COMMON_MASTERS", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                strSpecialtyCode = Convert.ToString(DS.Tables[0].Rows[0]["HCM_CODE"]);
            }

            return strSpecialtyCode;
        }

        void BindEMRPTMaster()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND EPM_ID = '" + EMR_ID + "'";

            DS = objCom.fnGetFieldValue("*", "EMR_PT_MASTER", Criteria, "EPM_ID");

            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("EPM_CC") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_CC"]) != "")
                {
                    EMR_CC = Convert.ToString(DS.Tables[0].Rows[0]["EPM_CC"]);

                    EMR_CC = RemoveSplChar(Convert.ToString(EMR_CC));


                    EMR_CC = EMR_CC.Replace("\n", string.Empty);
                    EMR_CC = EMR_CC.Replace(Environment.NewLine, string.Empty);
                }


                if (DS.Tables[0].Rows[0].IsNull("EPM_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_DATE"]) != "")
                {
                    DateTime dtEMRate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPM_DATE"]);

                    string strMonth = dtEMRate.Month.ToString();
                    if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                    string strDay = dtEMRate.Day.ToString();
                    if (strDay.Length == 1) { strDay = "0" + strDay; }


                    string strHour = dtEMRate.Hour.ToString();
                    if (strHour.Length == 1) { strHour = "0" + strHour; }

                    string strMinute = dtEMRate.Minute.ToString();
                    if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                    EMR_DATE = dtEMRate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";
                }


                if (DS.Tables[0].Rows[0].IsNull("EPM_START_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_START_DATE"]) != "")
                {
                    DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPM_START_DATE"]);

                    string strMonth = dtStartDate.Month.ToString();
                    if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                    string strDay = dtStartDate.Day.ToString();
                    if (strDay.Length == 1) { strDay = "0" + strDay; }

                    string strHour = dtStartDate.Hour.ToString();
                    if (strHour.Length == 1) { strHour = "0" + strHour; }

                    string strMinute = dtStartDate.Minute.ToString();
                    if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                    EMR_START_DATE = dtStartDate.Year.ToString() + strMonth + strDay;
                    EMR_START_DATE_TIME = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                }

                if (DS.Tables[0].Rows[0].IsNull("EPM_END_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_END_DATE"]) != "")
                {
                    DateTime dtEndDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPM_END_DATE"]);

                    string strMonth = dtEndDate.Month.ToString();
                    if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                    string strDay = dtEndDate.Day.ToString();
                    if (strDay.Length == 1) { strDay = "0" + strDay; }

                    string strHour = dtEndDate.Hour.ToString();
                    if (strHour.Length == 1) { strHour = "0" + strHour; }

                    string strMinute = dtEndDate.Minute.ToString();
                    if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                    EMR_END_DATE = dtEndDate.Year.ToString() + strMonth + strDay;
                    EMR_END_DATE_TIME = dtEndDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                }

            }

        }

        void GetEMRPTLabDetails(string LabCode, out string RequestedDateTime)
        {
            RequestedDateTime = "";

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND EPL_ID = '" + EMR_ID + "' AND EPL_LAB_CODE='" + LabCode + "'";

            DS = objCom.fnGetFieldValue("*", "EMR_PT_LABORATORY", Criteria, "EPL_POSITION");

            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("EPL_CREATED_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPL_CREATED_DATE"]) != "")
                {
                    DateTime dtInvDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPL_CREATED_DATE"]);

                    string strMonth = dtInvDate.Month.ToString();
                    if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                    string strDay = dtInvDate.Day.ToString();
                    if (strDay.Length == 1) { strDay = "0" + strDay; }

                    string strHour = dtInvDate.Hour.ToString();
                    if (strHour.Length == 1) { strHour = "0" + strHour; }

                    string strMinute = dtInvDate.Minute.ToString();
                    if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                    RequestedDateTime = dtInvDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                }
                else
                {
                    RequestedDateTime = EMR_START_DATE_TIME;
                }


            }
            else
            {
                RequestedDateTime = "";
            }
        }

        void BindIPAdmissionSummary()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND IAS_EMR_ID = '" + EMR_ID + "'";

            DS = objCom.fnGetFieldValue("*", "IP_ADMISSION_SUMMARY", Criteria, "IAS_EMR_ID");

            if (DS.Tables[0].Rows.Count > 0)
            {
            }
        }

        void BindEventType(string EventType)
        {
            testHl7MessageToTransmit.Append(strNewLineChar);
            testHl7MessageToTransmit.Append("EVN" + FieldSeparator);
            testHl7MessageToTransmit.Append(EventType + FieldSeparator); // EVN-1	Event Type Code	ID	O
            testHl7MessageToTransmit.Append(RecordedDateTime);       // EVN-2	Recorded Date Time	 TS 	O
        }

        void BindPatientIdentification_PID()
        {
            testHl7MessageToTransmit.Append(strNewLineChar);
            testHl7MessageToTransmit.Append("PID" + FieldSeparator);
            testHl7MessageToTransmit.Append(SetID + FieldSeparator);                                  // PID-1   Set ID
            testHl7MessageToTransmit.Append("" + FieldSeparator);                                     // PID-2	Patient ID
            testHl7MessageToTransmit.Append(PatientID + "^^^&" + SystemCode + FieldSeparator);        // PID-3	Patient Identifier List R
            testHl7MessageToTransmit.Append(PatientIDAlter + FieldSeparator);                         // PID-4   Alternate Patient ID - PID O
            testHl7MessageToTransmit.Append(PatientName + FieldSeparator);          // PID-5	Patient Name R
            testHl7MessageToTransmit.Append(MotherName + FieldSeparator);           // PID-6	Mother's Maiden Name O
            testHl7MessageToTransmit.Append(DOB + FieldSeparator);                                    // PID-7	Date/Time of Birth R
            testHl7MessageToTransmit.Append(Gender + FieldSeparator);                                 // PID-8	Administrative Sex R
            testHl7MessageToTransmit.Append(PatientAlias + FieldSeparator);         // PID-9	Patient Alias  O
            testHl7MessageToTransmit.Append(Race + FieldSeparator);                 // PID-10  Race O
            testHl7MessageToTransmit.Append(PatientAddress + FieldSeparator);       // PID-11  Patient Address O
            testHl7MessageToTransmit.Append(CountyCode + FieldSeparator);           // PID-12  County Code O
            testHl7MessageToTransmit.Append(PTHomePhone + FieldSeparator);             // PID-13  Home Phone Number O
            testHl7MessageToTransmit.Append(PTMobile + FieldSeparator);             // PID-14  Business Phone Number R
            testHl7MessageToTransmit.Append(PrimaryLanguage + FieldSeparator);      // PID-15  Primary Language O
            testHl7MessageToTransmit.Append(MaritalStatus + FieldSeparator);        // PID-16  Marital Status R
            testHl7MessageToTransmit.Append(Religion + FieldSeparator);             // PID-17  Religion  R
            testHl7MessageToTransmit.Append(PatientAccountNumber + FieldSeparator); // PID-18  Patient Account Number O
            testHl7MessageToTransmit.Append(EmiratesID + FieldSeparator);           // PID-19  SSN Number - Patient  R

            testHl7MessageToTransmit.Append("" + FieldSeparator); // PID-20  Driver's License Number Patient O
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PID-21  Mother's Identifier O
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PID-22  Ethnic Group O
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PID-23  Birth Place O
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PID-24  Multiple Birth Indicator O
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PID-25  Birth Order O
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PID-26  Citizenship O
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PID-27  Veterans Military Status O
            testHl7MessageToTransmit.Append(Nationality + FieldSeparator); // PID-28 Nationality  R
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PID-29  Patient Death Date and Time O
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PID-30  Patient Death Indicator O


            testHl7MessageToTransmit.Append("" + FieldSeparator); // PD1-1   Living Dependency
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PD1-2   Living Arrangement
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PD1-3   Patient Primary Facility
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PD1-4   Patient Primary Care Provider Name and ID Number
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PD1-5   Student Indicator
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PD1-6   Handicap
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PD1-7   Living Will Code
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PD1-8   Organ Donor Code
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PD1-9   Separate Bill
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PD1-10  Duplicate Patient
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PD1-11  Publicity Code
            testHl7MessageToTransmit.Append("" + FieldSeparator); // PD1-12  Protection Indicator
        }

        void BindNextofKinOrAssociatedParties()
        {
            if (KinName != "")
            {
                testHl7MessageToTransmit.Append(strNewLineChar);
                testHl7MessageToTransmit.Append("NK1" + FieldSeparator);
                testHl7MessageToTransmit.Append(SetID + FieldSeparator);                               // NK1-1   Set ID
                testHl7MessageToTransmit.Append(KinName + FieldSeparator);          // NK1-2   Name  
                testHl7MessageToTransmit.Append(KinRelationship + FieldSeparator); // NK1-3   Relationship  
                testHl7MessageToTransmit.Append(KinAddress + FieldSeparator);                        // NK1-4   Address  
                testHl7MessageToTransmit.Append(KinPhoneNumber + FieldSeparator); // NK1-5   Phone Number  
                testHl7MessageToTransmit.Append("" + FieldSeparator); // NK1-6   Business Phone Number
                testHl7MessageToTransmit.Append("" + FieldSeparator); // NK1-7   Contact Role
            }
        }

        Boolean CheckA03Available()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " HMHM_MESSAGE_TYPE='ADT' AND HMHM_MESSAGE_CODE='ADT-A03' AND HMHM_VISIT_ID = " + VISIT_ID;
            DS = objCom.fnGetFieldValue("top 1 HMHM_VISIT_ID", "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        void BindPatientVisitAdditionalInformation_PV1()
        {

            //Patient Visit
            testHl7MessageToTransmit.Append(strNewLineChar);
            testHl7MessageToTransmit.Append("PV1" + FieldSeparator);
            testHl7MessageToTransmit.Append("1" + FieldSeparator);          //  PV1-1   Set ID
            testHl7MessageToTransmit.Append(PatientClass == "" ? "O" + FieldSeparator : PatientClass + FieldSeparator); //  PV1-2   Patient Class R

            // Specialty
            //NOTE: For all the messages you have shared PV1-3.3 as ALAINwhich is invalid 
            //      Kindly note that for outpatient cases, there is no need to share PV1-3.3 sub field, you can keep it empty as:

            //testHl7MessageToTransmit.Append(SpecialtyName + "^" + SpecialtyName + "^" + BRANCH_NAME + "^" + PROVIDER_ID + "&" + SystemCode + "-DOHID" + FieldSeparator);//  PV1-3   Assigned Patient Location R


            string PatientLocation = SpecialityName;
            if (PatientLocation.Length > 20)
            {
                PatientLocation = PatientLocation.Substring(0, 20);
            }


            testHl7MessageToTransmit.Append(PatientLocation + "^" + PatientLocation + "^^" + PROVIDER_ID + "&" + SystemCode + "-DOHID" + FieldSeparator);//  PV1-3   Assigned Patient Location R

            if (MessageEvent == "ADT-A06")
            {
                testHl7MessageToTransmit.Append("" + FieldSeparator);           //  PV1-4   Admission Type O
                testHl7MessageToTransmit.Append("" + FieldSeparator);           //  PV1-5   Pre-admit Number O
                testHl7MessageToTransmit.Append("" + FieldSeparator);           //  PV1-6   Prior Patient Location O
            }
            else
            {
                testHl7MessageToTransmit.Append("" + FieldSeparator);           //  PV1-4   Admission Type O
                testHl7MessageToTransmit.Append("" + FieldSeparator);           //  PV1-5   Pre-admit Number O
                testHl7MessageToTransmit.Append("" + FieldSeparator);           //  PV1-6   Prior Patient Location O
            }

            if (ConsultingDr != "")
            {
                testHl7MessageToTransmit.Append(ConsultingDr + "^" + ConsultingDrName + "^^^^^&" + SystemCode + "-DOHID" + FieldSeparator);//  PV1-7   Attending Doctor  R
            }
            else
            {
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  PV1-7   Attending Doctor  R
            }

            ////if (ReferringDr != "")
            ////{
            ////    testHl7MessageToTransmit.Append(ReferringDr + "&" + SystemCode + "-DOHID" + FieldSeparator);//  PV1-8   Referring Doctor
            ////}
            ////else
            ////{
            testHl7MessageToTransmit.Append("" + FieldSeparator);//  PV1-8   Referring Doctor
            ////  }


            if (ConsultingDr != "")
            {
                testHl7MessageToTransmit.Append(ConsultingDr + "^" + ConsultingDrName + "^^^^^&" + SystemCode + "-DOHID" + FieldSeparator);//  PV1-9   Consulting Doctor
            }
            else
            {
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  PV1-9   Consulting Doctor
            }

            testHl7MessageToTransmit.Append(Specialty == "" ? "20" + FieldSeparator : Specialty + FieldSeparator);   // PV1-10  Hospital Service   R
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-11    Temporary Location O
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-12    Pre-admit Test Indicator 0
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-13    Re-admission Indicator 0
            testHl7MessageToTransmit.Append(AdmitSource == "" ? "9" + FieldSeparator : AdmitSource + FieldSeparator);// PV1-14 Admit Source R

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-15    Ambulatory Status O
            testHl7MessageToTransmit.Append(VIPIndicator + FieldSeparator);// PV1-16    VIP Indicator 0
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-17    Admitting Doctor 0
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-18    Patient Type 0

            //if (ConsultingDr != "")
            // {
            testHl7MessageToTransmit.Append(VisitNumber + "^^^&" + SystemCode + FieldSeparator);// PV1-19   Visit Number
            //}
            //else
            //{
            //    testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-19   Visit Number
            //}

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-20    Financial Class
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-21    Charge Price Indicator
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-22    Courtesy Code
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-23    Credit Rating
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-24     Contract Code
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-25     Contract Effective Date

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-26     Contract Amount
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-27     Contract Period
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-28     Interest Code

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-29     Transfer to Bad Debt Code
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-30     Transfer to Bad Debt Date
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-31     Bad Debt Agency Code

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-32     Bad Debt Transfer Amount
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-33     Bad Debt Recovery Amount
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-34     Delete Account Indicator
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-35     Delete Account Date

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-36     Discharge Disposition  R

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-37     Discharged to Location
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-38     Diet Type
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-39     Servicing Facility
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-40     Bed Status
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-41     Account Status
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-42     Pending Location
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-43     Prior Temporary Location

            testHl7MessageToTransmit.Append(VISIT_START_DATE_TIME + FieldSeparator);// PV1-44      Admit Date/Time  R //EMR_START_DATE_TIME

            if (MessageEvent != "ADT-A04" && MessageEvent != "ADT-A28" && MessageEvent != "ADT-A08")
            {
                testHl7MessageToTransmit.Append(EMR_END_DATE_TIME); // PV1-45	Discharge Date/Time	 TS 	O
            }
            else
            {
                if (MessageEvent == "ADT-A08")
                {
                    if (CheckA03Available() == true)
                    {
                        testHl7MessageToTransmit.Append(EMR_END_DATE_TIME); // PV1-45	Discharge Date/Time	 TS 	O
                    }
                }
            }

        }

        void BindPatientVisitAdditionalInformation_PV2()
        {

            //Patient Visit
            testHl7MessageToTransmit.Append(strNewLineChar);
            testHl7MessageToTransmit.Append("PV2" + FieldSeparator);
            testHl7MessageToTransmit.Append("" + FieldSeparator);          //  PV2-1	Prior Pending Location O
            testHl7MessageToTransmit.Append("" + FieldSeparator);          //  PV2-2	Accommodation Code 0
            testHl7MessageToTransmit.Append("^" + EMR_CC);          //  PV2-3	Admit Reason R
        }

        void BindPatientLabVisitAdditionalInformation_PV1(string RefDrSpecialty, string RefDrSpecialityName, string AttendingDrID, string AttendingDrName, string ConSulDrID, string ConsulDrName, string VistID, string AdmitDate)
        {

            //Patient Visit
            testHl7MessageToTransmit.Append(strNewLineChar);
            testHl7MessageToTransmit.Append("PV1" + FieldSeparator);
            testHl7MessageToTransmit.Append("1" + FieldSeparator);          //  PV1-1   Set ID
            testHl7MessageToTransmit.Append(PatientClass == "" ? "O" + FieldSeparator : PatientClass + FieldSeparator); //  PV1-2   Patient Class R

            // Specialty

            // testHl7MessageToTransmit.Append(SpecialtyName + "^" + SpecialtyName + "^" + BRANCH_NAME + "^" + PROVIDER_ID + "&" + SystemCode + "-DOHID" + FieldSeparator);//  PV1-3   Assigned Patient Location R
            testHl7MessageToTransmit.Append(RefDrSpecialityName + "^" + RefDrSpecialityName + "^^" + PROVIDER_ID + "&" + SystemCode + "-DOHID" + FieldSeparator);//  PV1-3   Assigned Patient Location R

            testHl7MessageToTransmit.Append("" + FieldSeparator);           //  PV1-4   Admission Type O
            testHl7MessageToTransmit.Append("" + FieldSeparator);           //  PV1-5   Pre-admit Number O
            testHl7MessageToTransmit.Append("" + FieldSeparator);           //  PV1-6   Prior Patient Location O

            // GDxxxx^Test_FamilyName^Test_GivenName^Test_MiddleName^^Dr.^^^&SPLMCARE-DOHID

            if (ConsultingDr != "")
            {
                testHl7MessageToTransmit.Append(AttendingDrID + "^" + AttendingDrName + "^^^^^&" + SystemCode + "-DOHID" + FieldSeparator);//  PV1-7   Attending Doctor  R
            }
            else
            {
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  PV1-7   Attending Doctor  R
            }

            ////if (ReferringDr != "")
            ////{
            ////    testHl7MessageToTransmit.Append(ReferringDr + "&" + SystemCode + "-DOHID" + FieldSeparator);//  PV1-8   Referring Doctor
            ////}
            ////else
            ////{
            testHl7MessageToTransmit.Append("" + FieldSeparator);//  PV1-8   Referring Doctor
            //// }


            if (ConsultingDr != "")
            {
                testHl7MessageToTransmit.Append(ConSulDrID + "^" + ConsulDrName + "^^^^^&" + SystemCode + "-DOHID" + FieldSeparator);//  PV1-9   Consulting Doctor
            }
            else
            {
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  PV1-9   Consulting Doctor
            }




            testHl7MessageToTransmit.Append(RefDrSpecialty == "" ? "20" + FieldSeparator : RefDrSpecialty + FieldSeparator);   // PV1-10  Hospital Service   R
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-11    Temporary Location O
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-12    Pre-admit Test Indicator 0
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-13    Re-admission Indicator 0
            testHl7MessageToTransmit.Append(AdmitSource == "" ? "9" + FieldSeparator : AdmitSource + FieldSeparator);// PV1-14 Admit Source R

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-15    Ambulatory Status O
            testHl7MessageToTransmit.Append(VIPIndicator + FieldSeparator);// PV1-16    VIP Indicator 0
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-17    Admitting Doctor 0
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-18    Patient Type 0

            //if (ConsultingDr != "")
            // {
            testHl7MessageToTransmit.Append(VistID + "^^^&" + SystemCode + FieldSeparator);// PV1-19   Visit Number
            //}
            //else
            //{
            //    testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-19   Visit Number
            //}

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-20    Financial Class
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-21    Charge Price Indicator
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-22    Courtesy Code
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-23    Credit Rating
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-24     Contract Code
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-25     Contract Effective Date

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-26     Contract Amount
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-27     Contract Period
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-28     Interest Code

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-29     Transfer to Bad Debt Code
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-30     Transfer to Bad Debt Date
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-31     Bad Debt Agency Code

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-32     Bad Debt Transfer Amount
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-33     Bad Debt Recovery Amount
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-34     Delete Account Indicator
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-35     Delete Account Date

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-36     Discharge Disposition  R

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-37     Discharged to Location
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-38     Diet Type
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-39     Servicing Facility
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-40     Bed Status
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-41     Account Status
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-42     Pending Location
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-43     Prior Temporary Location

            testHl7MessageToTransmit.Append(AdmitDate);// PV1-44      Admit Date/Time  R
        }

        void BindIP_PatientVisitAdditionalInformation_PV1()
        {

            //Patient Visit
            testHl7MessageToTransmit.Append(strNewLineChar);
            testHl7MessageToTransmit.Append("PV1" + FieldSeparator);
            testHl7MessageToTransmit.Append("1" + FieldSeparator);          //  PV1-1   Set ID
            testHl7MessageToTransmit.Append("I" + FieldSeparator); //  PV1-2   Patient Class R

            // Specialty
            //NOTE: For all the messages you have shared PV1-3.3 as ALAINwhich is invalid 
            //      Kindly note that for outpatient cases, there is no need to share PV1-3.3 sub field, you can keep it empty as:

            //testHl7MessageToTransmit.Append(SpecialtyName + "^" + SpecialtyName + "^" + BRANCH_NAME + "^" + PROVIDER_ID + "&" + SystemCode + "-DOHID" + FieldSeparator);//  PV1-3   Assigned Patient Location R


            string PatientLocation = AdmittingDrSpecialtyName;
            if (PatientLocation.Length > 20)
            {
                PatientLocation = PatientLocation.Substring(0, 20);
            }


            testHl7MessageToTransmit.Append(PatientLocation + "^" + PatientLocation + "^" + IP_WARD_NO + "^" + PROVIDER_ID + "&" + SystemCode + "-DOHID" + FieldSeparator);//  PV1-3   Assigned Patient Location R

            if (MessageEvent == "ADT-A06")
            {
                testHl7MessageToTransmit.Append("" + FieldSeparator);           //  PV1-4   Admission Type O
                testHl7MessageToTransmit.Append("" + FieldSeparator);           //  PV1-5   Pre-admit Number O
                testHl7MessageToTransmit.Append("" + FieldSeparator);           //  PV1-6   Prior Patient Location O
            }
            else
            {
                testHl7MessageToTransmit.Append("" + FieldSeparator);           //  PV1-4   Admission Type O
                testHl7MessageToTransmit.Append("" + FieldSeparator);           //  PV1-5   Pre-admit Number O
                testHl7MessageToTransmit.Append("" + FieldSeparator);           //  PV1-6   Prior Patient Location O
            }

            if (AdmittingDr != "")
            {
                testHl7MessageToTransmit.Append(AdmittingDr + "^" + AdmittingDrName + "^^^^^&" + SystemCode + "-DOHID" + FieldSeparator);//  PV1-7   Attending Doctor  R
            }
            else
            {
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  PV1-7   Attending Doctor  R
            }

            ////if (ReferringDr != "")
            ////{
            ////    testHl7MessageToTransmit.Append(ReferringDr + "&" + SystemCode + "-DOHID" + FieldSeparator);//  PV1-8   Referring Doctor
            ////}
            ////else
            ////{
            testHl7MessageToTransmit.Append("" + FieldSeparator);//  PV1-8   Referring Doctor
            ////  }


            if (AdmittingDr != "")
            {
                testHl7MessageToTransmit.Append(AdmittingDr + "^" + AdmittingDrName + "^^^^^&" + SystemCode + "-DOHID" + FieldSeparator);//  PV1-9   Consulting Doctor
            }
            else
            {
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  PV1-9   Consulting Doctor
            }



            testHl7MessageToTransmit.Append(AdmittingDrSpecialty == "" ? "20" + FieldSeparator : AdmittingDrSpecialty + FieldSeparator);   // PV1-10  Hospital Service   R
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-11    Temporary Location O
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-12    Pre-admit Test Indicator 0
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-13    Re-admission Indicator 0
            testHl7MessageToTransmit.Append(AdmitSource == "" ? "9" + FieldSeparator : AdmitSource + FieldSeparator);// PV1-14 Admit Source R

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-15    Ambulatory Status O
            testHl7MessageToTransmit.Append(VIPIndicator + FieldSeparator);// PV1-16    VIP Indicator 0
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-17    Admitting Doctor 0
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-18    Patient Type 0

            //if (ConsultingDr != "")
            // {
            testHl7MessageToTransmit.Append(IP_ADMISSION_NO + "^^^&" + SystemCode + FieldSeparator);// PV1-19   Visit Number
            //}
            //else
            //{
            //    testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-19   Visit Number
            //}

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-20    Financial Class
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-21    Charge Price Indicator
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-22    Courtesy Code
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-23    Credit Rating
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-24     Contract Code
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-25     Contract Effective Date

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-26     Contract Amount
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-27     Contract Period
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-28     Interest Code

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-29     Transfer to Bad Debt Code
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-30     Transfer to Bad Debt Date
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-31     Bad Debt Agency Code

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-32     Bad Debt Transfer Amount
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-33     Bad Debt Recovery Amount
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-34     Delete Account Indicator
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-35     Delete Account Date

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-36     Discharge Disposition  R

            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-37     Discharged to Location
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-38     Diet Type
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-39     Servicing Facility
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-40     Bed Status
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-41     Account Status
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-42     Pending Location
            testHl7MessageToTransmit.Append("" + FieldSeparator);// PV1-43     Prior Temporary Location

            testHl7MessageToTransmit.Append(ADMISSION_START_DATE_TIME + FieldSeparator);// PV1-44      Admit Date/Time  R //EMR_START_DATE_TIME


        }

        /* Boolean BindProblemDetailsAdd_PRB()
         {
             string ActionCode = "AD";
             DataSet DS = new DataSet();
             CommonBAL objCom = new CommonBAL();
             string Criteria = " 1=1  ";

             Criteria += " AND ISNULL(EPC_STATUS,'') <> 'D'   ";


             Criteria += " AND EPC_PT_ID = '" + PT_ID + "'";

             DS = objCom.fnGetFieldValue("*", "EMR_PT_CHRONIC_PROBLEMS", Criteria, "EPC_SL_NO");

             if (DS.Tables[0].Rows.Count > 0)
             {
                 String SL_NO;
              

                 for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                 {

                     DiagnosisCode = Convert.ToString(DS.Tables[0].Rows[i]["EPC_DIAG_CODE"]);
                     DiagnosisDescription = Convert.ToString(DS.Tables[0].Rows[i]["EPC_DIAG_NAME"]);

                     SL_NO = Convert.ToString(DS.Tables[0].Rows[i]["EPC_SL_NO"]);

                     string ClassificationCode = "W";
                     string ClassificationDesc = "WORKING";
                     if (DS.Tables[0].Rows[i].IsNull("EPC_PROB_CLASSIFICATION") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPC_PROB_CLASSIFICATION"]) != "")
                     {
                         ClassificationDesc = Convert.ToString(DS.Tables[0].Rows[i]["EPC_PROB_CLASSIFICATION"]);
                     }
                     switch (ClassificationDesc.ToUpper())
                     {
                         case "ADMITTING":
                             ClassificationCode = "A";
                             break;
                         case "WORKING":
                             ClassificationCode = "W";
                             break;
                         case "FINAL":
                             ClassificationCode = "F";
                             break;
                         default:
                             ClassificationCode = "W";
                             break;
                     }

                     string ProblemLifeCycle = "Active";


                     if (DS.Tables[0].Rows[i].IsNull("EPC_PROB_RESOLVED_DATE") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPC_PROB_RESOLVED_DATE"]) != "")
                     {
                         ProblemLifeCycle = Convert.ToString(DS.Tables[0].Rows[i]["EPC_PROB_LIFECYCLE"]);
                     }

                     string ProblemResolvedDate = "", ProblemDate = "";

                     if (DS.Tables[0].Rows[i].IsNull("EPC_PROB_RESOLVED_DATE") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPC_PROB_RESOLVED_DATE"]) != "")
                     {
                         DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[i]["EPC_PROB_RESOLVED_DATE"]);

                         string strMonth = dtStartDate.Month.ToString();
                         if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                         string strDay = dtStartDate.Day.ToString();
                         if (strDay.Length == 1) { strDay = "0" + strDay; }

                         string strHour = dtStartDate.Hour.ToString();
                         if (strHour.Length == 1) { strHour = "0" + strHour; }

                         string strMinute = dtStartDate.Minute.ToString();
                         if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                         ProblemResolvedDate = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                     }

                     if (DS.Tables[0].Rows[i].IsNull("EPC_CREATED_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPC_CREATED_DATE"]) != "")
                     {
                         DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[i]["EPC_CREATED_DATE"]);

                         string strMonth = dtStartDate.Month.ToString();
                         if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                         string strDay = dtStartDate.Day.ToString();
                         if (strDay.Length == 1) { strDay = "0" + strDay; }

                         string strHour = dtStartDate.Hour.ToString();
                         if (strHour.Length == 1) { strHour = "0" + strHour; }

                         string strMinute = dtStartDate.Minute.ToString();
                         if (strMinute.Length == 1) { strMinute = "0" + strMinute; }


                         ProblemDate = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                     }
                     if (DiagnosisCode == "" || DiagnosisDescription == "" || ProblemLifeCycle == "")
                     {

                         return false;
                     }

                     testHl7MessageToTransmit.Append(strNewLineChar);
                     testHl7MessageToTransmit.Append("PRB" + FieldSeparator);
                     testHl7MessageToTransmit.Append(ActionCode + FieldSeparator);                       // PRB-1	Action Code	ID	O
                     testHl7MessageToTransmit.Append(ProblemDate + FieldSeparator);        // PRB-2	Action Date/Time	 TS 	O
                     testHl7MessageToTransmit.Append(DiagnosisCode + "^" + DiagnosisDescription + "^ICD10" + FieldSeparator);           // PRB-3 	Problem ID	CE	R
                     testHl7MessageToTransmit.Append(PT_ID.Replace("/", "_") + "_" + SL_NO + "^^" + SystemCode + FieldSeparator);  // PRB-4 	 Problem Instance ID 	EI	 R 

                     testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-5	Episode of Care ID	EI	 O
                     testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-6	Problem List Priority  O
                     testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-7	Problem Established Date Time  O
                     testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-8	Anticipated Problem Resolution Date/Time  O

                     testHl7MessageToTransmit.Append(ProblemResolvedDate + FieldSeparator);      //  PRB-9 	Actual Problem Resolution Date/Time   O
                     testHl7MessageToTransmit.Append(ClassificationCode + "^" + ClassificationDesc + "^" + "MALAFFI" + FieldSeparator);      //  PRB-10 	 Problem Classification 	CE	 R
                     // testHl7MessageToTransmit.Append("F^Final^" + "MALAFFI" + FieldSeparator);      //  PRB-10 	 Problem Classification 	CE	 R


                     testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-11	Problem Management Discipline 	CE	 O
                     testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-12	 Problem Persistence	CE	 O
                     testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-13	Problem Confirmation Status	CE	 O

                     testHl7MessageToTransmit.Append(ProblemLifeCycle + "^" + ProblemLifeCycle + "^" + SystemCode + FieldSeparator);      //  PRB-14 	Problem Life Cycle Status 	CE	R
                     testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-15	Problem Life Cycle Status Date/Time	 TS 	 O
                     testHl7MessageToTransmit.Append(ProblemDate + FieldSeparator);      //  PRB-16 	Problem Date of Onset	 TS 	R

                     testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-17	 Problem Onset Text 	ST	 O
                     testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-18	 Problem Ranking 	CE	 O
                     testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-19 	 Certainty of Problem 	CE	O 
                     testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-20	Probability of Problem	NM	O 
                     testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-21	Individual Awareness of Problem	CE	O 
                     testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-22	Problem Prognosis	CE	O 
                     testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-23	Individual Awareness of Prognosis	CE	O 
                     testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-24	Family/Significant Other Awareness of Problem/Prognosis 	ST	O 
                     testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-25 	 Security/Sensitivity 	CE	 O 

                     testHl7MessageToTransmit.Append(strNewLineChar);
                     testHl7MessageToTransmit.Append("ROL" + FieldSeparator);
                     testHl7MessageToTransmit.Append("1" + FieldSeparator);      //  ROL-1	Role Instance ID	EI	 O 
                     testHl7MessageToTransmit.Append("UP" + FieldSeparator);      //  ROL-2	Action Code	ID	 O  //AD
                     testHl7MessageToTransmit.Append("CP^Consulting Provider^MALAFFI" + FieldSeparator);      //  ROL-3	Role	CE	R
                     if (ConsultingDr != "")
                     {
                         testHl7MessageToTransmit.Append(ConsultingDr + "^^" + ConsultingDrName + "^^^^^&" + SystemCode + "-DOHID" + FieldSeparator);//  ROL-4	Role Person	 XCN 	R
                     }
                     else
                     {
                         testHl7MessageToTransmit.Append("" + FieldSeparator);//  ROL-4	Role Person	 XCN 	R
                     }

                    
                 }

             }

             return true;
         }

         */

        Boolean BindProblemDetails_PRB(string ActionCode)
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  ";

            if (ActionCode == "DE")
            {
                Criteria += " AND ISNULL(EPC_STATUS,'') = 'D'  ";
            }
            else
            {
                Criteria += " AND ISNULL(EPC_STATUS,'') <> 'D'   ";
            }


            Criteria += " AND EPC_PT_ID = '" + PT_ID + "'";


            Criteria += " AND EPC_DIAG_CODE = '" + SERV_ID + "'";

            DS = objCom.fnGetFieldValue("*", "EMR_PT_CHRONIC_PROBLEMS", Criteria, "EPC_SL_NO");

            if (DS.Tables[0].Rows.Count > 0)
            {
                String SL_NO;

                DiagnosisCode = Convert.ToString(DS.Tables[0].Rows[0]["EPC_DIAG_CODE"]);
                DiagnosisDescription = Convert.ToString(DS.Tables[0].Rows[0]["EPC_DIAG_NAME"]);
                SL_NO = Convert.ToString(DS.Tables[0].Rows[0]["EPC_SL_NO"]);

                string ClassificationCode = "W";
                string ClassificationDesc = "WORKING";
                if (DS.Tables[0].Rows[0].IsNull("EPC_PROB_CLASSIFICATION") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPC_PROB_CLASSIFICATION"]) != "")
                {
                    ClassificationDesc = Convert.ToString(DS.Tables[0].Rows[0]["EPC_PROB_CLASSIFICATION"]);
                }
                switch (ClassificationDesc.ToUpper())
                {
                    case "ADMITTING":
                        ClassificationCode = "A";
                        break;
                    case "WORKING":
                        ClassificationCode = "W";
                        break;
                    case "FINAL":
                        ClassificationCode = "F";
                        break;
                    default:
                        ClassificationCode = "W";
                        break;
                }

                string ProblemLifeCycle = "Active";


                if (DS.Tables[0].Rows[0].IsNull("EPC_PROB_RESOLVED_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPC_PROB_RESOLVED_DATE"]) != "")
                {
                    ProblemLifeCycle = Convert.ToString(DS.Tables[0].Rows[0]["EPC_PROB_LIFECYCLE"]);
                }

                string ProblemResolvedDate = "", OnSetDate = "", ProblemDate = "";

                if (DS.Tables[0].Rows[0].IsNull("EPC_PROB_RESOLVED_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPC_PROB_RESOLVED_DATE"]) != "")
                {
                    DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPC_PROB_RESOLVED_DATE"]);

                    string strMonth = dtStartDate.Month.ToString();
                    if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                    string strDay = dtStartDate.Day.ToString();
                    if (strDay.Length == 1) { strDay = "0" + strDay; }

                    string strHour = dtStartDate.Hour.ToString();
                    if (strHour.Length == 1) { strHour = "0" + strHour; }

                    string strMinute = dtStartDate.Minute.ToString();
                    if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                    ProblemResolvedDate = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                }


                if (DS.Tables[0].Rows[0].IsNull("EPC_DATE_OF_ONSET") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPC_DATE_OF_ONSET"]) != "")
                {
                    DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPC_DATE_OF_ONSET"]);

                    string strMonth = dtStartDate.Month.ToString();
                    if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                    string strDay = dtStartDate.Day.ToString();
                    if (strDay.Length == 1) { strDay = "0" + strDay; }



                    OnSetDate = dtStartDate.Year.ToString() + strMonth + strDay;

                }

                if (DS.Tables[0].Rows[0].IsNull("EPC_CREATED_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPC_CREATED_DATE"]) != "")
                {
                    DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPC_CREATED_DATE"]);

                    string strMonth = dtStartDate.Month.ToString();
                    if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                    string strDay = dtStartDate.Day.ToString();
                    if (strDay.Length == 1) { strDay = "0" + strDay; }

                    string strHour = dtStartDate.Hour.ToString();
                    if (strHour.Length == 1) { strHour = "0" + strHour; }

                    string strMinute = dtStartDate.Minute.ToString();
                    if (strMinute.Length == 1) { strMinute = "0" + strMinute; }


                    ProblemDate = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                }



                if (DiagnosisCode == "" || DiagnosisDescription == "" || ProblemLifeCycle == "")
                {

                    return false;
                }

                testHl7MessageToTransmit.Append(strNewLineChar);
                testHl7MessageToTransmit.Append("PRB" + FieldSeparator);
                testHl7MessageToTransmit.Append(ActionCode + FieldSeparator);                       // PRB-1	Action Code	ID	O
                testHl7MessageToTransmit.Append(ProblemDate + FieldSeparator);        // PRB-2	Action Date/Time	 TS 	O
                testHl7MessageToTransmit.Append(DiagnosisCode + "^" + DiagnosisDescription + "^ICD10" + FieldSeparator);           // PRB-3 	Problem ID	CE	R
                testHl7MessageToTransmit.Append(PT_ID.Replace("/", "_") + "_" + SL_NO + "^^" + SystemCode + FieldSeparator);  // PRB-4 	 Problem Instance ID 	EI	 R 

                testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-5	Episode of Care ID	EI	 O
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-6	Problem List Priority  O
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-7	Problem Established Date Time  O
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-8	Anticipated Problem Resolution Date/Time  O

                testHl7MessageToTransmit.Append(ProblemResolvedDate + FieldSeparator);      //  PRB-9 	Actual Problem Resolution Date/Time   O
                testHl7MessageToTransmit.Append(ClassificationCode + "^" + ClassificationDesc + "^" + "MALAFFI" + FieldSeparator);      //  PRB-10 	 Problem Classification 	CE	 R
                // testHl7MessageToTransmit.Append("F^Final^" + "MALAFFI" + FieldSeparator);      //  PRB-10 	 Problem Classification 	CE	 R


                testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-11	Problem Management Discipline 	CE	 O
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-12	 Problem Persistence	CE	 O
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-13	Problem Confirmation Status	CE	 O

                testHl7MessageToTransmit.Append(ProblemLifeCycle + "^" + ProblemLifeCycle + "^" + SystemCode + FieldSeparator);      //  PRB-14 	Problem Life Cycle Status 	CE	R
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-15	Problem Life Cycle Status Date/Time	 TS 	 O
                testHl7MessageToTransmit.Append(OnSetDate + FieldSeparator);      //  PRB-16 	Problem Date of Onset	 TS 	R  //ProblemDate

                testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-17	 Problem Onset Text 	ST	 O
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-18	 Problem Ranking 	CE	 O
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-19 	 Certainty of Problem 	CE	O 
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-20	Probability of Problem	NM	O 
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-21	Individual Awareness of Problem	CE	O 
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-22	Problem Prognosis	CE	O 
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-23	Individual Awareness of Prognosis	CE	O 
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-24	Family/Significant Other Awareness of Problem/Prognosis 	ST	O 
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //  PRB-25 	 Security/Sensitivity 	CE	 O 

                testHl7MessageToTransmit.Append(strNewLineChar);
                testHl7MessageToTransmit.Append("ROL" + FieldSeparator);
                testHl7MessageToTransmit.Append("1" + FieldSeparator);      //  ROL-1	Role Instance ID	EI	 O 
                testHl7MessageToTransmit.Append("AD" + FieldSeparator);      //  ROL-2	Action Code	ID	 O 
                testHl7MessageToTransmit.Append("CP^Consulting Provider^MALAFFI" + FieldSeparator);      //  ROL-3	Role	CE	R
                if (ConsultingDr != "")
                {
                    testHl7MessageToTransmit.Append(ConsultingDr + "^" + ConsultingDrName + "^^^^^&" + SystemCode + "-DOHID" + FieldSeparator);//  ROL-4	Role Person	 XCN 	R
                }
                else
                {
                    testHl7MessageToTransmit.Append("" + FieldSeparator);//  ROL-4	Role Person	 XCN 	R
                }



            }
            else
            {
                return false;
            }

            return true;
        }

        void BindPTAllergyInformation_AL1()
        {

            DataSet DS = new DataSet();

            string Criteria = " EPA_PT_ID = '" + PT_ID + "'";


            CommonBAL objCom = new CommonBAL();

            DS = objCom.fnGetFieldValue("*", "EMR_PT_ALLERGIES", Criteria, "EPA_TYPE_CODE,EPA_IDENTIFI_DATE desc");

            if (DS.Tables[0].Rows.Count > 0)
            {
                int RowNumber = 1;

                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {


                    string AllergyTypeCode = Convert.ToString(DS.Tables[0].Rows[i]["EPA_TYPE_CODE"]).ToUpper();
                    string AllergyTypeDesc = Convert.ToString(DS.Tables[0].Rows[i]["EPA_TYPE_DESC"]).ToUpper();

                    string AllergyCode = Convert.ToString(DS.Tables[0].Rows[i]["EPA_ALLERGY_CODE"]).Trim();
                    string AllergyDescription = Convert.ToString(DS.Tables[0].Rows[i]["EPA_ALLERGY_DESC"]).Trim();

                    string AllergySeverityCode = Convert.ToString(DS.Tables[0].Rows[i]["EPA_SEVERITY_CODE"]).Trim();
                    string AllergySeverityDesc = Convert.ToString(DS.Tables[0].Rows[i]["EPA_SEVERITY_DESC"]).Trim();

                    string AllergyReaction = Convert.ToString(DS.Tables[0].Rows[i]["EPA_REACTION"]).Trim();

                    string AllergyDate = "";


                    if (DS.Tables[0].Rows.Count > 1)
                    {
                        if (AllergyTypeCode == "NA" || AllergyTypeCode == "NK")
                        {
                            goto LoopEnd;
                        }
                    }
                    if (AllergyTypeCode == "" || AllergySeverityCode == "" || AllergyDescription == "")
                    {
                        goto LoopEnd;
                    }


                    if (DS.Tables[0].Rows[0].IsNull("EPA_IDENTIFI_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPA_IDENTIFI_DATE"]) != "")
                    {
                        DateTime dtAllergyDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPA_IDENTIFI_DATE"]);

                        string strMonth = dtAllergyDate.Month.ToString();
                        if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                        string strDay = dtAllergyDate.Day.ToString();
                        if (strDay.Length == 1) { strDay = "0" + strDay; }

                        string strHour = dtAllergyDate.Hour.ToString();
                        if (strHour.Length == 1) { strHour = "0" + strHour; }

                        string strMinute = dtAllergyDate.Minute.ToString();
                        if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                        AllergyDate = dtAllergyDate.Year.ToString() + strMonth + strDay;


                    }



                    AllergyDescription = AllergyDescription.Replace("yes ", "").Replace("Yes ", "").Replace("YES ", "");

                    testHl7MessageToTransmit.Append(strNewLineChar);
                    testHl7MessageToTransmit.Append("AL1" + FieldSeparator);
                    testHl7MessageToTransmit.Append(RowNumber + FieldSeparator);                // AL1-1    Set ID  R

                    if (AllergyTypeCode == "NK")
                    {
                        testHl7MessageToTransmit.Append("" + FieldSeparator);            // AL1-2    Allergy Type  R
                        testHl7MessageToTransmit.Append("1602440002^No Known Allergies^SNM" + FieldSeparator); // AL1-3    Allergen Code Mnemonic Description  R
                        testHl7MessageToTransmit.Append("" + FieldSeparator);          // AL1-4    Allergy Severity  R
                    }
                    else
                    {
                        testHl7MessageToTransmit.Append(AllergyTypeCode + "^" + AllergyTypeDesc + "^MALAFFI" + FieldSeparator);            // AL1-2    Allergy Type  R
                        testHl7MessageToTransmit.Append("^" + AllergyDescription + FieldSeparator); // AL1-3    Allergen Code Mnemonic Description  R
                        testHl7MessageToTransmit.Append(AllergySeverityCode + "^" + AllergySeverityDesc + "^MALAFFI" + FieldSeparator);          // AL1-4    Allergy Severity  R
                    }

                    testHl7MessageToTransmit.Append(AllergyReaction + FieldSeparator);   // AL1-5    Allergy Reaction  O  + "^MALAFFI"

                    testHl7MessageToTransmit.Append(AllergyDate);                                        // AL1-6    IdentificationDate  R EMR_START_DATE


                LoopEnd: ;
                    RowNumber = RowNumber + 1;
                }
            }


        }

        void BindPTDiagnosis_DG1()
        {
            if (EMR_ID == "")
            {
                goto FunEnd;
            }
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND EPD_DIAG_CODE IS NOT NULL AND EPD_DIAG_CODE !='' AND EPD_TYPE IS NOT NULL ";

            Criteria += " AND ISNULL(EPD_STATUS,'') <> 'D'  ";

            Criteria += " AND EPD_ID = '" + EMR_ID + "'";

            DS = objCom.fnGetFieldValue("*", "EMR_PT_DIAGNOSIS", Criteria, "EPD_TYPE");

            if (DS.Tables[0].Rows.Count > 0)
            {
                int RowNumber = 1;
                string DiagnosisPriorityList = "";
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {

                    DiagnosisCode = Convert.ToString(DS.Tables[0].Rows[i]["EPD_DIAG_CODE"]);
                    DiagnosisDescription = Convert.ToString(DS.Tables[0].Rows[i]["EPD_DIAG_NAME"]);
                    string strProcedurePriority = Convert.ToString(DS.Tables[0].Rows[i]["EPD_TYPE"]).ToUpper();
                    switch (strProcedurePriority)
                    {
                        case "ADMITTING":
                            DiagnosisPriority = "0";
                            break;
                        case "PRINCIPAL":
                            DiagnosisPriority = "1";
                            break;
                        case "SECONDARY":
                            DiagnosisPriority = "2";
                            break;
                        default:
                            DiagnosisPriority = "2";
                            break;
                    }

                    string DiagnosisType = "A";
                    string strDiagnosisType = Convert.ToString(DS.Tables[0].Rows[i]["EPD_DIAG_STATUS_TYPE"]).ToUpper();
                    switch (strDiagnosisType)
                    {
                        case "ADMITTING":
                            DiagnosisType = "A";
                            break;
                        case "WORKING":
                            DiagnosisType = "W";
                            break;
                        case "FINAL":
                            DiagnosisType = "F";
                            break;
                        default:
                            DiagnosisType = "A";
                            break;
                    }


                    if (DiagnosisPriority == "1" && DiagnosisPriorityList != "")
                    {
                        if (DiagnosisPriorityList.IndexOf("1") != -1)
                        {
                            DiagnosisPriority = "2";
                        }
                    }

                    DiagnosisPriorityList += DiagnosisPriority;

                    testHl7MessageToTransmit.Append(strNewLineChar);
                    testHl7MessageToTransmit.Append("DG1" + FieldSeparator);
                    testHl7MessageToTransmit.Append(RowNumber + FieldSeparator);            // DG1-1   Set ID  R
                    testHl7MessageToTransmit.Append("" + FieldSeparator);                   // DG1-2   Diagnosis Coding Method
                    testHl7MessageToTransmit.Append(DiagnosisCode + "^" + DiagnosisDescription + "^ICD10" + FieldSeparator); // DG1-3   Diagnosis Code Description
                    testHl7MessageToTransmit.Append("" + FieldSeparator);                   // DG1-4   Diagnosis Coding Method


                    if (EMR_START_DATE_TIME != "")
                    {
                        testHl7MessageToTransmit.Append(EMR_START_DATE_TIME + FieldSeparator);  // DG1-5   Diagnosis Date/Time  R
                    }
                    else
                    {
                        testHl7MessageToTransmit.Append(EMR_END_DATE_TIME + FieldSeparator);  // DG1-5   Diagnosis Date/Time  R
                    }

                    testHl7MessageToTransmit.Append(DiagnosisType + FieldSeparator);        // DG1-6   Diagnosis Type

                    testHl7MessageToTransmit.Append("" + FieldSeparator);                   // DG1-7   Major Diagnostic Category
                    testHl7MessageToTransmit.Append("" + FieldSeparator);                   // DG1-8   Diagnostic-related Group
                    testHl7MessageToTransmit.Append("" + FieldSeparator);                   // DG1-9   DRG Approval Indicator
                    testHl7MessageToTransmit.Append("" + FieldSeparator);                   // DG1-10  DRG Grouper Review Code
                    testHl7MessageToTransmit.Append("" + FieldSeparator);                   // DG1-11  Outlier Type
                    testHl7MessageToTransmit.Append("" + FieldSeparator);                   // DG1-12  Outlier Days
                    testHl7MessageToTransmit.Append("" + FieldSeparator);                   // DG1-13  Outlier Cost
                    testHl7MessageToTransmit.Append("" + FieldSeparator);                   // DG1-14  Grouper Version And Type
                    testHl7MessageToTransmit.Append(DiagnosisPriority + FieldSeparator);    // DG1-15  Procedure Priority
                    testHl7MessageToTransmit.Append("" + FieldSeparator);                   // DG1-16  Diagnosing Clinician O
                    testHl7MessageToTransmit.Append("" + FieldSeparator);                   // DG1-17  Diagnosis Classification O
                    testHl7MessageToTransmit.Append("N");                                   // DG1-18  Confidential Indicator  R

                    RowNumber = RowNumber + 1;
                }

            }

        FunEnd: ;
        }

        void BindPTProcedures()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND ISNULL(EPP_STATUS,'') <> 'D' AND EPP_DIAG_CODE IS NOT NULL AND EPP_DIAG_CODE !='' ";

            Criteria += " AND EPP_ID = '" + EMR_ID + "'";

            DS = objCom.fnGetFieldValue("*,(CASE ISNULL(EPP_SERV_CODE_TYPE,'') WHEN 3 THEN 'CPT' WHEN 6 THEN 'USCLS' WHEN 5 THEN 'DRUG' WHEN 4 THEN 'HCPCS' WHEN 8 THEN 'SERVICE'  END ) AS EPP_SERV_CODE_TYPEDesc", "EMR_PT_PROCEDURES ", Criteria, "EPP_POSITION");

            if (DS.Tables[0].Rows.Count > 0)
            {
                int RowNumber = 1;

                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    string strCodeType = "";
                    ProceduresCode = Convert.ToString(DS.Tables[0].Rows[i]["EPP_DIAG_CODE"]);
                    ProceduresDescription = Convert.ToString(DS.Tables[0].Rows[i]["EPP_DIAG_NAME"]);

                    ProceduresDescription = RemoveSplChar(Convert.ToString(ProceduresDescription.Trim()));

                    ProceduresDescription = ProceduresDescription.Replace("\n", string.Empty);
                    ProceduresDescription = ProceduresDescription.Replace(Environment.NewLine, string.Empty);

                    if (ProceduresDescription.Length > 100)
                    {
                        ProceduresDescription = ProceduresDescription.Substring(0, 100);
                    }

                    if (DS.Tables[0].Rows[0].IsNull("EPP_SERV_CODE_TYPEDesc") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPP_SERV_CODE_TYPEDesc"]) != "")
                    {
                        strCodeType = Convert.ToString(DS.Tables[0].Rows[i]["EPP_SERV_CODE_TYPEDesc"]);
                    }

                    string HaadCodeType = "";
                    if (CheckProcedureType(ProceduresCode, out HaadCodeType) == false)
                    {
                        goto LoopEnd;
                    }

                    if (strCodeType == "")
                    {
                        strCodeType = HaadCodeType;
                    }
                    string strProcedurePriority = Convert.ToString(RowNumber);
                    switch (strProcedurePriority)
                    {
                        case "1":
                            ProcedurePriority = "1";  //Primary Procedure
                            break;
                        default:
                            ProcedurePriority = "2"; //Secondary Procedures
                            break;

                    }

                    testHl7MessageToTransmit.Append(strNewLineChar);
                    testHl7MessageToTransmit.Append("PR1" + FieldSeparator);
                    testHl7MessageToTransmit.Append(RowNumber + FieldSeparator); // PR1-1   Set ID  R
                    testHl7MessageToTransmit.Append(strCodeType + FieldSeparator); // PR1-2   Procedure Coding Method  //"CPT"
                    testHl7MessageToTransmit.Append(ProceduresCode + "^" + ProceduresDescription + "^" + strCodeType + FieldSeparator); // PR1-3   Procedure Code Description //"^CPT"
                    testHl7MessageToTransmit.Append(ProceduresDescription + FieldSeparator);    // PR1-4   Procedure Description 
                    testHl7MessageToTransmit.Append(EMR_START_DATE_TIME + FieldSeparator);      // PR1-5   Procedure Date/Time
                    testHl7MessageToTransmit.Append("" + FieldSeparator);                       // PR1-6   Procedure Functional Type
                    testHl7MessageToTransmit.Append("" + FieldSeparator);                       // PR1-7   Procedure Minutes

                    testHl7MessageToTransmit.Append("" + FieldSeparator); // PR1-8   Anesthesiologist 
                    testHl7MessageToTransmit.Append("" + FieldSeparator); // PR1-9   Anesthesia Code 
                    testHl7MessageToTransmit.Append("" + FieldSeparator); // PR1-10   Anesthesia Minutes
                    testHl7MessageToTransmit.Append("" + FieldSeparator); // PR1-11   Surgeon

                    if (ConsultingDr != "")
                    {
                        testHl7MessageToTransmit.Append(ConsultingDr + "^" + ConsultingDrName + "^^^^^&" + SystemCode + "-DOHID" + FieldSeparator);//  PR1-12   Procedure Practitioner O
                    }
                    else
                    {
                        testHl7MessageToTransmit.Append("" + FieldSeparator);//  PR1-12   Procedure Practitioner O
                    }
                    testHl7MessageToTransmit.Append("" + FieldSeparator); // PR1-13   Consent Code
                    testHl7MessageToTransmit.Append(ProcedurePriority + FieldSeparator); // PR1-14   Procedure Priority
                    testHl7MessageToTransmit.Append("" + FieldSeparator); // PR1-15   Associated Diagnosis Code



                    RowNumber = RowNumber + 1;
                LoopEnd: ;
                }

            }
        }

        void GetRadValue(string strServCode, out string ObsValue, out string ObsDateTime)
        {
            ObsValue = "";
            ObsDateTime = "";
            //  RTR_PATIENT_ID,RTR_DATE,RTR_DR_CODE,RTR_SERV_ID
            string Criteria = "1=1";

            Criteria += " AND RTR_PATIENT_ID='" + PatientID + "' AND   CAST(RTR_DATE AS DATE) = CAST('" + EMR_START_DATE + "' AS DATE) AND RTR_SERV_ID='" + strServCode + "'"; // AND RTR_DR_CODE='" + DR_ID + "'
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            DS = objCom.fnGetFieldValue(" TOP 1 RTR_TRANS_NO,RTR_REPORT,RTR_DATE", "RAD_TEST_REPORT", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["RTR_REPORT"]);

                RAD_TEST_REPORT_ID = Convert.ToString(DS.Tables[0].Rows[0]["RTR_TRANS_NO"]);

                if (DS.Tables[0].Rows[0].IsNull("RTR_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["RTR_DATE"]) != "")
                {
                    DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["RTR_DATE"]);

                    string strMonth = dtStartDate.Month.ToString();
                    if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                    string strDay = dtStartDate.Day.ToString();
                    if (strDay.Length == 1) { strDay = "0" + strDay; }

                    string strHour = dtStartDate.Hour.ToString();
                    if (strHour.Length == 1) { strHour = "0" + strHour; }

                    string strMinute = dtStartDate.Minute.ToString();
                    if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                    EMR_START_DATE = dtStartDate.Year.ToString() + strMonth + strDay;
                    ObsDateTime = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                }

            }
        }

        void BindPharmacyTreatmentOrder_RXO(string GenericCode, string GenericName, string PhyQty, string phyRemarks, string Unit, string UnitType, string Strength, string Dosage_Form, string Refills)
        {
            string PhyName = "";
            PhyName += GenericName + " " + Strength + " " + Dosage_Form;

            testHl7MessageToTransmit.Append(strNewLineChar);
            testHl7MessageToTransmit.Append("RXO" + FieldSeparator);
            testHl7MessageToTransmit.Append(GenericCode + "^" + PhyName.Replace("'", "") + "^DOHDRUG" + FieldSeparator);    //RXO-1	Requested Give Code	CE	R
            testHl7MessageToTransmit.Append(Unit + FieldSeparator);   //   RXO-2	Minimum Requested Give Amount	NM	R //phyAmount
            testHl7MessageToTransmit.Append("" + FieldSeparator);    //  RXO-3	Maximum Requested Give Amount	NM	O  //PhyQty 
            testHl7MessageToTransmit.Append(UnitType + "^" + UnitType + "^" + SystemCode + FieldSeparator);    //    RXO-4	Requested Give Units	CE	R

            string Dosage_FormCode = Dosage_Form;
            if (Dosage_Form.Length > 20)
            {
                Dosage_FormCode = Dosage_Form.Substring(0, 20);
            }

            testHl7MessageToTransmit.Append(Dosage_FormCode + "^" + Dosage_Form + "^" + SystemCode + FieldSeparator);    //    RXO-5	Request Dosage Form	CE	R

            // testHl7MessageToTransmit.Append("^" + phyRemarks + FieldSeparator);    //   RXO-6	Provider's Pharmacy/Treatment Instructions	CE	R

            testHl7MessageToTransmit.Append("" + FieldSeparator);    //   RXO-6	Provider's Pharmacy/Treatment Instructions	CE	R
            testHl7MessageToTransmit.Append("^" + phyRemarks + FieldSeparator);    //  RXO-7	Provider's Administration Instructions	CE	R

            testHl7MessageToTransmit.Append("" + FieldSeparator);    //  RXO-8	Deliver to Location	LA1	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);    //  RXO-9	Allow Substitutions	ID	O

            testHl7MessageToTransmit.Append(GenericCode + "^" + PhyName.Replace("'", "") + "^DOHDRUG" + FieldSeparator);    //  RXO-10	Requested Dispense Code	CE	O



            if (UnitType == "Tablet" || UnitType == "Tablet(s)")
            {
                testHl7MessageToTransmit.Append(PhyQty + FieldSeparator);    //  RXO-11	Requested Dispense Amount	NM	R  
                testHl7MessageToTransmit.Append(UnitType + "^" + UnitType + "^" + SystemCode + FieldSeparator);    //  RXO-12	Requested Dispense Units	CE	R // Unit
            }
            else
            {
                testHl7MessageToTransmit.Append("" + FieldSeparator);    //  RXO-11	Requested Dispense Amount	NM	R  
                testHl7MessageToTransmit.Append("" + FieldSeparator);    //  RXO-12	Requested Dispense Units	CE	R // Unit
            }

            //Looking at your responses below, I think the requested dispense quantity and units is not captured in your EMR.
            //You can leave RXO-11 and RXO-12 blank in your messages.
            //Thanks,
            //Osman

            //testHl7MessageToTransmit.Append(PhyQty + FieldSeparator);    //  RXO-11	Requested Dispense Amount	NM	R  
            //testHl7MessageToTransmit.Append(UnitType + "^" + UnitType + "^" + SystemCode + FieldSeparator);    //  RXO-12	Requested Dispense Units	CE	R // Unit



            testHl7MessageToTransmit.Append(Refills + FieldSeparator);    //  RXO-13	Number Of Refills	NM	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);    //   RXO-14	Ordering Providers DEA Number	 XCN 	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);    //   RXO-15	Pharmacist Treatment Suppliers Verifier ID	 XCN 	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);    //   RXO-16	Needs Human Review	ID	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);    //  RXO-17	Request Give Per Time Unit	ST	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);    //  RXO-18	Request Give Strength	NM	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);    //  RXO-19	Request Give Strength Units	CE	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);    //   RXO-20	Indication	CE	O



        }

        void BindPharmacyTreatmentAdmi_RXA(Int32 AdminSub_ID, string AdminStart, string AdminEnd, string DrugCode, string PackageName, string PhyQty, string phyRemarks, string Unit, string UnitType, string Strength, string Dosage_Form, string AdministeringProviderID, string AdministeringProviderName, string CompletionStatus)
        {
            string PhyName = "";
            PhyName += PackageName + " " + Strength + " " + Dosage_Form;

            testHl7MessageToTransmit.Append(strNewLineChar);
            testHl7MessageToTransmit.Append("RXA" + FieldSeparator);

            testHl7MessageToTransmit.Append("" + FieldSeparator);   // RXA-1	Give Sub ID Counter	NM	O
            testHl7MessageToTransmit.Append(AdminSub_ID + FieldSeparator);    // RXA-2	Administration Sub ID Counter	NM	O

            testHl7MessageToTransmit.Append(AdminStart + FieldSeparator); //RXA-3	Date/Time Start Of Administration	 TS 	R
            testHl7MessageToTransmit.Append(AdminEnd + FieldSeparator); //RXA-4	Date/Time End Of Administration	 TS 	R
            testHl7MessageToTransmit.Append(DrugCode + "^" + PhyName.Replace("'", "") + "^DOHDRUG" + FieldSeparator);    //RXA-5	Administered Code	CE	R


            testHl7MessageToTransmit.Append(Unit + FieldSeparator);   //   RXA-6	Administered Amount	NM	R
            testHl7MessageToTransmit.Append(UnitType + "^" + UnitType + "^" + SystemCode + FieldSeparator);    //    RXA-7	Administered Units	CE	R

            string Dosage_FormCode = Dosage_Form;
            if (Dosage_Form.Length > 20)
            {
                Dosage_FormCode = Dosage_Form.Substring(0, 20);
            }

            testHl7MessageToTransmit.Append(Dosage_FormCode + "^" + Dosage_Form + "^" + SystemCode + FieldSeparator);    //   RXA-8	Administered Dosage Form	CE	R
            testHl7MessageToTransmit.Append("^" + phyRemarks + FieldSeparator);    //   RXA-9	Administration Notes	CE	O


            if (AdministeringProviderID != "")
            {
                testHl7MessageToTransmit.Append(AdministeringProviderID + "^" + AdministeringProviderName + "^^^^^&" + SystemCode + "-DOHID" + FieldSeparator);//  RXA-10	Administering Provider	 XCN 	O
            }
            else
            {
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  RXA-10	Administering Provider	 XCN 	O
            }


            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXA-11	Administered at Location	LA2	O	 
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXA-12	Administered Per (Time Unit)	ST	O	 
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXA-13	Administered Strength	NM	O	 
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXA-14	Administered Strength Units	CE	O	 
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXA-15	Substance Lot Number	ST	R	 
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXA-16	Substance Expiration Date	 TS 	O	 
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXA-17	Substance Manufacturer Name	CE	O	 
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXA-18	Substance/Treatment Refusal Reason	CE	 
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXA-19	Indication	CE	O	 
            testHl7MessageToTransmit.Append(CompletionStatus + FieldSeparator);    // RXA-20	Completion Status	ID	R	 
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXA-21	Action Code	ID	O	 





        }

        void BindPharmacyTreatmentRoute_RXR(string PhyRoute)
        {

            testHl7MessageToTransmit.Append(strNewLineChar);
            testHl7MessageToTransmit.Append("RXR" + FieldSeparator);
            testHl7MessageToTransmit.Append(PhyRoute + "^MALAFFI" + FieldSeparator);

        }

        void BindPharmacyTreatmentDispense_RXD(string ActivityID, string ProceduresCode, string PackageName, string PhyQty, string PhyDossage, string phyRemarks, string ActivityStart, string Unit, string UnitType, string Strength, string Dosage_Form, string DispensingProviderID, string DispensingProviderName, string DispensingInstruction)
        {

            string PhyName = "";
            PhyName += PackageName + " " + Strength + " " + Dosage_Form;


            testHl7MessageToTransmit.Append(strNewLineChar);
            testHl7MessageToTransmit.Append("RXD" + FieldSeparator);
            testHl7MessageToTransmit.Append(ActivityID + FieldSeparator);  //  RXD-1	Dispense Sub ID Counter	NM	O

            testHl7MessageToTransmit.Append(ProceduresCode + "^" + PhyName.Replace("'", "") + "^DOHDRUG" + FieldSeparator);    //RXD-2	Dispense/Give Code	CE	R
            testHl7MessageToTransmit.Append(ActivityStart + FieldSeparator);   //  RXD-3	Date/Time Dispensed	 TS 	R
            testHl7MessageToTransmit.Append(Unit + FieldSeparator);   //RXD-4	Actual Dispense Amount	NM	R  PaymentAmt  //PaymentAmt
            testHl7MessageToTransmit.Append(UnitType + "^" + UnitType + "^" + SystemCode + FieldSeparator);   //RXD-5	Actual Dispense Units	CE	R

            string Dosage_FormCode = Dosage_Form;
            if (Dosage_Form.Length > 20)
            {
                Dosage_FormCode = Dosage_Form.Substring(0, 20);
            }

            testHl7MessageToTransmit.Append(Dosage_FormCode + "^" + Dosage_Form + "^" + SystemCode + FieldSeparator);   //RXD-6	Actual Dosage Form	CE	R

            testHl7MessageToTransmit.Append("" + FieldSeparator);    //  RXD-7	Prescription Number	ST	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXD-8	Number of Refills Remaining	NM	O
            testHl7MessageToTransmit.Append(phyRemarks + FieldSeparator);    // RXD-9	Dispense Notes	ST	O  //"^" + phyRemarks


            if (DispensingProviderID != "")
            {
                testHl7MessageToTransmit.Append(DispensingProviderID + "^" + DispensingProviderName + "^^^^^&" + SystemCode + "-DOHID" + FieldSeparator);// RXD-10	Dispensing Provider	 XCN 	O
            }
            else
            {
                testHl7MessageToTransmit.Append("" + FieldSeparator);// RXD-10	Dispensing Provider	 XCN 	O
            }


            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXD-11	Substitution Status	ID	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXD-12	Total Daily Dose	CQ	R //PhyDossage + "^ml^" + SystemCode + FieldSeparator
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXD-13	Dispense to Location	LA2	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXD-14	Needs Human Review	ID	O
            testHl7MessageToTransmit.Append(DispensingInstruction + FieldSeparator);    // RXD-15	Pharmacy/Treatment Supplier's Special Dispensing Instructions	CE	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXD-16	Actual Strength	NM	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXD-17	Actual Strength Unit	CE	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXD-18	Substance Lot Number	ST	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXD-19	Substance Expiration Date	 TS 	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXD-20	Substance Manufacturer Name	CE	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);    // RXD-21	Indication	CE	O




            //testHl7MessageToTransmit.Append("1" + FieldSeparator);   //   RXO-2	Minimum Requested Give Amount	NM	R
            //testHl7MessageToTransmit.Append("" + FieldSeparator);    //  RXO-3	Maximum Requested Give Amount	NM	O
            //testHl7MessageToTransmit.Append(PhyDossage + "^ml^" + SystemCode + FieldSeparator);    //    RXO-4	Requested Give Units	CE	R
            //testHl7MessageToTransmit.Append(PhyDossage + "^ml^" + SystemCode + FieldSeparator);    //    RXO-5	Request Dosage Form	CE	R
            //testHl7MessageToTransmit.Append(phyRemarks + FieldSeparator);    //   RXO-6	Provider's Pharmacy/Treatment Instructions	CE	R
            //testHl7MessageToTransmit.Append(phyRemarks + FieldSeparator);    //  RXO-7	Provider's Administration Instructions	CE	R
            //testHl7MessageToTransmit.Append("" + FieldSeparator);    //  RXO-8	Deliver to Location	LA1	O
            //testHl7MessageToTransmit.Append("" + FieldSeparator);    //  RXO-9	Allow Substitutions	ID	O
            //testHl7MessageToTransmit.Append("" + FieldSeparator);    //  RXO-10	Requested Dispense Code	CE	O
            //testHl7MessageToTransmit.Append("1" + FieldSeparator);    //  RXO-11	Requested Dispense Amount	NM	R
            //testHl7MessageToTransmit.Append(PhyDossage + "^ml^" + FieldSeparator);    //  RXO-12	Requested Dispense Units	CE	R
            //testHl7MessageToTransmit.Append("" + FieldSeparator);    //  RXO-13	Number Of Refills	NM	O
            //testHl7MessageToTransmit.Append("" + FieldSeparator);    //   RXO-14	Ordering Providers DEA Number	 XCN 	O
            //testHl7MessageToTransmit.Append("" + FieldSeparator);    //   RXO-15	Pharmacist Treatment Suppliers Verifier ID	 XCN 	O
            //testHl7MessageToTransmit.Append("" + FieldSeparator);    //   RXO-16	Needs Human Review	ID	O
            //testHl7MessageToTransmit.Append("" + FieldSeparator);    //  RXO-17	Request Give Per Time Unit	ST	O
            //testHl7MessageToTransmit.Append("" + FieldSeparator);    //  RXO-18	Request Give Strength	NM	O
            //testHl7MessageToTransmit.Append("" + FieldSeparator);    //  RXO-19	Request Give Strength Units	CE	O
            //testHl7MessageToTransmit.Append("" + FieldSeparator);    //   RXO-20	Indication	CE	O



        }

        Boolean ValidateNL7Message()
        {
            Boolean IsNoError = true;

            if (PatientID == "")
            {
                LogFileWriting("PID-3	Patient Identifier List is empty PT_ID = " + PT_ID + " VISIT_ID = " + VISIT_ID + " EMR_ID = " + EMR_ID);
                IsNoError = false;
            }


            if (PatientName == "")
            {
                LogFileWriting("PID-5	Patient Name is empty PT_ID = " + PT_ID + " VISIT_ID = " + VISIT_ID + " EMR_ID = " + EMR_ID);

                IsNoError = false;
            }


            if (DOB == "")
            {
                LogFileWriting("PID-7	Date/Time of Birth is empty PT_ID = " + PT_ID + " VISIT_ID = " + VISIT_ID + " EMR_ID = " + EMR_ID);

                IsNoError = false;
            }

            if (Gender == "")
            {
                LogFileWriting("PID-8	Administrative Sex is empty PT_ID = " + PT_ID + " VISIT_ID = " + VISIT_ID + " EMR_ID = " + EMR_ID);

                IsNoError = false;
            }


            if (Religion == "")
            {
                LogFileWriting("PID-17  Religion  is empty PT_ID = " + PT_ID + " VISIT_ID = " + VISIT_ID + " EMR_ID = " + EMR_ID);

                IsNoError = false;
            }

            if (EmiratesID == "")
            {
                LogFileWriting("PID-19  SSN Number  EmiratesID is empty PT_ID = " + PT_ID + " VISIT_ID = " + VISIT_ID + " EMR_ID = " + EMR_ID);

                IsNoError = false;
            }


            if (Nationality == "")
            {
                LogFileWriting("PID-28 Nationality  is empty  PT_ID = " + PT_ID + " VISIT_ID = " + VISIT_ID + " EMR_ID = " + EMR_ID);

                IsNoError = false;
            }

            if (NationalityCode == "")
            {
                LogFileWriting("PID-28 Nationality Code is empty  PT_ID = " + PT_ID + " VISIT_ID = " + VISIT_ID + " EMR_ID = " + EMR_ID);

                IsNoError = false;
            }


            if (MessageEvent != "ADT-A01" && MessageEvent != "ADT-A06" && MessageEvent != "ADT-A28" && MessageEvent != "ADT-A31" && MessageEvent != "PPR-PC1" && MessageEvent != "PPR-PC2" && MessageEvent != "PPR-PC3" && MessageEvent != "ORU-R01")
            {
                if (PatientClass == "")
                {
                    LogFileWriting("PV1-28 Patient Class is empty PT_ID = " + PT_ID + " VISIT_ID = " + VISIT_ID + " EMR_ID = " + EMR_ID);

                    IsNoError = false;
                }


                if (ConsultingDr == "")
                {
                    LogFileWriting("PV1-7   Attending/Consulting Doctor LicenseNo is empty PT_ID = " + PT_ID + " VISIT_ID = " + VISIT_ID + " EMR_ID = " + EMR_ID);

                    IsNoError = false;
                }
            }

            if (PT_TYPE.ToUpper() == "CR" || PT_TYPE.ToUpper() == "CREDIT")
            {
                string strPayerID = "";
                GetCompanyDtls(COMP_ID, out strPayerID);
                PayerID = strPayerID;


                if (PayerID == "")
                {
                    LogFileWriting(" Company Payer ID is empty PT_ID = " + PT_ID + " VISIT_ID = " + VISIT_ID + " EMR_ID = " + EMR_ID);

                    IsNoError = false;
                }

            }


            ////if (EMR_START_DATE_TIME == "")
            ////{
            ////    LogFileWriting(" EMR StartTime is empty PT_ID = " + PT_ID + " VISIT_ID = " + VISIT_ID + " EMR_ID = " + EMR_ID);

            ////    IsNoError = false;
            ////}

            return IsNoError;
        }

        Boolean CheckPTDiagnosis()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND ISNULL(EPD_STATUS,'') <> 'D'   ";

            Criteria += " AND EPD_ID = '" + EMR_ID + "'";

            DS = objCom.fnGetFieldValue(" TOP 1 *", "EMR_PT_DIAGNOSIS", Criteria, "EPD_TYPE");

            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;

            }

            return false;


        }

        Boolean CheckPTDeletedDiagnosis()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND ISNULL(EPD_STATUS,'') = 'D'  ";

            Criteria += " AND EPD_ID = '" + EMR_ID + "'";

            DS = objCom.fnGetFieldValue(" TOP 1 *", "EMR_PT_DIAGNOSIS", Criteria, "EPD_TYPE");

            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;

            }

            return false;


        }

        Boolean CheckPTProcedures()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND ISNULL(EPP_STATUS,'') <> 'D'   ";
            Criteria += " AND EPP_ID = '" + EMR_ID + "'";

            DS = objCom.fnGetFieldValue("  TOP 1 *", "EMR_PT_PROCEDURES", Criteria, "EPP_POSITION");

            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;

        }

        Boolean CheckProcedureType(string ProceduresCode, out string strCodeType)
        {
            strCodeType = "";

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  AND HHS_STATUS='Active' ";

            Criteria += " AND HHS_CODE = '" + ProceduresCode + "'";

            DS = objCom.fnGetFieldValue("  TOP 1 HHS_CODE,HHS_TYPE ", "HMS_HAAD_SERVICES", Criteria, "HHS_CODE");

            if (DS.Tables[0].Rows.Count > 0)
            {
                strCodeType = Convert.ToString(DS.Tables[0].Rows[0]["HHS_TYPE"]);
                return true;
            }
            return false;
        }

        Boolean CheckPTProblems()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND ISNULL(EPC_STATUS,'') <> 'D'   ";

            Criteria += " AND EPC_PT_ID = '" + PT_ID + "'";
            Criteria += " AND EPC_DIAG_CODE = '" + SERV_ID + "'";

            DS = objCom.fnGetFieldValue(" TOP 1 *", "EMR_PT_CHRONIC_PROBLEMS", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;

            }

            return false;


        }

        Boolean CheckPTDeleteProblems()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND ISNULL(EPC_STATUS,'') = 'D'   ";

            Criteria += " AND EPC_PT_ID = '" + PT_ID + "'";
            Criteria += " AND EPC_DIAG_CODE = '" + SERV_ID + "'";

            DS = objCom.fnGetFieldValue(" TOP 1 *", "EMR_PT_CHRONIC_PROBLEMS", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;

            }

            return false;


        }

        ////void GetPatientClass()
        ////{
        ////    PatientClass = "O";
        ////    DataSet DS = new DataSet();
        ////    CommonBAL objCom = new CommonBAL();
        ////    string Criteria = " 1=1 ";

        ////    Criteria += " AND EPV_ID = '" + EMR_ID + "'";

        ////    DS = objCom.fnGetFieldValue("  TOP 1 EPV_PATIENT_CLASS ", "EMR_PT_VITALS", Criteria, "EPV_DATE desc");

        ////    if (DS.Tables[0].Rows.Count > 0)
        ////    {

        ////        if (DS.Tables[0].Rows[0].IsNull("EPV_PATIENT_CLASS") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPV_PATIENT_CLASS"]) != "")
        ////        {
        ////            PatientClass = Convert.ToString(DS.Tables[0].Rows[0]["EPV_PATIENT_CLASS"]);
        ////        }
        ////        else
        ////        {
        ////            PatientClass = "O";
        ////        }


        ////    }
        ////    else
        ////    {
        ////        PatientClass = "O";
        ////    }

        ////}

        void GetServiceMasterName(string SearchType, string ServCode, out string strServName, out string strCodeType)
        {
            strServName = "";
            strCodeType = "";
            string Criteria = " 1=1 AND HSM_STATUS='A' ";
            if (SearchType == "HSM_SERV_ID")
            {
                Criteria += " AND  HSM_SERV_ID   ='" + ServCode + "'";
            }
            else
            {
                Criteria += " AND  HSM_HAAD_CODE   ='" + ServCode + "'";
            }

            CommonBAL objCom = new CommonBAL();
            DataSet DSServ = new DataSet();
            DSServ = objCom.fnGetFieldValue("TOP 1 *,(CASE ISNULL(HSM_CODE_TYPE,3) WHEN 3 THEN 'CPT' WHEN 6 THEN 'USCLS' WHEN 5 THEN 'DRUG' WHEN 4 THEN 'HCPCS' WHEN 8 THEN 'SERVICE'  END ) AS CodeTypeDesc", "HMS_SERVICE_MASTER", Criteria, "");

            if (DSServ.Tables[0].Rows.Count > 0)
            {
                strServName = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_NAME"]);
                strCodeType = Convert.ToString(DSServ.Tables[0].Rows[0]["CodeTypeDesc"]);
            }



        }

        void GetSampleCollDtls(string SampleCollectID, out string DeliveryDateTime)
        {
            DeliveryDateTime = "";
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND LTSM_TEST_SAMPLE_ID='" + SampleCollectID + "'";

            DS = objCom.fnGetFieldValue("TOP 1 LTSM_DELIVERY_DATE", "LAB_TEST_SAMPLE_MASTER", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {

                DateTime dtInvDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["LTSM_DELIVERY_DATE"]);

                string strMonth = dtInvDate.Month.ToString();
                if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                string strDay = dtInvDate.Day.ToString();
                if (strDay.Length == 1) { strDay = "0" + strDay; }

                string strHour = dtInvDate.Hour.ToString();
                if (strHour.Length == 1) { strHour = "0" + strHour; }

                string strMinute = dtInvDate.Minute.ToString();
                if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                DeliveryDateTime = dtInvDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

            }

        }


        void BindOrderControl_ORC(string ActionCode, Int32 intSlNo, string TEST_REPORT_ID, string OrderingProviderID, string OrderingProviderName, string phyPrescDate, string OrderQtyTiming, string OrderStatus, string OrderEffectiveDate)
        {
            testHl7MessageToTransmit.Append(strNewLineChar);
            testHl7MessageToTransmit.Append("ORC" + FieldSeparator);
            testHl7MessageToTransmit.Append(ActionCode + FieldSeparator); //  ORC-1	Order Control	ID	R

            //NOTE:  ORC-2	PLACER ORDER NUMBER WE HAVE EMR ID OR VISIT ID WITH PROCEDURE CODE EACH ORDER NUMBER SOULD UNIQUE
            testHl7MessageToTransmit.Append(TEST_REPORT_ID + "_" + intSlNo + "^^" + SystemCode + FieldSeparator);  //  ORC-2	Placer Order Number	EI	R* //EMR_ID + "_" + SLNo 
            testHl7MessageToTransmit.Append(TEST_REPORT_ID + "_1" + intSlNo + "^^" + SystemCode + FieldSeparator);  //  ORC-3	Filler Order Number	EI	R*


            testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-4	Placer Group Number	EI	O


            /* NOTE:
            Check below table for Medication Order status codes and their use:
            Malaffi Code	    Malaffi     Description	Use
            active	            Active	    • New medication orders
            • Modifying         medication   orders
            • Partial           Dispense
            on-hold	            On Hold	    • Placing medications on hold
            completed	        Completed	• Medication completed/ended/fully dispensed
            entered-in-error    Entered In Error	• Delete/Cancel medication orders
            Stopped	            Stopped	    • Stopping/Discontinue medications

             */

            if (ActionCode == "DE")
            {
                testHl7MessageToTransmit.Append("entered-in-error" + FieldSeparator); //  ORC-5	Order Status	ID	O
            }
            else
            {
                //testHl7MessageToTransmit.Append("active" + FieldSeparator); //  ORC-5	Order Status	ID	O
                testHl7MessageToTransmit.Append(OrderStatus + FieldSeparator); //  ORC-5	Order Status	ID	O
            }



            testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-6	Response Flag	ID	O

            if (OrderQtyTiming != "")
            {
                testHl7MessageToTransmit.Append(OrderQtyTiming + FieldSeparator); //  ORC-7	Quantity Timing	TQ	O
            }
            else
            {
                testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-7	Quantity Timing	TQ	O
            }

            testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-8	Parent	EIP	O
            testHl7MessageToTransmit.Append(phyPrescDate + FieldSeparator); //  ORC-9	Date/Time of Transaction	 TS 	O //TestDateTime
            testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-10	Entered by	 XCN 	O
            testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-11	Verified by	 XCN 	O


            if (OrderingProviderID != "")
            {
                // testHl7MessageToTransmit.Append(ConsultingDr + "^^" + ConsultingDrName + "^^Dr.^^^&" + SystemCode + "-DOHID" + FieldSeparator);//  ORC-12	Ordering Provider	 XCN 	R
                testHl7MessageToTransmit.Append(OrderingProviderID + "^" + OrderingProviderName + "^^^^^&" + SystemCode + "-DOHID" + FieldSeparator);//  ORC-12	Ordering Provider	 XCN 	R
            }
            else
            {
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  ORC-12	Ordering Provider	 XCN 	R
            }

            testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-13	Enterer's Location	PL	O
            testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-14	Call Back Phone Number	XTN	O
            testHl7MessageToTransmit.Append(OrderEffectiveDate + FieldSeparator); //  ORC-15	Order Effective Date Time	 TS 	O
            ////testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-16	Order Control Code Reason	CE	O
            ////testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-17	Entering Organization	CE	O
            ////testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-18	Entering Device	CE	O
            ////testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-19	Action by	 XCN 	O
            ////testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-20	Advanced Beneficiary Notice Code	CE	O
            ////testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-21	Ordering Facility Name	XON	O
            ////testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-22	Ordering Facility Address	XAD	O
            ////testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-23	Ordering Facility Phone Number	XTN	O
            ////testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-24	Ordering Provider Address	XAD	O
            ////testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-25	Order Status Modifier	CWE	O
            ////testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-26	Advanced Beneficiary Notice Override Reason	CWE	O
            ////testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-27	Filler's Expected Availability Date/Time	 TS 	O
            ////testHl7MessageToTransmit.Append("" + FieldSeparator); //  ORC-28	Confidentiality Code	CWE	O

        }


        void BindObservationRequest_OBR(Int32 intSlNo, string TEST_REPORT_ID, string LabCode, string LabDesc, string CodeTypeDesc, string ObsSubID, string ObservStart, string ObservEnd, string RequestedDateTime, string DeliveryDateTime, string VerifiedDateTime, string DiagnosticService, string ResultStatus, string OrderingProviderID, string OrderingProviderName, string ResultInterpreterID, string ResultInterpreterName, string Parent)
        {

            testHl7MessageToTransmit.Append(strNewLineChar);
            testHl7MessageToTransmit.Append("OBR" + FieldSeparator);
            testHl7MessageToTransmit.Append(intSlNo + FieldSeparator); // OBR-1	Set ID	SI	R

            //NOTE:  ORC-2	PLACER ORDER NUMBER WE HAVE EMR ID OR VISIT ID WITH PROCEDURE CODE EACH ORDER NUMBER SOULD UNIQUE
            testHl7MessageToTransmit.Append(TEST_REPORT_ID + "_" + intSlNo + "^^" + SystemCode + FieldSeparator); // OBR-2	Placer Order Number	EI	R /EMR_ID + "_" + SLNo
            testHl7MessageToTransmit.Append(TEST_REPORT_ID + "_" + intSlNo + intSlNo + "^^" + SystemCode + FieldSeparator); // OBR-3	Filler Order Number	EI	O

            if (CodeTypeDesc != "")
            {
                testHl7MessageToTransmit.Append(LabCode + "^" + LabDesc + "^" + CodeTypeDesc + FieldSeparator); //  OBR-4	Universal Service Identifier	CE	R
            }
            else
            {
                testHl7MessageToTransmit.Append(LabCode + "^" + LabDesc + "^CPT" + FieldSeparator); //  OBR-4	Universal Service Identifier	CE	R

            }
            testHl7MessageToTransmit.Append(LabDesc + FieldSeparator);    //  OBR-5	Priority	ID	O 
            testHl7MessageToTransmit.Append(RequestedDateTime + FieldSeparator);      //  OBR-6	Requested Date/Time	 TS 	O          (1)
            testHl7MessageToTransmit.Append(ObservStart + FieldSeparator);      //   OBR-7	Observation Date/Time	 TS 	R  (2)

            testHl7MessageToTransmit.Append(ObservEnd + FieldSeparator);      //   OBR-8	Observation End Date/Time	 TS 	R (4)   //VerifiedDateTime

            testHl7MessageToTransmit.Append("" + FieldSeparator);      //   OBR-9	Collection Volume	CQ	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);      //   OBR-10	Collector Identifier	 XCN 	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);      //   OBR-11	Specimen Action Code	ID	O

            testHl7MessageToTransmit.Append("" + FieldSeparator);      //   OBR-12	Danger Code	CE	R
            testHl7MessageToTransmit.Append("" + FieldSeparator);      //   OBR-13	Relevant Clinical Information	ST	R

            testHl7MessageToTransmit.Append(DeliveryDateTime + FieldSeparator);      //   OBR-14	Specimen Received Date/Time	 TS 	R  (3)

            testHl7MessageToTransmit.Append("" + FieldSeparator);      //   OBR-15	Specimen Source	SPS	R
            if (OrderingProviderID != "")
            {
                // testHl7MessageToTransmit.Append(ConsultingDr + "^^" + ConsultingDrName + "^^Dr.^^^&" + SystemCode + "-DOHID" + FieldSeparator);//  ORC-12	Ordering Provider	 XCN 	R
                testHl7MessageToTransmit.Append(OrderingProviderID + "^" + OrderingProviderName + "^^^^^&" + SystemCode + "-DOHID" + FieldSeparator);//  ORC-12	Ordering Provider	 XCN 	R
            }
            else
            {
                testHl7MessageToTransmit.Append("" + FieldSeparator);//  OBR-16	Ordering Provider	 XCN 	R
            }

            testHl7MessageToTransmit.Append("" + FieldSeparator);      //OBR-17	Order Callback Phone Number	XTN	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);      //OBR-18	Placer Field 1	ST	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);      //OBR-19	Placer Field 2	ST	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);      //OBR-20	Filler Field 1	ST	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);      //OBR-21	Filler Field 2	ST	O
            testHl7MessageToTransmit.Append(VerifiedDateTime + FieldSeparator);      //OBR-22	Results Rpt/StatusChange Date/Time	 TS 	R //ObsDateTime (6)

            testHl7MessageToTransmit.Append("" + FieldSeparator);      //OBR-23	Charge to Practice	MOC	O
            testHl7MessageToTransmit.Append(DiagnosticService + FieldSeparator);      //OBR-24	Diagnostic Service Section ID	ID	R  //RAD	Radiology"LAB" 

            if (ResultStatus != "")
            {
                testHl7MessageToTransmit.Append(ResultStatus + FieldSeparator);      //OBR-25	Result Status	ID	
            }
            else
            {
                testHl7MessageToTransmit.Append("F" + FieldSeparator);      //OBR-25	Result Status	ID	R
            }

            if (intSlNo != 1)
            {
                testHl7MessageToTransmit.Append("600-7&Bacteria identified in Blood by Culture&LOINC^" + ObsSubID + FieldSeparator);      //OBR-26	Parent Result	PRL	O
            }
            else
            {
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //OBR-26	Parent Result	PRL	O
            }

            testHl7MessageToTransmit.Append("" + FieldSeparator);      //OBR-27	Quantity/Timing	TQ	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);      //OBR-28	Result Copies to	 XCN 	O
            testHl7MessageToTransmit.Append(Parent + FieldSeparator);      //OBR-29	Parent	EIP	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);       //OBR-30	Transportation Mode	ID	O
            testHl7MessageToTransmit.Append("" + FieldSeparator);       //OBR-31	Reason for Study	CE	O


            if (ResultInterpreterID != "")
            {
                //testHl7MessageToTransmit.Append(ConsultingDr + "&" + ResultInterpreter + "&&&&&&&" + SystemCode + "-DOHID");  //OBR-32	Principal Result Interpreter	NDL	R
                testHl7MessageToTransmit.Append(ResultInterpreterID + "&" + ResultInterpreterName + "&&&&&&" + SystemCode + "-DOHID");  //OBR-32	Principal Result Interpreter	NDL	R
            }

            //testHl7MessageToTransmit.Append("" + FieldSeparator);       //OBR-33	Assistant Result Interpreter	NDL	O
            //testHl7MessageToTransmit.Append("" + FieldSeparator);       //OBR-34	Technician	NDL	O
            //testHl7MessageToTransmit.Append("" + FieldSeparator);      //OBR-35	Transcriptionist	NDL	O






        }

        void BindObservationResult_OBX(Boolean MmicrobiologyValues, Int32 intSlNo, string LabCode, string LabDesc, string strSysCode, string ObsSubID, string ObsValue, string ObsUnit, string TestDateTime, string AbnormalFlag, string Range, string ResultStatus, string OrderingProviderID, string OrderingProviderName)
        {

            if (ObsValue != "")
            {
                testHl7MessageToTransmit.Append(strNewLineChar);
                testHl7MessageToTransmit.Append("OBX" + FieldSeparator);
                testHl7MessageToTransmit.Append(intSlNo + FieldSeparator); // OBX-1	Set ID	SI	R

                int vNu;
                Decimal vDec;
                bool isNumeric = int.TryParse(ObsValue, out vNu);
                bool isDec = Decimal.TryParse(ObsValue, out vDec);

                if (isNumeric == true)
                {
                    testHl7MessageToTransmit.Append("NM" + FieldSeparator);      //   OBX-2	Value Type	ID	R
                }
                else if (isDec == true)
                {
                    testHl7MessageToTransmit.Append("CE" + FieldSeparator);      //   OBX-2	Value Type	ID	R
                }
                else
                {
                    testHl7MessageToTransmit.Append("ST" + FieldSeparator);      //   OBX-2	Value Type	ID	R
                }



                testHl7MessageToTransmit.Append(LabCode + "^" + LabDesc + "^" + strSysCode + FieldSeparator);     //   OBX-3	Observation Identifier	CE	R
                testHl7MessageToTransmit.Append(ObsSubID + FieldSeparator);      //   OBX-4	Observation Sub ID	ST	O

                //if (isNumeric == true || isDec == true)
                //{
                //    testHl7MessageToTransmit.Append(ObsValue + FieldSeparator);      //   OBX-5	Observation Value	Variable	R
                //}
                //else
                //{
                //    testHl7MessageToTransmit.Append(ObsValue + "^^" + SystemCode + FieldSeparator);      //   OBX-5	Observation Value	Variable	R
                //}

                if (isNumeric == true)
                {
                    testHl7MessageToTransmit.Append(ObsValue + FieldSeparator);      //   OBX-5	Observation Value	Variable	R
                }
                else if (isDec == true)
                {
                    testHl7MessageToTransmit.Append(ObsValue + "^^" + SystemCode + FieldSeparator);      //   OBX-5	Observation Value	Variable	R
                }
                else
                {
                    testHl7MessageToTransmit.Append(ObsValue + FieldSeparator);      //   OBX-5	Observation Value	Variable	R
                }



                if (ObsUnit == "") //MmicrobiologyValues == true ||
                {
                    testHl7MessageToTransmit.Append(ObsUnit + FieldSeparator);      //   OBX-6	Units	CE	R
                }
                else
                {
                    testHl7MessageToTransmit.Append(ObsUnit + "^" + ObsUnit + "^" + SystemCode + FieldSeparator);      //   OBX-6	Units	CE	R
                }

                testHl7MessageToTransmit.Append(Range + FieldSeparator);      //   OBX-7	References Range	ST	R
                testHl7MessageToTransmit.Append(AbnormalFlag + FieldSeparator);      //   OBX-8	Abnormal Flags	IS	R  "N" 
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //   OBX-9	Probability	NM	O
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //   OBX-10	Nature of Abnormal Test	ID	O


                if (ResultStatus != "")
                {
                    if (ResultStatus == "X")
                    {
                        testHl7MessageToTransmit.Append("D" + FieldSeparator);      //   OBX-11	Observation Result Status	ID	R      // F	Final results

                    }
                    else
                    {
                        testHl7MessageToTransmit.Append(ResultStatus + FieldSeparator);      //   OBX-11	Observation Result Status	ID	R      // F	Final results
                    }
                }
                else
                {
                    testHl7MessageToTransmit.Append("F" + FieldSeparator);      //   OBX-11	Observation Result Status	ID	R      // F	Final results
                }

                testHl7MessageToTransmit.Append("" + FieldSeparator);      //   OBX-12	Effective Date of Reference Range Values	 TS 	O
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //   OBX-13	User-defined Access Checks	ST	O
                testHl7MessageToTransmit.Append(TestDateTime + FieldSeparator);      //   OBX-14	Date/Time of the Observation	 TS 	R //ObsDateTime
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //   OBX-15	Producer's Reference	CE	O


                if (OrderingProviderID != "")
                {

                    testHl7MessageToTransmit.Append(OrderingProviderID + "^" + OrderingProviderName + "^^^^^&" + SystemCode + "-DOHID" + FieldSeparator);//  OBR-16	Ordering Provider	 XCN 	R
                }
                else
                {
                    testHl7MessageToTransmit.Append("" + FieldSeparator);      //   OBX-16	Responsible Observer	 XCN 	R
                }



                testHl7MessageToTransmit.Append("" + FieldSeparator);      //   OBX-17	Observation Method	CE	O
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //   OBX-18	Equipment Instance Identifier	EI	O
                testHl7MessageToTransmit.Append(TestDateTime + FieldSeparator);      //   OBX-19	Date/Time of the Analysis	 TS 	R //ObsDateTime
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //   OBX-20	RESERVED	ST	O
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //   OBX-21	RESERVED	ST	O
                testHl7MessageToTransmit.Append("" + FieldSeparator);      //   OBX-22	RESERVED	ST	O
                testHl7MessageToTransmit.Append(BRANCH_NAME + "^^" + PROVIDER_ID + "^^^&" + SystemCode + "-DOHID");      //   OBX-23	Performing Organization Name	XON	R



            }

        }

        void BindNotesAndComments_NTE(string CollectionMethod)
        {
            testHl7MessageToTransmit.Append(strNewLineChar);
            testHl7MessageToTransmit.Append("NTE" + FieldSeparator);
            testHl7MessageToTransmit.Append("1" + FieldSeparator); // NTE-1	Set ID	SI	O
            testHl7MessageToTransmit.Append("L" + FieldSeparator); // NTE-2	Source of comment	O
            testHl7MessageToTransmit.Append("SPECIMEN TYPE: " + CollectionMethod + FieldSeparator); // NTE-3	Comment	O
            testHl7MessageToTransmit.Append("RE^Remarks^MALAFFI"); // NTE-4	Comment Type	O
        }

        void BindNotesAndCommentsForVitalsign_NTE(string ObservationType, string CollectionMethod)
        {
            testHl7MessageToTransmit.Append(strNewLineChar);
            testHl7MessageToTransmit.Append("NTE" + FieldSeparator);
            testHl7MessageToTransmit.Append("1" + FieldSeparator); // NTE-1	Set ID	SI	O
            testHl7MessageToTransmit.Append("L" + FieldSeparator); // NTE-2	Source of comment	O
            testHl7MessageToTransmit.Append(ObservationType + ": " + CollectionMethod + FieldSeparator); // NTE-3	Comment	O
            testHl7MessageToTransmit.Append("RE^Remarks^MALAFFI"); // NTE-4	Comment Type	O
        }

        void MessageHeader()
        {
            string strDate = "", strTime = "";
            strDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff");
            string[] arrDate;
            arrDate = strDate.Split(' ');
            string strDate1 = arrDate[0];
            strTime = arrDate[1];
            string[] arrDate1 = strDate1.Split('/');
            string strTimeStamp = "";
            strTimeStamp = arrDate1[2] + arrDate1[1] + arrDate1[0] + strTime.Replace(":", "").Replace(".", "");


            if (EMR_ID != "")
            {
                MessageControlID = PT_ID.Replace("/", "_") + '-' + EMR_ID + '-' + strTimeStamp;
            }
            else
            {
                MessageControlID = PT_ID.Replace("/", "_") + '-' + VISIT_ID + '-' + strTimeStamp;
            }


            if (MessageEvent == "ADT-A28" || MessageEvent == "ADT-A31")
            {
                MessageControlID = PT_ID.Replace("/", "_") + '-' + strTimeStamp;
            }


            if (MessageEvent == "ORU-R01")
            {
                if (RAD_TEST_REPORT_ID != "" && RAD_TEST_REPORT_ID != null)
                {

                    MessageControlID = PT_ID.Replace("/", "_") + "-" + RAD_TEST_REPORT_ID + '-' + strTimeStamp;
                }

                if (LAB_TEST_REPORT_ID != "" && LAB_TEST_REPORT_ID != null)
                {

                    MessageControlID = PT_ID.Replace("/", "_") + "-" + LAB_TEST_REPORT_ID + '-' + strTimeStamp;
                }
            }


            if (MessageEvent == "ADT-A01" || MessageEvent == "ADT-A02" || MessageEvent == "ADT-A06")
            {
                MessageControlID = PT_ID.Replace("/", "_") + "-" + IP_ADMISSION_NO + '-' + strTimeStamp;
            }

            string strMonth = DateTime.Now.Month.ToString();
            if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

            string strDay = DateTime.Now.Day.ToString();
            if (strDay.Length == 1) { strDay = "0" + strDay; }

            string strHour = DateTime.Now.Hour.ToString();
            if (strHour.Length == 1) { strHour = "0" + strHour; }

            string strMinute = DateTime.Now.Minute.ToString();
            if (strMinute.Length == 1) { strMinute = "0" + strMinute; }


            DateTimeOfMessage = DateTime.Now.Year.ToString() + strMonth + strDay + strHour + strMinute + "00+0400";

            RecordedDateTime = DateTime.Now.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";


            testHl7MessageToTransmit.Append("MSH" + FieldSeparator);
            testHl7MessageToTransmit.Append(EncodingCharacters + FieldSeparator);
            testHl7MessageToTransmit.Append(SystemCode + "^" + SystemCode + FieldSeparator);
            testHl7MessageToTransmit.Append(SystemCode + "^" + SystemCode + FieldSeparator);
            testHl7MessageToTransmit.Append(ReceivingApplication + FieldSeparator);
            testHl7MessageToTransmit.Append(ReceivingFacility + FieldSeparator);
            testHl7MessageToTransmit.Append(DateTimeOfMessage + FieldSeparator); //Date/Time of Message  R
            testHl7MessageToTransmit.Append(Security);  //Security

            testHl7MessageToTransmit.Append(MessageType + FieldSeparator); //Message Type
            testHl7MessageToTransmit.Append(MessageControlID + FieldSeparator);
            testHl7MessageToTransmit.Append(ProcessingID + FieldSeparator);
            testHl7MessageToTransmit.Append(VersionID);

        FunEnd: ;
        }

        byte[] Combine(byte[] a1, byte[] a2, byte[] a3)
        {
            byte[] ret = new byte[a1.Length + a2.Length + a3.Length];
            Array.Copy(a1, 0, ret, 0, a1.Length);
            Array.Copy(a2, 0, ret, a1.Length, a2.Length);
            Array.Copy(a3, 0, ret, a1.Length + a2.Length, a3.Length);
            return ret;
        }

        void SendHL7Message(string Message, string MessageType, out string ReturnMessage, out Int32 MessageSent)
        {
            MessageSent = 0;
            ReturnMessage = "";
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 "; // HMC_MSG_SEND_TYPE='UAT' ";  //HMC_STATUS='A' AND
            DataSet DSMalaffiConfig = new DataSet();
            DSMalaffiConfig = objCom.fnGetFieldValue("*", "HMS_MALAFFI_CONFIGURATION", Criteria, "");
            LogFileWriting("SendHL7Message start - Message Type " + MessageType);
            if (DSMalaffiConfig.Tables[0].Rows.Count > 0)
            {


                string strIP = "";
                Int32 intPort = 41182;

                if (MessageType == "ADT")
                {
                    if (DSMalaffiConfig.Tables[0].Rows[0].IsNull("HMC_ADT_IP") == false && Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_ADT_IP"]) != "")
                    {
                        strIP = Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_ADT_IP"]);
                    }

                    if (DSMalaffiConfig.Tables[0].Rows[0].IsNull("HMC_ADT_PORT") == false && Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_ADT_PORT"]) != "")
                    {
                        intPort = Convert.ToInt32(DSMalaffiConfig.Tables[0].Rows[0]["HMC_ADT_PORT"]);
                    }
                }

                if (MessageType == "RESULT" || MessageType == "RESULT_VITAL")
                {
                    if (DSMalaffiConfig.Tables[0].Rows[0].IsNull("HMC_RESULT_IP") == false && Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_RESULT_IP"]) != "")
                    {
                        strIP = Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_RESULT_IP"]);
                    }

                    if (DSMalaffiConfig.Tables[0].Rows[0].IsNull("HMC_RESULT_PORT") == false && Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_RESULT_PORT"]) != "")
                    {
                        intPort = Convert.ToInt32(DSMalaffiConfig.Tables[0].Rows[0]["HMC_RESULT_PORT"]);
                    }
                }

                if (MessageType == "PROB")
                {
                    if (DSMalaffiConfig.Tables[0].Rows[0].IsNull("HMC_PROB_IP") == false && Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_PROB_IP"]) != "")
                    {
                        strIP = Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_PROB_IP"]);
                    }

                    if (DSMalaffiConfig.Tables[0].Rows[0].IsNull("HMC_PROB_PORT") == false && Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_PROB_PORT"]) != "")
                    {
                        intPort = Convert.ToInt32(DSMalaffiConfig.Tables[0].Rows[0]["HMC_PROB_PORT"]);
                    }
                }

                if (MessageType == "MEDS")
                {
                    if (DSMalaffiConfig.Tables[0].Rows[0].IsNull("HMC_MEDS_IP") == false && Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_MEDS_IP"]) != "")
                    {
                        strIP = Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_MEDS_IP"]);
                    }

                    if (DSMalaffiConfig.Tables[0].Rows[0].IsNull("HMC_MEDS_PORT") == false && Convert.ToString(DSMalaffiConfig.Tables[0].Rows[0]["HMC_MEDS_PORT"]) != "")
                    {
                        intPort = Convert.ToInt32(DSMalaffiConfig.Tables[0].Rows[0]["HMC_MEDS_PORT"]);
                    }
                }

                string host = strIP;
                int port = intPort;
                LogFileWriting(" Host IP and Port " + strIP + " " + intPort);

                TcpClient tcpclnt = new TcpClient();
                // string str = "MSH|^~\\&||.|||199908180016||ADT^A04|ADT.1.1698593|P|2.5\r\nPID|1||000395122||PEREZ^ADRIAN^C^^^||19880517180606|M|";
                try
                {
                    System.Net.ServicePointManager.Expect100Continue = false;
                    tcpclnt.Connect(host, port);
                    LogFileWriting("Connected ");
                    ASCIIEncoding asen = new ASCIIEncoding();
                    byte[] b1 = { 0x0B };
                    byte[] b2 = { 0x1C, 0x0D };

                    // add header an tail to message string

                    byte[] ba = Combine(b1, asen.GetBytes(Message), b2);
                    Stream stm = tcpclnt.GetStream();
                    stm.Write(ba, 0, ba.Length);

                    LogFileWriting("Write in Streem ");

                    // LogFileWriting("60 Sec sleeping....");

                    // System.Threading.Thread.Sleep(60000);
                    LogFileWriting("SendHL7Message  1");
                    byte[] bb = new byte[6000];
                    LogFileWriting("SendHL7Message  2");
                    int k = stm.Read(bb, 0, 6000);
                    LogFileWriting("SendHL7Message  3");

                    LogFileWriting("Read   Streem ");

                    ReturnMessage = System.Text.Encoding.UTF8.GetString(bb, 0, k - 1);

                    ReturnMessage = ReturnMessage.Replace("'", "");

                    LogFileWriting("Get  ReturnMessage");
                    tcpclnt.Close();
                    LogFileWriting("Connection Closed");

                    MessageSent = 1;
                    LogFileWriting(System.DateTime.Now.ToString() + " SendHL7Message ()  " + ReturnMessage);

                    if (ReturnMessage.Contains("MSH") && ReturnMessage.Contains("ERR"))
                    {
                        MessageSent = 2;
                    }

                    LogFileWriting("MessageSent Value = " + MessageSent);
                }
                catch (Exception ex)
                {
                    LogFileWriting(System.DateTime.Now.ToString() + " SendHL7Message () Error Message   " + ex.Message);
                    MessageSent = 0;
                    LogFileWriting("MessageSent Value = " + MessageSent);
                }


            }
            else
            {
                LogFileWriting("HMS_MALAFFI_CONFIGURATION No data " + Criteria);
                MessageSent = 0;
            }


        }

        string RemoveSplChar(string Data)
        {
            string strReturnValue = "";
            Data = Data.Replace(@"\", @"\E\"); //
            Data = Data.Replace("|", @"\F\");
            Data = Data.Replace("^", @"\S\");
            Data = Data.Replace("~", @"\R\");
            Data = Data.Replace("&", @"\T\");

            strReturnValue = Data;
            return strReturnValue;
        }
        #endregion


        #region Events
        public Boolean PTRegisterMessageGenerate_ADT_A04()
        {
            Boolean isError = false;
            MessageType = "ADT^A04";
            MessageEvent = "ADT-A04";
            MESSAGE_TYPE = "ADT";

            LogFileWriting("****************** A04 " + System.DateTime.Now.ToString() + "******************");

            GetPatientDetails();
            GetPatientVisitDtls();
            BindEMRPTMaster();
            // GetPatientClass();

            if (ValidateNL7Message() == false)
            {
                isError = true;
            }


            testHl7MessageToTransmit = new StringBuilder();



            MessageHeader();
            BindEventType("A04");

            BindPatientIdentification_PID();

            BindNextofKinOrAssociatedParties();

            BindPatientVisitAdditionalInformation_PV1();

            BindPTAllergyInformation_AL1();

            //NOTE:  A04 (Register a patient) will not have always all the diagnosis and procedures 
            //BindPTDiagnosis_DG1();
            //BindPTProcedures();



            BindInsurance_IN1();



            string FileName = MessageEvent + "_" + MessageControlID;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            string ReturnMessage = "";
            Int32 MessageSent = 0;

            Boolean SendToMalaffi = false;
            string[] arrMessageTypes = UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrMessageTypes.Length; intSegType++)
            {
                if (arrMessageTypes[intSegType] == MessageEvent) //MESSAGE_TYPE
                {
                    SendToMalaffi = true;
                }
            }

            if (SendToMalaffi == true)
            {
                SendHL7Message(testHl7MessageToTransmit.ToString(), MESSAGE_TYPE, out  ReturnMessage, out  MessageSent);
            }
            else
            {
                MessageSent = 3;
            }

            LogFileWriting("HL7File Created file name is  " + FileName);

            MessageID = MessageControlID;
            HL7FileName = FileName;
            MessageCode = MessageEvent; // "ADT-A04";
            MessageDesc = "Register A Patient";

            RETURN_MESSAGE = ReturnMessage.Replace("'", "");
            //  MalaffiHL7MessageMasterAdd();


            CommonBAL objCom = new CommonBAL();
            string Criteria = " HMHM_UPLOAD_STATUS ='PENDING' ";
            Criteria += " AND HMHM_BRANCH_ID='" + BRANCH_ID + "' AND HMHM_PT_ID='" + PT_ID + "' AND HMHM_EMR_ID='" + EMR_ID + "'";
            Criteria += " AND HMHM_MESSAGE_TYPE='" + MESSAGE_TYPE + "' and HMHM_MESSAGE_CODE='" + MessageCode + "'";

            string FieldNameWithValues = "HMHM_MESSAGE_ID='" + MessageControlID + "', HMHM_FILE_NAME='" + FileName + "' ";

            if (MessageSent == 1)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'UPLOADED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }
            if (MessageSent == 2)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'ERROR' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            if (MessageSent == 3)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'FILE_CREATED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            string FieldNameWithValues1 = "HMHM_RETURN_MESSAGE='" + RETURN_MESSAGE + "'";
            objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            if (isError == true)
            {
                return false;
            }


            return true;
        }


        public Boolean AddPatientInfoMessageGenerate_ADT_A28()
        {
            Boolean isError = false;
            MessageType = "ADT^A28";
            MessageEvent = "ADT-A28";
            MESSAGE_TYPE = "ADT";

            LogFileWriting("****************** A28 " + System.DateTime.Now.ToString() + "******************");

            GetPatientDetails();
            // GetPatientVisitDtls();
            // BindEMRPTMaster();
            //  GetPatientClass();



            if (ValidateNL7Message() == false)
            {
                isError = true;
            }


            testHl7MessageToTransmit = new StringBuilder();



            MessageHeader();
            BindEventType("A28");
            BindPatientIdentification_PID();
            BindNextofKinOrAssociatedParties();


            BindInsurance_IN1();

            /* NOTE: ADT-A28 should be triggered once the patient is registered for the first time in the facility, ADT-A31 should be triggered
                     when there is any update on patient demographics like address, phone number, insurance information, allergy information, etc. 
                    in the shared messages these two events are triggered with all encounter information such as Procedures, diagnosis, etc. which is wrong.
             */
            //// BindPTDiagnosis_DG1();
            ////  BindPTProcedures();


            string FileName = MessageEvent + "_" + MessageControlID;

            Boolean SendToMalaffi = false;
            string[] arrMessageTypes = UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrMessageTypes.Length; intSegType++)
            {
                if (arrMessageTypes[intSegType] == MessageEvent) //MESSAGE_TYPE
                {
                    SendToMalaffi = true;
                }
            }


            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            string ReturnMessage = "";
            Int32 MessageSent = 0;



            if (SendToMalaffi == true)
            {
                SendHL7Message(testHl7MessageToTransmit.ToString(), MESSAGE_TYPE, out  ReturnMessage, out  MessageSent);

            }
            else
            {
                MessageSent = 3;
            }


            LogFileWriting("HL7File Created file name is  " + FileName);

            MessageID = MessageControlID;
            HL7FileName = FileName;
            MessageCode = MessageEvent; // "ADT-A28";
            MessageDesc = "Add Patient Information";

            RETURN_MESSAGE = ReturnMessage.Replace("'", "");
            // MalaffiHL7MessageMasterAdd();

            CommonBAL objCom = new CommonBAL();
            string Criteria = " HMHM_UPLOAD_STATUS ='PENDING' ";
            Criteria += " AND HMHM_BRANCH_ID='" + BRANCH_ID + "' AND HMHM_PT_ID='" + PT_ID + "'";// AND HMHM_VISIT_ID='" + VISIT_ID + "'";
            Criteria += " AND HMHM_MESSAGE_TYPE='" + MESSAGE_TYPE + "' and HMHM_MESSAGE_CODE='" + MessageCode + "'";

            string FieldNameWithValues = "HMHM_MESSAGE_ID='" + MessageControlID + "', HMHM_FILE_NAME='" + FileName + "' ";
            if (MessageSent == 1)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'UPLOADED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }
            if (MessageSent == 2)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'ERROR' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            if (MessageSent == 3)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'FILE_CREATED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            string FieldNameWithValues1 = "HMHM_RETURN_MESSAGE='" + RETURN_MESSAGE + "'";
            objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);


            if (isError == true)
            {
                return false;
            }


            return true;
        }

        public Boolean UpdatePatientInfoMessageGenerate_ADT_A31()
        {
            Boolean isError = false;
            MessageType = "ADT^A31";
            MessageEvent = "ADT-A31";
            MESSAGE_TYPE = "ADT";

            LogFileWriting("******************  A31 " + System.DateTime.Now.ToString() + "******************");

            GetPatientDetails();
            // GetPatientVisitDtls();
            // BindEMRPTMaster();
            // GetPatientClass();



            if (ValidateNL7Message() == false)
            {
                isError = true;

            }


            testHl7MessageToTransmit = new StringBuilder();



            MessageHeader();
            BindEventType("A31");
            BindPatientIdentification_PID();

            BindNextofKinOrAssociatedParties();


            BindPTAllergyInformation_AL1();
            BindInsurance_IN1();

            /* NOTE: ADT-A28 should be triggered once the patient is registered for the first time in the facility, ADT-A31 should be triggered
                   when there is any update on patient demographics like address, phone number, insurance information, allergy information, etc. 
                  in the shared messages these two events are triggered with all encounter information such as Procedures, diagnosis, etc. which is wrong.
           */
            //// BindPTDiagnosis_DG1();
            ////  BindPTProcedures();




            string FileName = MessageEvent + "_" + MessageControlID;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            string ReturnMessage = "";
            Int32 MessageSent = 0;


            Boolean SendToMalaffi = false;
            string[] arrMessageTypes = UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrMessageTypes.Length; intSegType++)
            {
                if (arrMessageTypes[intSegType] == MessageEvent) //MESSAGE_TYPE
                {
                    SendToMalaffi = true;
                }
            }

            if (SendToMalaffi == true)
            {
                SendHL7Message(testHl7MessageToTransmit.ToString(), MESSAGE_TYPE, out  ReturnMessage, out  MessageSent);
            }
            else
            {
                MessageSent = 3;
            }

            LogFileWriting("HL7File Created file name is  " + FileName);

            MessageID = MessageControlID;
            HL7FileName = FileName;
            MessageCode = MessageEvent; // "ADT-A31";
            MessageDesc = "Update Patient Information";

            RETURN_MESSAGE = ReturnMessage.Replace("'", "");
            // MalaffiHL7MessageMasterAdd();


            CommonBAL objCom = new CommonBAL();
            string Criteria = " HMHM_UPLOAD_STATUS ='PENDING' ";
            Criteria += " AND HMHM_BRANCH_ID='" + BRANCH_ID + "' AND HMHM_PT_ID='" + PT_ID + "'";// AND HMHM_VISIT_ID='" + VISIT_ID + "'";
            Criteria += " AND HMHM_MESSAGE_TYPE='" + MESSAGE_TYPE + "' and HMHM_MESSAGE_CODE='" + MessageCode + "'";

            string FieldNameWithValues = "HMHM_MESSAGE_ID='" + MessageControlID + "', HMHM_FILE_NAME='" + FileName + "' ";
            if (MessageSent == 1)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'UPLOADED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }
            if (MessageSent == 2)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'ERROR' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            if (MessageSent == 3)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'FILE_CREATED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            string FieldNameWithValues1 = "HMHM_RETURN_MESSAGE='" + RETURN_MESSAGE + "'";
            objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);


            if (isError == true)
            {
                return false;
            }


            return true;
        }

        public Boolean CancelAdmit_visitMessageGenerate_ADT_A11()
        {
            Boolean isError = false;
            MessageType = "ADT^A11";
            MessageEvent = "ADT-A11";
            MESSAGE_TYPE = "ADT";

            LogFileWriting("****************** A11 " + System.DateTime.Now.ToString() + "******************");

            GetPatientDetails();
            GetPatientVisitDtls();
            if (EMR_ID != "")
            {
                BindEMRPTMaster();
            }
            // GetPatientClass();

            if (ValidateNL7Message() == false)
            {
                isError = true;
            }


            testHl7MessageToTransmit = new StringBuilder();





            MessageHeader();
            BindEventType("A11");


            BindPatientIdentification_PID();
            BindNextofKinOrAssociatedParties();
            BindPatientVisitAdditionalInformation_PV1();

            // BindPTDiagnosis_DG1();

            string strDate = "", strTime = "";
            strDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            string[] arrDate;
            arrDate = strDate.Split(' ');
            string strDate1 = arrDate[0];
            strTime = arrDate[1];

            string[] arrDate1 = strDate1.Split('/');
            string strTimeStamp = "";
            strTimeStamp = arrDate1[2] + arrDate1[1] + arrDate1[0] + strTime.Replace(":", "");



            string FileName = MessageEvent + "_" + MessageControlID; // + PROVIDER_ID + "-" + PT_ID.Replace("/", "_") + "-" + VISIT_ID + "-" + EMR_ID + '-' + strTimeStamp;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            string ReturnMessage = "";
            Int32 MessageSent = 0;

            Boolean SendToMalaffi = false;
            string[] arrMessageTypes = UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrMessageTypes.Length; intSegType++)
            {
                if (arrMessageTypes[intSegType] == MessageEvent) //MESSAGE_TYPE
                {
                    SendToMalaffi = true;
                }
            }

            if (SendToMalaffi == true)
            {
                SendHL7Message(testHl7MessageToTransmit.ToString(), MESSAGE_TYPE, out  ReturnMessage, out  MessageSent);
            }
            else
            {
                MessageSent = 3;
            }

            LogFileWriting("HL7File Created file name is  " + FileName);

            MessageID = MessageControlID;
            HL7FileName = FileName;
            MessageCode = MessageEvent;// "ADT-A11";
            MessageDesc = "Cancel Admit/visit Notification";

            RETURN_MESSAGE = ReturnMessage.Replace("'", "");
            // MalaffiHL7MessageMasterAdd();


            CommonBAL objCom = new CommonBAL();
            string Criteria = " HMHM_UPLOAD_STATUS ='PENDING' ";
            Criteria += " AND HMHM_BRANCH_ID='" + BRANCH_ID + "' AND HMHM_PT_ID='" + PT_ID + "' AND HMHM_VISIT_ID='" + VISIT_ID + "'";
            Criteria += " AND HMHM_MESSAGE_TYPE='" + MESSAGE_TYPE + "' and HMHM_MESSAGE_CODE='" + MessageCode + "'";

            string FieldNameWithValues = "HMHM_MESSAGE_ID='" + MessageControlID + "', HMHM_FILE_NAME='" + FileName + "' ";
            if (MessageSent == 1)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'UPLOADED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }
            if (MessageSent == 2)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'ERROR' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            if (MessageSent == 3)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'FILE_CREATED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            string FieldNameWithValues1 = "HMHM_RETURN_MESSAGE='" + RETURN_MESSAGE + "'";
            objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            if (isError == true)
            {
                return false;
            }

            return true;
        }




        public Boolean Admit_VisitMessageGenerate_ADT_A01()
        {
            Boolean isError = false;
            MessageType = "ADT^A01";
            MessageEvent = "ADT-A01";
            MESSAGE_TYPE = "ADT";

            LogFileWriting("****************** A01 " + System.DateTime.Now.ToString() + "******************");

            GetPatientDetails();
            GetIPAdmissionSummary();
            // BindEMRPTMaster();
            // GetPatientClass();

            if (ValidateNL7Message() == false)
            {
                isError = true;
            }


            testHl7MessageToTransmit = new StringBuilder();



            MessageHeader();
            BindEventType("A01");

            BindPatientIdentification_PID();

            BindNextofKinOrAssociatedParties();

            BindIP_PatientVisitAdditionalInformation_PV1();

            BindPTAllergyInformation_AL1();

            //NOTE:  A01 (Register a patient) will not have always all the diagnosis and procedures 
            //BindPTDiagnosis_DG1();
            //BindPTProcedures();



            BindInsurance_IN1();



            string FileName = MessageEvent + "_" + MessageControlID;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            string ReturnMessage = "";
            Int32 MessageSent = 0;

            Boolean SendToMalaffi = false;
            string[] arrMessageTypes = UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrMessageTypes.Length; intSegType++)
            {
                if (arrMessageTypes[intSegType] == MessageEvent) //MESSAGE_TYPE
                {
                    SendToMalaffi = true;
                }
            }

            if (SendToMalaffi == true)
            {
                SendHL7Message(testHl7MessageToTransmit.ToString(), MESSAGE_TYPE, out  ReturnMessage, out  MessageSent);
            }
            else
            {
                MessageSent = 3;
            }

            LogFileWriting("HL7File Created file name is  " + FileName);

            MessageID = MessageControlID;
            HL7FileName = FileName;
            MessageCode = MessageEvent;
            MessageDesc = "Admit/visit Notification";

            RETURN_MESSAGE = ReturnMessage.Replace("'", "");
            //  MalaffiHL7MessageMasterAdd();


            CommonBAL objCom = new CommonBAL();
            string Criteria = " HMHM_UPLOAD_STATUS ='PENDING' ";
            Criteria += " AND HMHM_BRANCH_ID='" + BRANCH_ID + "' AND HMHM_PT_ID='" + PT_ID + "' AND HMHM_IP_ADMISSION_NO='" + IP_ADMISSION_NO + "'";
            Criteria += " AND HMHM_MESSAGE_TYPE='" + MESSAGE_TYPE + "' and HMHM_MESSAGE_CODE='" + MessageCode + "'";

            string FieldNameWithValues = "HMHM_MESSAGE_ID='" + MessageControlID + "', HMHM_FILE_NAME='" + FileName + "' ";
            if (MessageSent == 1)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'UPLOADED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }
            if (MessageSent == 2)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'ERROR' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            if (MessageSent == 3)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'FILE_CREATED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            string FieldNameWithValues1 = "HMHM_RETURN_MESSAGE='" + RETURN_MESSAGE + "'";
            objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            if (isError == true)
            {
                return false;
            }


            return true;
        }

        public Boolean ChangeOutpatientToInpatientMessageGenerate_ADT_A06()
        {
            Boolean isError = false;
            MessageType = "ADT^A06";
            MessageEvent = "ADT-A06";
            MESSAGE_TYPE = "ADT";

            LogFileWriting("****************** A06 " + System.DateTime.Now.ToString() + "******************");

            GetPatientDetails();
            GetIPAdmissionSummary();
            // BindEMRPTMaster();
            // GetPatientClass();

            if (ValidateNL7Message() == false)
            {
                isError = true;
            }


            testHl7MessageToTransmit = new StringBuilder();



            MessageHeader();
            BindEventType("A06");

            BindPatientIdentification_PID();

            BindNextofKinOrAssociatedParties();

            BindIP_PatientVisitAdditionalInformation_PV1();

            BindPTAllergyInformation_AL1();

            //NOTE:  A06 (Register a patient) will not have always all the diagnosis and procedures 
            //BindPTDiagnosis_DG1();
            //BindPTProcedures();



            BindInsurance_IN1();



            string FileName = MessageEvent + "_" + MessageControlID;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            string ReturnMessage = "";
            Int32 MessageSent = 0;

            Boolean SendToMalaffi = false;
            string[] arrMessageTypes = UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrMessageTypes.Length; intSegType++)
            {
                if (arrMessageTypes[intSegType] == MessageEvent) //MESSAGE_TYPE
                {
                    SendToMalaffi = true;
                }
            }

            if (SendToMalaffi == true)
            {
                SendHL7Message(testHl7MessageToTransmit.ToString(), MESSAGE_TYPE, out  ReturnMessage, out  MessageSent);
            }
            else
            {
                MessageSent = 3;
            }

            LogFileWriting("HL7File Created file name is  " + FileName);

            MessageID = MessageControlID;
            HL7FileName = FileName;
            MessageCode = MessageEvent;
            MessageDesc = "Admit/visit Notification";

            RETURN_MESSAGE = ReturnMessage.Replace("'", "");
            //  MalaffiHL7MessageMasterAdd();


            CommonBAL objCom = new CommonBAL();
            string Criteria = " HMHM_UPLOAD_STATUS ='PENDING' ";
            Criteria += " AND HMHM_BRANCH_ID='" + BRANCH_ID + "' AND HMHM_PT_ID='" + PT_ID + "' AND HMHM_IP_ADMISSION_NO='" + IP_ADMISSION_NO + "'";
            Criteria += " AND HMHM_MESSAGE_TYPE='" + MESSAGE_TYPE + "' and HMHM_MESSAGE_CODE='" + MessageCode + "'";

            string FieldNameWithValues = "HMHM_MESSAGE_ID='" + MessageControlID + "', HMHM_FILE_NAME='" + FileName + "' ";
            if (MessageSent == 1)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'UPLOADED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }
            if (MessageSent == 2)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'ERROR' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            if (MessageSent == 3)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'FILE_CREATED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            string FieldNameWithValues1 = "HMHM_RETURN_MESSAGE='" + RETURN_MESSAGE + "'";
            objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            if (isError == true)
            {
                return false;
            }


            return true;
        }

        public Boolean TransferPatientMessageGenerate_ADT_A02()
        {
            Boolean isError = false;
            LogFileWriting("****************** A02 " + System.DateTime.Now.ToString() + "******************");

            GetPatientDetails();
            GetPatientVisitDtls();
            BindEMRPTMaster();
            //  GetPatientClass();

            if (ValidateNL7Message() == false)
            {
                isError = true;
            }


            testHl7MessageToTransmit = new StringBuilder();



            MessageEvent = "ADT-A02";
            MessageType = "ADT^A02";
            MessageHeader();
            BindEventType("A02");


            BindPatientIdentification_PID();
            BindNextofKinOrAssociatedParties();
            BindPatientVisitAdditionalInformation_PV1();


            string strDate = "", strTime = "";
            strDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            string[] arrDate;
            arrDate = strDate.Split(' ');
            string strDate1 = arrDate[0];
            strTime = arrDate[1];

            string[] arrDate1 = strDate1.Split('/');
            string strTimeStamp = "";
            strTimeStamp = arrDate1[2] + arrDate1[1] + arrDate1[0] + strTime.Replace(":", "");



            string FileName = MessageEvent + "_" + MessageControlID; // + PROVIDER_ID + "-" + PT_ID.Replace("/", "_") + "-" + VISIT_ID + "-" + EMR_ID + '-' + strTimeStamp;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            string ReturnMessage = "";
            Int32 MessageSent = 0;
            SendHL7Message(testHl7MessageToTransmit.ToString(), "ADT", out  ReturnMessage, out  MessageSent);
            LogFileWriting("HL7File Created file name is  " + FileName);

            MessageID = MessageControlID;
            HL7FileName = FileName;
            MessageCode = MessageEvent;
            MessageDesc = "Transfer A Patient";
            MESSAGE_TYPE = "ADT";
            RETURN_MESSAGE = ReturnMessage.Replace("'", "");
            MalaffiHL7MessageMasterAdd();

            if (isError == true)
            {
                return false;
            }

            return true;

        }

        public Boolean DischargeEndVisitMessageGenerate_ADT_A03()
        {
            Boolean isError = false;
            MessageType = "ADT^A03";
            MessageEvent = "ADT-A03";
            MESSAGE_TYPE = "ADT";

            LogFileWriting("****************** A03 " + System.DateTime.Now.ToString() + "******************");

            GetPatientDetails();
            GetPatientVisitDtls();
            BindEMRPTMaster();
            // GetPatientClass();

            if (ValidateNL7Message() == false)
            {
                isError = true;
            }


            testHl7MessageToTransmit = new StringBuilder();





            MessageHeader();
            BindEventType("A03");


            BindPatientIdentification_PID();
            BindNextofKinOrAssociatedParties();
            BindPatientVisitAdditionalInformation_PV1();
            BindPatientVisitAdditionalInformation_PV2();

            BindPTAllergyInformation_AL1();
            BindPTDiagnosis_DG1();
            BindPTProcedures();
            BindInsurance_IN1();

            string strDate = "", strTime = "";
            strDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            string[] arrDate;
            arrDate = strDate.Split(' ');
            string strDate1 = arrDate[0];
            strTime = arrDate[1];

            string[] arrDate1 = strDate1.Split('/');
            string strTimeStamp = "";
            strTimeStamp = arrDate1[2] + arrDate1[1] + arrDate1[0] + strTime.Replace(":", "");



            string FileName = MessageEvent + "_" + MessageControlID; // + PROVIDER_ID + "-" + PT_ID.Replace("/", "_") + "-" + VISIT_ID + "-" + EMR_ID + '-' + strTimeStamp;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            string ReturnMessage = "";
            Int32 MessageSent = 0;

            Boolean SendToMalaffi = false;
            string[] arrMessageTypes = UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrMessageTypes.Length; intSegType++)
            {
                if (arrMessageTypes[intSegType] == MessageEvent) //MESSAGE_TYPE
                {
                    SendToMalaffi = true;
                }
            }

            if (SendToMalaffi == true)
            {
                SendHL7Message(testHl7MessageToTransmit.ToString(), "ADT", out  ReturnMessage, out  MessageSent);
            }
            else
            {
                MessageSent = 3;
            }

            LogFileWriting("HL7File Created file name is  " + FileName);


            MessageID = MessageControlID;
            HL7FileName = FileName;
            MessageCode = MessageEvent;// "ADT-A03";
            MessageDesc = "Discharge/End Visit";

            RETURN_MESSAGE = ReturnMessage.Replace("'", "");
            // MalaffiHL7MessageMasterAdd();



            CommonBAL objCom = new CommonBAL();
            string Criteria = " HMHM_UPLOAD_STATUS ='PENDING' ";
            Criteria += " AND HMHM_BRANCH_ID='" + BRANCH_ID + "' AND HMHM_PT_ID='" + PT_ID + "' AND HMHM_EMR_ID='" + EMR_ID + "'";
            Criteria += " AND HMHM_MESSAGE_TYPE='" + MESSAGE_TYPE + "' and HMHM_MESSAGE_CODE='" + MessageCode + "'";

            string FieldNameWithValues = "HMHM_MESSAGE_ID='" + MessageControlID + "', HMHM_FILE_NAME='" + FileName + "' ";
            if (MessageSent == 1)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'UPLOADED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }
            if (MessageSent == 2)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'ERROR' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            if (MessageSent == 3)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'FILE_CREATED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            string FieldNameWithValues1 = "HMHM_RETURN_MESSAGE='" + RETURN_MESSAGE + "'";
            objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            if (isError == true)
            {
                return false;
            }

            return true;
        }

        public Boolean PreAmitPatientMessageGenerate_ADT_A05()
        {
            Boolean isError = false;
            LogFileWriting("****************** A05 " + System.DateTime.Now.ToString() + "******************");

            GetPatientDetails();
            GetPatientVisitDtls();
            BindEMRPTMaster();
            // GetPatientClass();

            if (ValidateNL7Message() == false)
            {
                isError = true;
            }


            testHl7MessageToTransmit = new StringBuilder();




            MessageType = "ADT^A05";
            MessageEvent = "ADT-A05";
            MessageHeader();
            BindEventType("A05");


            BindPatientIdentification_PID();
            BindNextofKinOrAssociatedParties();
            BindPatientVisitAdditionalInformation_PV1();


            BindPTAllergyInformation_AL1();
            BindPTDiagnosis_DG1();
            BindPTProcedures();

            string strDate = "", strTime = "";
            strDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            string[] arrDate;
            arrDate = strDate.Split(' ');
            string strDate1 = arrDate[0];
            strTime = arrDate[1];

            string[] arrDate1 = strDate1.Split('/');
            string strTimeStamp = "";
            strTimeStamp = arrDate1[2] + arrDate1[1] + arrDate1[0] + strTime.Replace(":", "");



            string FileName = MessageEvent + "_" + MessageControlID; // + PROVIDER_ID + "-" + PT_ID.Replace("/", "_") + "-" + VISIT_ID + "-" + EMR_ID + '-' + strTimeStamp;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            string ReturnMessage = "";
            Int32 MessageSent = 0;
            SendHL7Message(testHl7MessageToTransmit.ToString(), "ADT", out  ReturnMessage, out  MessageSent);
            LogFileWriting("HL7File Created file name is  " + FileName);

            MessageID = MessageControlID;
            HL7FileName = FileName;
            MessageCode = MessageEvent; ; //"ADT-A05";
            MessageDesc = "Pre-Admit A Patient";
            MESSAGE_TYPE = "ADT";
            RETURN_MESSAGE = ReturnMessage.Replace("'", "");
            MalaffiHL7MessageMasterAdd();

            if (isError == true)
            {
                return false;
            }

            return true;
        }

        public Boolean ChangeOutPatientToInPatienMessageGenerate_ADT_A06()
        {
            Boolean isError = false;
            LogFileWriting("****************** A06 " + System.DateTime.Now.ToString() + "******************");

            GetPatientDetails();
            GetPatientVisitDtls();
            BindEMRPTMaster();
            // GetPatientClass();

            if (ValidateNL7Message() == false)
            {
                isError = true;
            }


            testHl7MessageToTransmit = new StringBuilder();




            MessageType = "ADT^A06";
            MessageEvent = "ADT-A06";
            MessageHeader();
            BindEventType("A06");


            BindPatientIdentification_PID();
            BindNextofKinOrAssociatedParties();
            BindPatientVisitAdditionalInformation_PV1();


            BindPTAllergyInformation_AL1();
            BindPTDiagnosis_DG1();
            BindPTProcedures();


            string strDate = "", strTime = "";
            strDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            string[] arrDate;
            arrDate = strDate.Split(' ');
            string strDate1 = arrDate[0];
            strTime = arrDate[1];

            string[] arrDate1 = strDate1.Split('/');
            string strTimeStamp = "";
            strTimeStamp = arrDate1[2] + arrDate1[1] + arrDate1[0] + strTime.Replace(":", "");



            string FileName = MessageEvent + "_" + MessageControlID; // + PROVIDER_ID + "-" + PT_ID.Replace("/", "_") + "-" + VISIT_ID + "-" + EMR_ID + '-' + strTimeStamp;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            string ReturnMessage = "";
            Int32 MessageSent = 0;
            SendHL7Message(testHl7MessageToTransmit.ToString(), "ADT", out  ReturnMessage, out  MessageSent);
            LogFileWriting("HL7File Created file name is  " + FileName);

            MessageID = MessageControlID;
            HL7FileName = FileName;
            MessageCode = MessageEvent;// "ADT-A06";
            MessageDesc = "Change OutPatient To InPatient";
            MESSAGE_TYPE = "ADT";
            RETURN_MESSAGE = ReturnMessage.Replace("'", "");
            MalaffiHL7MessageMasterAdd();

            if (isError == true)
            {
                return false;
            }

            return true;
        }

        public Boolean ChangeInPatientToOutPatienMessageGenerate_ADT_A07()
        {
            Boolean isError = false;
            LogFileWriting("****************** A07 " + System.DateTime.Now.ToString() + "******************");

            GetPatientDetails();
            GetPatientVisitDtls();
            BindEMRPTMaster();
            // GetPatientClass();

            if (ValidateNL7Message() == false)
            {
                isError = true;
            }


            testHl7MessageToTransmit = new StringBuilder();




            MessageType = "ADT^A07";
            MessageEvent = "ADT-A07";
            MessageHeader();
            BindEventType("A07");


            BindPatientIdentification_PID();
            BindNextofKinOrAssociatedParties();
            BindPatientVisitAdditionalInformation_PV1();


            BindPTAllergyInformation_AL1();
            BindPTDiagnosis_DG1();
            BindPTProcedures();


            string strDate = "", strTime = "";
            strDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            string[] arrDate;
            arrDate = strDate.Split(' ');
            string strDate1 = arrDate[0];
            strTime = arrDate[1];

            string[] arrDate1 = strDate1.Split('/');
            string strTimeStamp = "";
            strTimeStamp = arrDate1[2] + arrDate1[1] + arrDate1[0] + strTime.Replace(":", "");



            string FileName = MessageEvent + "_" + MessageControlID; // + PROVIDER_ID + "-" + PT_ID.Replace("/", "_") + "-" + VISIT_ID + "-" + EMR_ID + '-' + strTimeStamp;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            string ReturnMessage = "";
            Int32 MessageSent = 0;
            SendHL7Message(testHl7MessageToTransmit.ToString(), "ADT", out  ReturnMessage, out  MessageSent);
            LogFileWriting("HL7File Created file name is  " + FileName);

            MessageID = MessageControlID;
            HL7FileName = FileName;
            MessageCode = MessageEvent;// "ADT-A07";
            MessageDesc = "Change InPatient To OutPatient";
            MESSAGE_TYPE = "ADT";
            RETURN_MESSAGE = ReturnMessage.Replace("'", "");
            MalaffiHL7MessageMasterAdd();

            if (isError == true)
            {
                return false;
            }

            return true;
        }

        public Boolean UpdatePatientEncounterInfoMessageGenerate_ADT_A08()
        {
            Boolean isError = false;
            MessageType = "ADT^A08";
            MessageEvent = "ADT-A08";
            MESSAGE_TYPE = "ADT";

            LogFileWriting("****************** A08 " + System.DateTime.Now.ToString() + "******************");

            GetPatientDetails();
            GetPatientVisitDtls();
            BindEMRPTMaster();
            // GetPatientClass();

            if (ValidateNL7Message() == false)
            {
                isError = true;
            }


            testHl7MessageToTransmit = new StringBuilder();





            MessageHeader();
            BindEventType("A08");


            BindPatientIdentification_PID();
            BindNextofKinOrAssociatedParties();
            BindPatientVisitAdditionalInformation_PV1();
            BindPatientVisitAdditionalInformation_PV2();

            BindPTAllergyInformation_AL1();
            BindPTDiagnosis_DG1();
            BindPTProcedures();
            BindInsurance_IN1();

            string strDate = "", strTime = "";
            strDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            string[] arrDate;
            arrDate = strDate.Split(' ');
            string strDate1 = arrDate[0];
            strTime = arrDate[1];

            string[] arrDate1 = strDate1.Split('/');
            string strTimeStamp = "";
            strTimeStamp = arrDate1[2] + arrDate1[1] + arrDate1[0] + strTime.Replace(":", "");



            string FileName = MessageEvent + "_" + MessageControlID; // + PROVIDER_ID + "-" + PT_ID.Replace("/", "_") + "-" + VISIT_ID + "-" + EMR_ID + '-' + strTimeStamp;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            string ReturnMessage = "";
            Int32 MessageSent = 0;


            Boolean SendToMalaffi = false;
            string[] arrMessageTypes = UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrMessageTypes.Length; intSegType++)
            {
                if (arrMessageTypes[intSegType] == MessageEvent) //MESSAGE_TYPE
                {
                    SendToMalaffi = true;
                }
            }

            if (SendToMalaffi == true)
            {
                SendHL7Message(testHl7MessageToTransmit.ToString(), MESSAGE_TYPE, out  ReturnMessage, out  MessageSent);
            }
            else
            {
                MessageSent = 3;
            }





            LogFileWriting("HL7File Created file name is  " + FileName);

            MessageID = MessageControlID;
            HL7FileName = FileName;
            MessageCode = MessageEvent;// "ADT-A08";
            MessageDesc = "Update Patient Encounter Information";

            RETURN_MESSAGE = ReturnMessage.Replace("'", "");
            // MalaffiHL7MessageMasterAdd();


            CommonBAL objCom = new CommonBAL();
            string Criteria = " HMHM_UPLOAD_STATUS ='PENDING' ";
            Criteria += " AND HMHM_BRANCH_ID='" + BRANCH_ID + "' AND HMHM_PT_ID='" + PT_ID + "' AND HMHM_EMR_ID='" + EMR_ID + "'";
            Criteria += " AND HMHM_MESSAGE_TYPE='" + MESSAGE_TYPE + "' and HMHM_MESSAGE_CODE='" + MessageCode + "'";

            string FieldNameWithValues = "HMHM_MESSAGE_ID='" + MessageControlID + "', HMHM_FILE_NAME='" + FileName + "' ";
            if (MessageSent == 1)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'UPLOADED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }
            if (MessageSent == 2)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'ERROR' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }


            if (MessageSent == 3)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'FILE_CREATED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            string FieldNameWithValues1 = "HMHM_RETURN_MESSAGE='" + RETURN_MESSAGE + "'";
            objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            if (isError == true)
            {
                return false;
            }

            return true;
        }


        public Boolean CancelTransferPatientMessageGenerate_ADT_A12()
        {
            Boolean isError = false;
            LogFileWriting("****************** A12 " + System.DateTime.Now.ToString() + "******************");

            GetPatientDetails();
            GetPatientVisitDtls();
            BindEMRPTMaster();
            // GetPatientClass();

            if (ValidateNL7Message() == false)
            {
                isError = true;
            }


            testHl7MessageToTransmit = new StringBuilder();




            MessageType = "ADT^A12";
            MessageEvent = "ADT-A12";

            MessageHeader();
            BindEventType("A12");


            BindPatientIdentification_PID();
            BindNextofKinOrAssociatedParties();
            BindPatientVisitAdditionalInformation_PV1();

            BindPTDiagnosis_DG1();

            string strDate = "", strTime = "";
            strDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            string[] arrDate;
            arrDate = strDate.Split(' ');
            string strDate1 = arrDate[0];
            strTime = arrDate[1];

            string[] arrDate1 = strDate1.Split('/');
            string strTimeStamp = "";
            strTimeStamp = arrDate1[2] + arrDate1[1] + arrDate1[0] + strTime.Replace(":", "");



            string FileName = MessageEvent + "_" + MessageControlID; // + PROVIDER_ID + "-" + PT_ID.Replace("/", "_") + "-" + VISIT_ID + "-" + EMR_ID + '-' + strTimeStamp;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            string ReturnMessage = "";
            Int32 MessageSent = 0;
            SendHL7Message(testHl7MessageToTransmit.ToString(), "ADT", out  ReturnMessage, out  MessageSent);
            LogFileWriting("HL7File Created file name is  " + FileName);

            MessageID = MessageControlID;
            HL7FileName = FileName;
            MessageCode = MessageEvent;// "ADT-A12";
            MessageDesc = "Cancel Transfer";
            MESSAGE_TYPE = "ADT";
            RETURN_MESSAGE = ReturnMessage.Replace("'", "");
            MalaffiHL7MessageMasterAdd();

            if (isError == true)
            {
                return false;
            }

            return true;
        }

        public Boolean CancelDischargeEndVisitMessageGenerate_ADT_A13()
        {
            Boolean isError = false;
            LogFileWriting("****************** A13 " + System.DateTime.Now.ToString() + "******************");

            GetPatientDetails();
            GetPatientVisitDtls();
            BindEMRPTMaster();
            // GetPatientClass();

            if (ValidateNL7Message() == false)
            {
                isError = true;
            }


            testHl7MessageToTransmit = new StringBuilder();




            MessageType = "ADT^A13";
            MessageEvent = "ADT-A13";

            MessageHeader();
            BindEventType("A13");


            BindPatientIdentification_PID();
            BindNextofKinOrAssociatedParties();
            BindPatientVisitAdditionalInformation_PV1();


            BindPTAllergyInformation_AL1();
            BindPTDiagnosis_DG1();
            BindPTProcedures();
            BindInsurance_IN1();

            string strDate = "", strTime = "";
            strDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            string[] arrDate;
            arrDate = strDate.Split(' ');
            string strDate1 = arrDate[0];
            strTime = arrDate[1];

            string[] arrDate1 = strDate1.Split('/');
            string strTimeStamp = "";
            strTimeStamp = arrDate1[2] + arrDate1[1] + arrDate1[0] + strTime.Replace(":", "");



            string FileName = MessageEvent + "_" + MessageControlID; // + PROVIDER_ID + "-" + PT_ID.Replace("/", "_") + "-" + VISIT_ID + "-" + EMR_ID + '-' + strTimeStamp;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            string ReturnMessage = "";
            Int32 MessageSent = 0;
            SendHL7Message(testHl7MessageToTransmit.ToString(), "ADT", out  ReturnMessage, out  MessageSent);
            LogFileWriting("HL7File Created file name is  " + FileName);

            MessageID = MessageControlID;
            HL7FileName = FileName;
            MessageCode = MessageEvent;// "ADT-A13";
            MessageDesc = "Cancel Discharge/End Visit";
            MESSAGE_TYPE = "ADT";
            RETURN_MESSAGE = ReturnMessage.Replace("'", "");
            MalaffiHL7MessageMasterAdd();

            if (isError == true)
            {
                return false;
            }

            return true;
        }



        public Boolean AddProblemMessageGenerate_PPR_PC1()
        {
            Boolean isError = false;
            MessageType = "PPR^PC1";
            MessageEvent = "PPR-PC1";
            MESSAGE_TYPE = "PROB";

            LogFileWriting("****************** " + System.DateTime.Now.ToString() + "******************");
            GetPatientDetails();
            GetPatientVisitDtls();
            // BindEMRPTMaster();
            // GetPatientClass();



            if (ValidateNL7Message() == false)
            {
                isError = true;
            }


            if (CheckPTProblems() == false)
            {
                isError = true;
                goto FunEnd;
            }

            testHl7MessageToTransmit = new StringBuilder();



            MessageHeader();
            BindPatientIdentification_PID();
            BindPatientVisitAdditionalInformation_PV1();

            BindProblemDetails_PRB("AD");


            // BindProblemDetailsAdd_PRB();

            string FileName = MessageEvent + "_" + MessageControlID;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            string ReturnMessage = "";
            Int32 MessageSent = 0;


            Boolean SendToMalaffi = false;
            string[] arrMessageTypes = UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrMessageTypes.Length; intSegType++)
            {
                if (arrMessageTypes[intSegType] == MessageEvent) //MESSAGE_TYPE
                {
                    SendToMalaffi = true;
                }
            }

            if (SendToMalaffi == true)
            {
                SendHL7Message(testHl7MessageToTransmit.ToString(), MESSAGE_TYPE, out  ReturnMessage, out  MessageSent);
            }
            else
            {
                MessageSent = 3;
            }

            LogFileWriting("HL7File Created file name is  " + FileName);

            MessageID = MessageControlID;
            HL7FileName = FileName;
            MessageCode = MessageEvent;// "PPR-PC1";
            MessageDesc = "Add Problem";

            RETURN_MESSAGE = ReturnMessage.Replace("'", "");
            // MalaffiHL7MessageMasterAdd();

            CommonBAL objCom = new CommonBAL();
            string Criteria = " HMHM_UPLOAD_STATUS ='PENDING' ";
            Criteria += " AND HMHM_BRANCH_ID='" + BRANCH_ID + "' AND HMHM_PT_ID='" + PT_ID + "' AND HMHM_VISIT_ID='" + VISIT_ID + "'";
            Criteria += " AND HMHM_MESSAGE_TYPE='" + MESSAGE_TYPE + "' and HMHM_MESSAGE_CODE='" + MessageCode + "'";

            string FieldNameWithValues = "HMHM_MESSAGE_ID='" + MessageControlID + "', HMHM_FILE_NAME='" + FileName + "' ";
            if (MessageSent == 1)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'UPLOADED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }
            if (MessageSent == 2)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'ERROR' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            if (MessageSent == 3)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'FILE_CREATED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            string FieldNameWithValues1 = "HMHM_RETURN_MESSAGE='" + RETURN_MESSAGE + "'";
            objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

        FunEnd: ;
            if (isError == true)
            {
                return false;
            }

            return true;
        }

        public Boolean UpdateProblemMessageGenerate_PPR_PC2()
        {
            Boolean isError = false;
            MessageType = "PPR^PC2";
            MessageEvent = "PPR-PC2";
            MESSAGE_TYPE = "PROB";

            GetPatientDetails();
            GetPatientVisitDtls();
            //BindEMRPTMaster();
            //GetPatientClass();

            if (ValidateNL7Message() == false)
            {
                isError = true;
            }


            testHl7MessageToTransmit = new StringBuilder();

            if (CheckPTProblems() == false)
            {
                isError = true;
                goto FunEnd;
            }



            MessageHeader();
            BindPatientIdentification_PID();
            BindPatientVisitAdditionalInformation_PV1();

            BindProblemDetails_PRB("UP");


            string strDate = "", strTime = "";
            strDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            string[] arrDate;
            arrDate = strDate.Split(' ');
            string strDate1 = arrDate[0];
            strTime = arrDate[1];

            string[] arrDate1 = strDate1.Split('/');
            string strTimeStamp = "";
            strTimeStamp = arrDate1[2] + arrDate1[1] + arrDate1[0] + strTime.Replace(":", "");



            string FileName = MessageEvent + "_" + MessageControlID;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            string ReturnMessage = "";
            Int32 MessageSent = 0;

            Boolean SendToMalaffi = false;
            string[] arrMessageTypes = UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrMessageTypes.Length; intSegType++)
            {
                if (arrMessageTypes[intSegType] == MessageEvent) //MESSAGE_TYPE
                {
                    SendToMalaffi = true;
                }
            }

            if (SendToMalaffi == true)
            {
                SendHL7Message(testHl7MessageToTransmit.ToString(), "PROB", out  ReturnMessage, out  MessageSent);

            }
            else
            {
                MessageSent = 3;
            }

            LogFileWriting("HL7File Created file name is  " + FileName);

            MessageID = MessageControlID;
            HL7FileName = FileName;
            MessageCode = MessageEvent;// "PPR-PC2";
            MessageDesc = "Update Problem";

            RETURN_MESSAGE = ReturnMessage.Replace("'", "");
            // MalaffiHL7MessageMasterAdd();

            CommonBAL objCom = new CommonBAL();
            string Criteria = " HMHM_UPLOAD_STATUS ='PENDING' ";
            Criteria += " AND HMHM_BRANCH_ID='" + BRANCH_ID + "' AND HMHM_PT_ID='" + PT_ID + "' AND HMHM_VISIT_ID='" + VISIT_ID + "'";
            Criteria += " AND HMHM_MESSAGE_TYPE='" + MESSAGE_TYPE + "' and HMHM_MESSAGE_CODE='" + MessageCode + "'";

            string FieldNameWithValues = "HMHM_MESSAGE_ID='" + MessageControlID + "', HMHM_FILE_NAME='" + FileName + "' ";
            if (MessageSent == 1)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'UPLOADED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }
            if (MessageSent == 2)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'ERROR' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            if (MessageSent == 3)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'FILE_CREATED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            string FieldNameWithValues1 = "HMHM_RETURN_MESSAGE='" + RETURN_MESSAGE + "'";
            objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);


        FunEnd: ;
            if (isError == true)
            {
                return false;
            }


            return true;
        }

        public Boolean DeleteProblemMessageGenerate_PPR_PC3()
        {
            Boolean isError = false;
            MessageType = "PPR^PC3";
            MessageEvent = "PPR-PC3";
            MESSAGE_TYPE = "PROB";

            GetPatientDetails();
            GetPatientVisitDtls();
            //BindEMRPTMaster();
            //GetPatientClass();

            if (ValidateNL7Message() == false)
            {
                isError = true;
            }


            testHl7MessageToTransmit = new StringBuilder();

            if (CheckPTDeleteProblems() == false)
            {
                isError = true;
                goto FunEnd;
            }




            MessageHeader();
            BindPatientIdentification_PID();
            BindPatientVisitAdditionalInformation_PV1();

            BindProblemDetails_PRB("DE");


            string strDate = "", strTime = "";
            strDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            string[] arrDate;
            arrDate = strDate.Split(' ');
            string strDate1 = arrDate[0];
            strTime = arrDate[1];

            string[] arrDate1 = strDate1.Split('/');
            string strTimeStamp = "";
            strTimeStamp = arrDate1[2] + arrDate1[1] + arrDate1[0] + strTime.Replace(":", "");



            string FileName = MessageEvent + "_" + MessageControlID;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            string ReturnMessage = "";
            Int32 MessageSent = 0;

            Boolean SendToMalaffi = false;
            string[] arrMessageTypes = UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrMessageTypes.Length; intSegType++)
            {
                if (arrMessageTypes[intSegType] == MessageEvent) //MESSAGE_TYPE
                {
                    SendToMalaffi = true;
                }
            }

            if (SendToMalaffi == true)
            {
                SendHL7Message(testHl7MessageToTransmit.ToString(), MESSAGE_TYPE, out  ReturnMessage, out  MessageSent);
            }
            else
            {
                MessageSent = 3;
            }

            LogFileWriting("HL7File Created file name is  " + FileName);

            MessageID = MessageControlID;
            HL7FileName = FileName;
            MessageCode = MessageEvent;// "PPR-PC3";
            MessageDesc = "Delete Problem";

            RETURN_MESSAGE = ReturnMessage.Replace("'", "");
            // MalaffiHL7MessageMasterAdd();

            CommonBAL objCom = new CommonBAL();
            string Criteria = " HMHM_UPLOAD_STATUS ='PENDING' ";
            Criteria += " AND HMHM_BRANCH_ID='" + BRANCH_ID + "' AND HMHM_PT_ID='" + PT_ID + "' AND HMHM_VISIT_ID='" + VISIT_ID + "'";
            Criteria += " AND HMHM_MESSAGE_TYPE='" + MESSAGE_TYPE + "' and HMHM_MESSAGE_CODE='" + MessageCode + "'";

            string FieldNameWithValues = "HMHM_MESSAGE_ID='" + MessageControlID + "', HMHM_FILE_NAME='" + FileName + "' ";
            if (MessageSent == 1)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'UPLOADED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }
            if (MessageSent == 2)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'ERROR' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            if (MessageSent == 3)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'FILE_CREATED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            string FieldNameWithValues1 = "HMHM_RETURN_MESSAGE='" + RETURN_MESSAGE + "'";
            objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

        FunEnd: ;
            if (isError == true)
            {
                return false;
            }

            return true;
        }


        public Boolean RadMessageGenerate_ORU_R01()
        {
            Boolean isError = false;
            MessageType = "ORU^R01";
            MessageEvent = "ORU-R01";
            MESSAGE_TYPE = "RESULT";

            LogFileWriting("****************** ORU-R01 " + System.DateTime.Now.ToString() + "******************");



            GetPatientDetails();
            // GetPatientVisitDtls();


            if (ValidateNL7Message() == false)
            {
                isError = true;
                goto FunEnd;
            }



            testHl7MessageToTransmit = new StringBuilder();


            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND RTR_TRANS_NO = '" + RAD_TEST_REPORT_ID + "' AND RTR_RESULT_STATUS IN ('F','C','X')";

            //\r

            DS = objCom.fnGetFieldValue("REPLACE( CONVERT(VARCHAR(8000), RTR_REPORT), CHAR(13) + CHAR(10), ' ' )  AS RTR_REPORTDesc, *", "RAD_TEST_REPORT", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {

                    string RadCode = Convert.ToString(DS.Tables[0].Rows[i]["RTR_CPT_CODE"]);
                    string RadName = "", CodeTypeDesc = "";
                    GetServiceMasterName("HSM_HAAD_CODE", RadCode, out  RadName, out CodeTypeDesc);   // Convert.ToString(DS.Tables[0].Rows[i]["RTR_SERV_DESCRIPTION"]);

                    RadName = RemoveSplChar(Convert.ToString(RadName.Trim()));

                    string RawObsValue = Convert.ToString(DS.Tables[0].Rows[0]["RTR_REPORTDesc"]);




                    RawObsValue = RawObsValue.Replace("&nbsp;", string.Empty);
                    string ObsValue = Regex.Replace(RawObsValue, "<.*?>", String.Empty);

                    ObsValue = RemoveSplChar(Convert.ToString(ObsValue.Trim()));

                    ObsValue = ObsValue.Replace("&nbsp;", string.Empty);
                    ObsValue = ObsValue.Replace("\n", string.Empty);
                    ObsValue = ObsValue.Replace(Environment.NewLine, string.Empty);


                    string ResultStatus = Convert.ToString(DS.Tables[0].Rows[0]["RTR_RESULT_STATUS"]);
                    string DiagnosticService;

                    if (DS.Tables[0].Rows[0].IsNull("RTR_DIAGNOSTIC_SERVICE") == false && Convert.ToString(DS.Tables[0].Rows[0]["RTR_DIAGNOSTIC_SERVICE"]) != "")
                    {
                        DiagnosticService = Convert.ToString(DS.Tables[0].Rows[0]["RTR_DIAGNOSTIC_SERVICE"]);
                    }
                    else
                    {
                        DiagnosticService = "RAD";
                    }


                    RAD_TEST_REPORT_ID = Convert.ToString(DS.Tables[0].Rows[0]["RTR_TRANS_NO"]);

                    Int32 SLNo = i + 1;

                    string RequestedDateTime = "", ObservStart = "", ObservEnd = "", ObservDate = "", OrderTransDate = "";

                    if (DS.Tables[0].Rows[0].IsNull("RTR_REQUEST_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["RTR_REQUEST_DATE"]) != "")
                    {
                        DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["RTR_REQUEST_DATE"]);

                        string strMonth = dtStartDate.Month.ToString();
                        if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                        string strDay = dtStartDate.Day.ToString();
                        if (strDay.Length == 1) { strDay = "0" + strDay; }

                        string strHour = dtStartDate.Hour.ToString();
                        if (strHour.Length == 1) { strHour = "0" + strHour; }

                        string strMinute = dtStartDate.Minute.ToString();
                        if (strMinute.Length == 1) { strMinute = "0" + strMinute; }


                        RequestedDateTime = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                    }


                    if (DS.Tables[0].Rows[0].IsNull("RTR_OBSERV_START") == false && Convert.ToString(DS.Tables[0].Rows[0]["RTR_OBSERV_START"]) != "")
                    {
                        DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["RTR_OBSERV_START"]);

                        string strMonth = dtStartDate.Month.ToString();
                        if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                        string strDay = dtStartDate.Day.ToString();
                        if (strDay.Length == 1) { strDay = "0" + strDay; }

                        string strHour = dtStartDate.Hour.ToString();
                        if (strHour.Length == 1) { strHour = "0" + strHour; }

                        string strMinute = dtStartDate.Minute.ToString();
                        if (strMinute.Length == 1) { strMinute = "0" + strMinute; }


                        ObservStart = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                    }


                    if (DS.Tables[0].Rows[0].IsNull("RTR_OBSERV_END") == false && Convert.ToString(DS.Tables[0].Rows[0]["RTR_OBSERV_END"]) != "")
                    {
                        DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["RTR_OBSERV_END"]);

                        string strMonth = dtStartDate.Month.ToString();
                        if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                        string strDay = dtStartDate.Day.ToString();
                        if (strDay.Length == 1) { strDay = "0" + strDay; }

                        string strHour = dtStartDate.Hour.ToString();
                        if (strHour.Length == 1) { strHour = "0" + strHour; }

                        string strMinute = dtStartDate.Minute.ToString();
                        if (strMinute.Length == 1) { strMinute = "0" + strMinute; }


                        ObservEnd = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                    }

                    if (DS.Tables[0].Rows[0].IsNull("RTR_OBSERV_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["RTR_OBSERV_DATE"]) != "")
                    {
                        DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["RTR_OBSERV_DATE"]);

                        string strMonth = dtStartDate.Month.ToString();
                        if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                        string strDay = dtStartDate.Day.ToString();
                        if (strDay.Length == 1) { strDay = "0" + strDay; }

                        string strHour = dtStartDate.Hour.ToString();
                        if (strHour.Length == 1) { strHour = "0" + strHour; }

                        string strMinute = dtStartDate.Minute.ToString();
                        if (strMinute.Length == 1) { strMinute = "0" + strMinute; }


                        ObservDate = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                    }

                    if (DS.Tables[0].Rows[0].IsNull("RTR_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["RTR_DATE"]) != "")
                    {
                        DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["RTR_DATE"]);

                        string strMonth = dtStartDate.Month.ToString();
                        if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                        string strDay = dtStartDate.Day.ToString();
                        if (strDay.Length == 1) { strDay = "0" + strDay; }

                        string strHour = dtStartDate.Hour.ToString();
                        if (strHour.Length == 1) { strHour = "0" + strHour; }

                        string strMinute = dtStartDate.Minute.ToString();
                        if (strMinute.Length == 1) { strMinute = "0" + strMinute; }


                        OrderTransDate = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                    }




                    if (ObsValue == "")
                    {
                        goto LoopEnd;
                    }

                    string OrderingProviderID, OrderingProviderName;
                    string DR_ID = Convert.ToString(DS.Tables[0].Rows[0]["RTR_REF_DR"]).Trim(); //LTRM_DR_CODE

                    string DrFirstName, DrMiddleName, DrLastName, LicenseNo, DrDeptName;
                    GetStaffDetails(DR_ID, out   DrFirstName, out DrMiddleName, out   DrLastName, out   LicenseNo, out DrDeptName);


                    if (LicenseNo == "")
                    {
                        GetRefDrfDetails(DR_ID, out   DrFirstName, out DrMiddleName, out   DrLastName, out   LicenseNo, out DrDeptName);
                    }

                    LogFileWriting("LabMessageGenerate_ORU_R01 3");

                    if (LicenseNo != "")
                    {
                        OrderingProviderID = LicenseNo;
                    }
                    else
                    {
                        OrderingProviderID = DR_ID;
                    }

                    if (DrFirstName.Length > 20)
                    {
                        DrFirstName = DrFirstName.Substring(0, 15);
                    }
                    if (DrLastName.Length > 20)
                    {
                        DrLastName = DrLastName.Substring(0, 15);
                    }


                    // if (DrLastName != "")
                    // {
                    OrderingProviderName = DrLastName + "^" + DrFirstName + "^" + DrMiddleName;      // DrFirstName + "^" + DrLastName;
                    //}
                    //else
                    //{
                    //    OrderingProviderName = DrFirstName + "^" + DrFirstName;
                    //}



                    LogFileWriting("LabMessageGenerate_ORU_R01 4");

                    string ResultInterpreterID, ResultInterpreterName;
                    string RefDR_ID = Convert.ToString(DS.Tables[0].Rows[0]["RTR_DR_CODE"]).Trim(); //LTRM_REF_DR

                    string RefDrFirstName, RefDrMiddleName, RefDrLastName, RefLicenseNo, RefDrSpecialtyName;
                    GetStaffDetails(RefDR_ID, out   RefDrFirstName, out RefDrMiddleName, out   RefDrLastName, out   RefLicenseNo, out RefDrSpecialtyName);

                    LogFileWriting("LabMessageGenerate_ORU_R01 5");
                    if (RefLicenseNo != "")
                    {
                        ResultInterpreterID = RefLicenseNo;
                    }
                    else
                    {
                        ResultInterpreterID = RefDR_ID;
                    }

                    if (RefDrFirstName.Length > 20)
                    {
                        RefDrFirstName = RefDrFirstName.Substring(0, 15);
                    }
                    if (RefDrLastName.Length > 20)
                    {
                        RefDrLastName = RefDrLastName.Substring(0, 15);
                    }


                    // if (RefDrLastName != "")
                    // {
                    ResultInterpreterName = RefDrLastName + "&" + RefDrFirstName + "&" + RefDrMiddleName;  // RefDrFirstName + "&" + RefDrLastName;
                    //}
                    //else
                    //{
                    //    ResultInterpreterName = RefDrFirstName + "&" + RefDrFirstName;
                    //}

                    MessageHeader();
                    BindPatientIdentification_PID();
                    string RefDrSpecialty = GetCommnMasterCode(RefDrSpecialtyName, "Specialty");

                    BindPatientLabVisitAdditionalInformation_PV1(RefDrSpecialty, RefDrSpecialtyName, ResultInterpreterID, ResultInterpreterName, OrderingProviderID, OrderingProviderName, RAD_TEST_REPORT_ID, RequestedDateTime);


                    string ActionCode = "NW";
                    if (ResultStatus == "C")
                    {
                        ActionCode = "RU";
                    }
                    else if (ResultStatus == "X")
                    {
                        ActionCode = "DE";
                    }
                    else
                    {
                        ActionCode = "NW";
                    }


                    BindOrderControl_ORC(ActionCode, 1, RAD_TEST_REPORT_ID, OrderingProviderID, OrderingProviderName, OrderTransDate, "", "active", OrderTransDate);

                    if (RadCode == "" || RadName == "" || RequestedDateTime == "" || ObservStart == "" || ObservEnd == "" || ObservDate == "" || OrderTransDate == "" || OrderingProviderID == "" || OrderingProviderName == "" || ResultInterpreterID == "" || ResultInterpreterName == "")
                    {
                        LogFileWriting("RadMessageGenerate_ORU_R01 function Some details missing   RTR_TRANS_NO =" + RAD_TEST_REPORT_ID);
                        goto LoopEnd;

                    }
                    // BindLabObservationRequest_OBR(RAD_TEST_REPORT_ID, RadCode, RadName, RequestedDateTime, ObservStart, ObservEnd, DeliveryDateTime, AuthorizDate, DiagnosticService, ResultStatus, OrderingProviderID, OrderingProviderName, ResultInterpreterID, ResultInterpreterName);
                    BindObservationRequest_OBR(1, RAD_TEST_REPORT_ID, RadCode, RadName, CodeTypeDesc, "", ObservStart, ObservEnd, RequestedDateTime, "", OrderTransDate, DiagnosticService, ResultStatus, OrderingProviderID, OrderingProviderName, ResultInterpreterID, ResultInterpreterName, "");



                    // BindRadObservationRequest_OBR(RadCode, RadName, ObsDateTime, SLNo, RequestedDateTime, ObservStart, ObservEnd, AuthorizDate);

                    BindObservationResult_OBX(false, 1, RadCode, RadName, SystemCode, "", ObsValue, "", ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                    //BindRadObservationResult_OBX(RadCode, RadName, ObsValue, ObsDateTime);



                    string FileName = MessageEvent + "_" + MessageControlID;
                    MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

                    string ReturnMessage = "";
                    Int32 MessageSent = 0;



                    Boolean SendToMalaffi = false;
                    string[] arrMessageTypes = UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
                    for (Int32 intSegType = 0; intSegType < arrMessageTypes.Length; intSegType++)
                    {
                        if (arrMessageTypes[intSegType] == MessageEvent) //MESSAGE_TYPE
                        {
                            SendToMalaffi = true;
                        }
                    }

                    if (SendToMalaffi == true)
                    {
                        SendHL7Message(testHl7MessageToTransmit.ToString(), MESSAGE_TYPE, out  ReturnMessage, out  MessageSent);
                    }
                    else
                    {
                        MessageSent = 3;
                    }

                    LogFileWriting("HL7File Created file name is  " + FileName);

                    // MessageID = MessageControlID;
                    //HL7FileName = FileName;
                    // MessageCode = MessageEvent; // "ORU-R01";
                    // MessageDesc = "Result ORU Rad";
                    // MESSAGE_TYPE = "RESULT";
                    RETURN_MESSAGE = ReturnMessage.Replace("'", "");
                    // MalaffiHL7MessageMasterAdd();

                    objCom = new CommonBAL();
                    string Criteria1 = " HMHM_UPLOAD_STATUS ='PENDING' ";
                    Criteria1 += " AND HMHM_BRANCH_ID='" + BRANCH_ID + "' AND HMHM_PT_ID='" + PT_ID + "'";
                    Criteria1 += " AND HMHM_RAD_TEST_REPORT_ID='" + RAD_TEST_REPORT_ID + "'";
                    Criteria1 += " AND HMHM_MESSAGE_TYPE='" + MESSAGE_TYPE + "' and HMHM_MESSAGE_CODE='" + MessageEvent + "'";

                    string FieldNameWithValues = "HMHM_MESSAGE_ID='" + MessageControlID + "', HMHM_FILE_NAME='" + FileName + "' ";
                    if (MessageSent == 1)
                    {
                        FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'UPLOADED' ";
                        objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria1);

                    }
                    if (MessageSent == 2)
                    {
                        FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'ERROR' ";
                        objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria1);

                    }
                    if (MessageSent == 3)
                    {
                        FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'FILE_CREATED' ";
                        objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria1);

                    }

                    string FieldNameWithValues1 = "HMHM_RETURN_MESSAGE='" + RETURN_MESSAGE + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

                LoopEnd: ;
                }





            }


        FunEnd: ;
            if (isError == true)
            {
                return false;
            }


            return true;

        }


        public Boolean LabMessageGenerate_ORU_R01()
        {

            Boolean isError = false;
            MessageType = "ORU^R01";
            MessageEvent = "ORU-R01";
            MESSAGE_TYPE = "RESULT";

            LogFileWriting("****************** ORU-R01 " + System.DateTime.Now.ToString() + "******************");

            GetPatientDetails();
            // GetPatientVisitDtls();
            LogFileWriting("LabMessageGenerate_ORU_R01 0");
            ////if (EMR_ID != "")
            ////{
            ////    BindEMRPTMaster();
            ////    GetPatientClass();

            ////}

            if (ValidateNL7Message() == false)
            {
                isError = true;
                goto FunEnd;
            }


            LogFileWriting("LabMessageGenerate_ORU_R01 1");

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            string TestDateTime = "", SampleCollectTime = "", DeliveryDateTime = "", SampleCollectID = "", VerifiedDateTime = "", DiagnosticService = "", ResultStatus = "", CollectionMethod = "";
            Boolean MicroorganismIdentified = false;
            objCom = new CommonBAL();
            DS = new DataSet();
            Criteria = " 1=1 ";

            Criteria += " AND  LTRM_PATIENT_ID = '" + PatientID + "' AND LTRM_TEST_REPORT_ID='" + LAB_TEST_REPORT_ID + "' AND LTRM_RESULT_STATUS IN ('F','C','X') ";


            DS = objCom.fnGetFieldValue("*", "LAB_TEST_REPORT_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                LogFileWriting("LabMessageGenerate_ORU_R01 2");

                string LabCode = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_SERV_ID"]);
                string LabDesc = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_SERV_DESCRIPTION"]);

                LabDesc = RemoveSplChar(Convert.ToString(LabDesc.Trim()));

                SampleCollectID = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_SAMPLE_ID"]);

                ResultStatus = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_RESULT_STATUS"]);

                if (DS.Tables[0].Rows[0].IsNull("LTRM_DIAGNOSTIC_SERVICE") == false && Convert.ToString(DS.Tables[0].Rows[0]["LTRM_DIAGNOSTIC_SERVICE"]) != "")
                {

                    DiagnosticService = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_DIAGNOSTIC_SERVICE"]);
                }
                else
                {
                    DiagnosticService = "LAB";
                }


                if (DS.Tables[0].Rows[0].IsNull("LTRM_COLLECTION_METHOD_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["LTRM_COLLECTION_METHOD_NAME"]) != "")
                {

                    CollectionMethod = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_COLLECTION_METHOD_NAME"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("LTRM_MICROORGANISM_IDENTIFIED") == false && Convert.ToString(DS.Tables[0].Rows[0]["LTRM_MICROORGANISM_IDENTIFIED"]) != "")
                {

                    MicroorganismIdentified = Convert.ToBoolean(DS.Tables[0].Rows[0]["LTRM_MICROORGANISM_IDENTIFIED"]);
                }


                if (DS.Tables[0].Rows[0].IsNull("LTRM_TEST_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["LTRM_TEST_DATE"]) != "")
                {
                    DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["LTRM_TEST_DATE"]);

                    string strMonth = dtStartDate.Month.ToString();
                    if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                    string strDay = dtStartDate.Day.ToString();
                    if (strDay.Length == 1) { strDay = "0" + strDay; }

                    string strHour = dtStartDate.Hour.ToString();
                    if (strHour.Length == 1) { strHour = "0" + strHour; }

                    string strMinute = dtStartDate.Minute.ToString();
                    if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                    TestDateTime = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                }


                if (DS.Tables[0].Rows[0].IsNull("LTRM_COLLECTION_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["LTRM_COLLECTION_DATE"]) != "")
                {
                    DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["LTRM_COLLECTION_DATE"]);

                    string strMonth = dtStartDate.Month.ToString();
                    if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                    string strDay = dtStartDate.Day.ToString();
                    if (strDay.Length == 1) { strDay = "0" + strDay; }

                    string strHour = dtStartDate.Hour.ToString();
                    if (strHour.Length == 1) { strHour = "0" + strHour; }

                    string strMinute = dtStartDate.Minute.ToString();
                    if (strMinute.Length == 1) { strMinute = "0" + strMinute; }


                    SampleCollectTime = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                }

                if (DS.Tables[0].Rows[0].IsNull("LTRM_DELIVERY_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["LTRM_DELIVERY_DATE"]) != "")
                {
                    DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["LTRM_DELIVERY_DATE"]);

                    string strMonth = dtStartDate.Month.ToString();
                    if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                    string strDay = dtStartDate.Day.ToString();
                    if (strDay.Length == 1) { strDay = "0" + strDay; }

                    string strHour = dtStartDate.Hour.ToString();
                    if (strHour.Length == 1) { strHour = "0" + strHour; }

                    string strMinute = dtStartDate.Minute.ToString();
                    if (strMinute.Length == 1) { strMinute = "0" + strMinute; }


                    DeliveryDateTime = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                }


                if (DS.Tables[0].Rows[0].IsNull("LTRM_AUTHORIZE_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["LTRM_AUTHORIZE_DATE"]) != "")
                {
                    DateTime dtVerifiedDateTime = Convert.ToDateTime(DS.Tables[0].Rows[0]["LTRM_AUTHORIZE_DATE"]);

                    string strMonth = dtVerifiedDateTime.Month.ToString();
                    if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                    string strDay = dtVerifiedDateTime.Day.ToString();
                    if (strDay.Length == 1) { strDay = "0" + strDay; }

                    string strHour = dtVerifiedDateTime.Hour.ToString();
                    if (strHour.Length == 1) { strHour = "0" + strHour; }

                    string strMinute = dtVerifiedDateTime.Minute.ToString();
                    if (strMinute.Length == 1) { strMinute = "0" + strMinute; }


                    VerifiedDateTime = dtVerifiedDateTime.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                }

                string OrderingProviderID, OrderingProviderName;
                string DR_ID = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_REF_DR"]).Trim(); //LTRM_DR_CODE

                string DrFirstName, DrMiddleName, DrLastName, LicenseNo, DrSpecialityName;
                GetStaffDetails(DR_ID, out   DrFirstName, out DrMiddleName, out   DrLastName, out   LicenseNo, out DrSpecialityName);


                LogFileWriting("LabMessageGenerate_ORU_R01 3");
                if (LicenseNo != "")
                {
                    OrderingProviderID = LicenseNo;
                }
                else
                {
                    OrderingProviderID = DR_ID;
                }

                if (DrFirstName.Length > 20)
                {
                    DrFirstName = DrFirstName.Substring(0, 15);
                }
                if (DrLastName.Length > 20)
                {
                    DrLastName = DrLastName.Substring(0, 15);
                }


                //if (DrLastName != "")
                // {
                OrderingProviderName = DrLastName + "^" + DrFirstName + "^" + DrMiddleName; //DrFirstName + "^" + DrLastName;
                //}
                //else
                //{
                //    OrderingProviderName = DrFirstName + "^" + DrFirstName;
                //}

                LogFileWriting("LabMessageGenerate_ORU_R01 4");

                string ResultInterpreterID, ResultInterpreterName;
                string RefDR_ID = Convert.ToString(DS.Tables[0].Rows[0]["LTRM_DR_CODE"]).Trim(); //LTRM_REF_DR

                string RefDrFirstName, RefDrMiddleName, RefDrLastName, RefLicenseNo, RefDrSpecialityName;
                GetStaffDetails(RefDR_ID, out   RefDrFirstName, out RefDrMiddleName, out   RefDrLastName, out   RefLicenseNo, out RefDrSpecialityName);

                LogFileWriting("LabMessageGenerate_ORU_R01 5");
                if (RefLicenseNo != "")
                {
                    ResultInterpreterID = RefLicenseNo;
                }
                else
                {
                    ResultInterpreterID = RefDR_ID;
                }

                if (RefDrFirstName.Length > 20)
                {
                    RefDrFirstName = RefDrFirstName.Substring(0, 15);
                }
                if (RefDrLastName.Length > 20)
                {
                    RefDrLastName = RefDrLastName.Substring(0, 15);
                }


                //if (RefDrLastName != "")
                // {
                ResultInterpreterName = RefDrLastName + "&" + RefDrFirstName + "&" + RefDrMiddleName;  //RefDrFirstName + "&" + RefDrLastName;
                //}
                //else
                //{
                //    ResultInterpreterName = RefDrFirstName + "&" + RefDrFirstName;
                //}

                LogFileWriting("LabMessageGenerate_ORU_R01 6");


                string RequestedDateTime = "";
                if (EMR_ID != "" && EMR_ID != "0")
                {
                    GetEMRPTLabDetails(LabCode, out   RequestedDateTime);

                    if (RequestedDateTime == "")
                    {
                        RequestedDateTime = SampleCollectTime;
                    }
                }
                else
                {
                    RequestedDateTime = SampleCollectTime;
                }

                testHl7MessageToTransmit = new StringBuilder();


                if (DeliveryDateTime == "")
                {
                    GetSampleCollDtls(SampleCollectID, out   DeliveryDateTime);
                }

                LogFileWriting("LabMessageGenerate_ORU_R01 7");



                MessageHeader();

                BindPatientIdentification_PID();
                LogFileWriting("LabMessageGenerate_ORU_R01 8");
                string RefDrSpecialty = GetCommnMasterCode(RefDrSpecialityName, "Specialty");


                //if (EMR_ID != "")
                //{
                //    BindPatientVisitAdditionalInformation_PV1();
                //}
                //else
                //{

                BindPatientLabVisitAdditionalInformation_PV1(RefDrSpecialty, RefDrSpecialityName, ResultInterpreterID, ResultInterpreterName, OrderingProviderID, OrderingProviderName, LAB_TEST_REPORT_ID, SampleCollectTime);

                // }
                LogFileWriting("LabMessageGenerate_ORU_R01 9");

                string ActionCode = "NW";
                if (ResultStatus == "C")
                {
                    ActionCode = "RU";
                }
                else if (ResultStatus == "X")
                {
                    ActionCode = "DE";
                }
                else
                {
                    ActionCode = "NW";
                }

                BindOrderControl_ORC(ActionCode, 1, LAB_TEST_REPORT_ID, OrderingProviderID, OrderingProviderName, TestDateTime, "", "active", TestDateTime);



                BindObservationRequest_OBR(1, LAB_TEST_REPORT_ID, LabCode, LabDesc, "CPT", "", SampleCollectTime, TestDateTime, RequestedDateTime, DeliveryDateTime, VerifiedDateTime, DiagnosticService, ResultStatus, OrderingProviderID, OrderingProviderName, ResultInterpreterID, ResultInterpreterName, "");

                if (CollectionMethod != "")
                {
                    BindNotesAndComments_NTE(CollectionMethod);
                }

                if (LabCode == "" || SampleCollectTime == "" || TestDateTime == "" || RequestedDateTime == "" || DeliveryDateTime == "" || VerifiedDateTime == "" || DiagnosticService == "")
                {
                    LogFileWriting("ORULabMessageGenerate function Some details missing   LAB_TEST_REPORT_ID = " + LAB_TEST_REPORT_ID);
                    goto LoopEnd;
                }


                Int32 intSlNo = 1;
                Criteria = " 1=1 ";
                Criteria += " AND LTRD_TEST_REPORT_ID = '" + LAB_TEST_REPORT_ID + "'";

                DS = objCom.fnGetFieldValue("*", "LAB_TEST_REPORT_DETAIL", Criteria, "LTRD_SLNO");
                if (DS.Tables[0].Rows.Count > 0)
                {
                    decimal nConversion = 0;


                    foreach (DataRow DR in DS.Tables[0].Rows)
                    {
                        string ObsUnit = "";
                        string strLabValue = "";
                        string AbnormalFlag = "";
                        string Range = "";
                        string decLabValue = "";
                        string obsCode = "", obsDesc = "", strSystemCode = "";

                        ////if (MicroorganismIdentified == true)
                        ////{
                        ////    obsCode = "Result";
                        ////    obsDesc = "Result";
                        ////    strSystemCode = "MALAFFI";
                        ////}
                        ////else
                        ////{
                        obsCode = Convert.ToString(DR["LTRD_TEST_PROFILE_ID"]);
                        obsDesc = Convert.ToString(DR["LTRD_TEST_DESCRIPTION"]);
                        strSystemCode = SystemCode;
                        //// }

                        decLabValue = DR.IsNull("LTRD_TEST_RESULT") == true ? "" : Convert.ToString(DR["LTRD_TEST_RESULT"]);

                        //if (nConversion != 0) decLabValue = decLabValue * nConversion;
                        strLabValue = RemoveSplChar(Convert.ToString(decLabValue.Trim()));

                        AbnormalFlag = "N";
                        if (DR.IsNull("LTRD_RANGE_TYPE") == false && Convert.ToString(DR["LTRD_RANGE_TYPE"]) != "")
                        {

                            AbnormalFlag = RemoveSplChar(Convert.ToString(DR["LTRD_RANGE_TYPE"]));
                        }

                        //if (MicroorganismIdentified == false)
                        //{

                        if (DR.IsNull("LTRD_TEST_UNIT") == false && Convert.ToString(DR["LTRD_TEST_UNIT"]) != "") //LTRD_RANGE
                        {

                            ObsUnit = RemoveSplChar(Convert.ToString(DR["LTRD_TEST_UNIT"]));

                        }

                        if (ObsUnit.Trim().ToUpper() == "NA" || ObsUnit.Trim().ToUpper() == "NILL" || ObsUnit.Trim().ToUpper() == "NIL" || ObsUnit.Trim().ToUpper() == "NI" || ObsUnit.Trim().ToUpper() == "NL")
                        {
                            ObsUnit = "";
                        }


                        if (DR.IsNull("LTRD_TEST_NORMALVALUE") == false && Convert.ToString(DR["LTRD_TEST_NORMALVALUE"]) != "") //LTRD_RANGE
                        {

                            Range = RemoveSplChar(Convert.ToString(DR["LTRD_TEST_NORMALVALUE"]));
                        }

                        if (Range.Trim().ToUpper() == "NA" || Range.Trim().ToUpper() == "NILL" || Range.Trim().ToUpper() == "NIL" || Range.Trim().ToUpper() == "NI" || Range.Trim().ToUpper() == "NL")
                        {
                            Range = "";
                        }


                        // }

                        //OBX
                        BindObservationResult_OBX(false, intSlNo, obsCode, obsDesc, strSystemCode, "", strLabValue, ObsUnit, TestDateTime, AbnormalFlag, Range, ResultStatus, OrderingProviderID, OrderingProviderName);

                        intSlNo = intSlNo + 1;
                    }

                }



                if (MicroorganismIdentified == true)
                {
                    Criteria = " 1=1 ";
                    Criteria += " AND LTMD_TEST_REPORT_ID = '" + LAB_TEST_REPORT_ID + "'";

                    // DS = objCom.fnGetFieldValue("*", "LAB_TEST_REPORT_MICROBIOLOGY_DETAIL", Criteria, "LTMD_TEST_REPORT_ID");
                    DS = objCom.fnGetFieldValue("LTMD_ORGANISM_TYPE_NAME", "LAB_TEST_REPORT_MICROBIOLOGY_DETAIL", Criteria + " GROUP BY LTMD_TEST_REPORT_ID,LTMD_ORGANISM_TYPE_NAME", "LTMD_TEST_REPORT_ID");

                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        Int32 ObsSubID = 1;
                        foreach (DataRow DR in DS.Tables[0].Rows)
                        {

                            string OrganismTypeName = Convert.ToString(DR["LTMD_ORGANISM_TYPE_NAME"]);


                            BindObservationResult_OBX(true, intSlNo, "600-7", "MICROORGANISM IDENTIFIED", "LOINC", Convert.ToString(ObsSubID), OrganismTypeName, "", TestDateTime, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);

                            intSlNo = intSlNo + 1;
                            ObsSubID = ObsSubID + 1;

                        }
                    }

                }


                LogFileWriting("ORULabMessageGenerate 4");

                if (MicroorganismIdentified == true)
                {
                    // 48741813&&SYSTEM_CODE^48741814&&SYSTEM_CODE

                    string Parent = LAB_TEST_REPORT_ID + "_1&&" + SystemCode + "^" + LAB_TEST_REPORT_ID + "_11&&" + SystemCode;
                    DataSet DSMicroGroup = new DataSet();
                    Criteria = " 1=1 ";
                    Criteria += " AND LTMD_TEST_REPORT_ID = '" + LAB_TEST_REPORT_ID + "'";

                    DSMicroGroup = objCom.fnGetFieldValue("LTMD_ORGANISM_TYPE_NAME,MAX(LTMD_STATUS) as LTMD_STATUS ", "LAB_TEST_REPORT_MICROBIOLOGY_DETAIL", Criteria + " GROUP BY LTMD_TEST_REPORT_ID,LTMD_ORGANISM_TYPE_NAME", "LTMD_TEST_REPORT_ID");

                    if (DSMicroGroup.Tables[0].Rows.Count > 0)
                    {
                        Int32 MicroGroupSLNo = 2;
                        Int32 ObRSubID = 1;

                        foreach (DataRow DRMicroGroup in DSMicroGroup.Tables[0].Rows)
                        {
                            Int32 ObsSubID = 1;
                            intSlNo = 1;
                            string OrganismTypeName = Convert.ToString(DRMicroGroup["LTMD_ORGANISM_TYPE_NAME"]);

                            string OrganismGroupStatus = ResultStatus;

                            if (DRMicroGroup.IsNull("LTMD_STATUS") == false && Convert.ToString(DRMicroGroup["LTMD_STATUS"]) != "")
                            {
                                OrganismGroupStatus = Convert.ToString(DRMicroGroup["LTMD_STATUS"]);
                            }

                            BindObservationRequest_OBR(MicroGroupSLNo, LAB_TEST_REPORT_ID, "87186", "Antibiotic MIC", "CPT", Convert.ToString(ObRSubID), SampleCollectTime, TestDateTime, RequestedDateTime, DeliveryDateTime, VerifiedDateTime, DiagnosticService, OrganismGroupStatus, OrderingProviderID, OrderingProviderName, ResultInterpreterID, ResultInterpreterName, Parent);

                            BindObservationResult_OBX(true, 1, "600-7", "MICROORGANISM IDENTIFIED", "LOINC", Convert.ToString(ObsSubID), OrganismTypeName, "", TestDateTime, "", "", OrganismGroupStatus, OrderingProviderID, OrderingProviderName);


                            Criteria = " 1=1 ";
                            Criteria += " AND LTMD_TEST_REPORT_ID = '" + LAB_TEST_REPORT_ID + "'";
                            Criteria += " AND LTMD_ORGANISM_TYPE_NAME='" + OrganismTypeName + "'";

                            DS = objCom.fnGetFieldValue("*", "LAB_TEST_REPORT_MICROBIOLOGY_DETAIL", Criteria, "LTMD_TEST_REPORT_ID");
                            if (DS.Tables[0].Rows.Count > 0)
                            {

                                foreach (DataRow DR in DS.Tables[0].Rows)
                                {
                                    intSlNo = intSlNo + 1;
                                    string strLabValue = Convert.ToString(DR["LTMD_ANTIBIOTICS_VALUE0."]);
                                    string ObsUnit = Convert.ToString(DR["LTMD_ANTIBIOTICS_UNIT"]);
                                    string AbnormalFlag = "N";

                                    if (DR.IsNull("LTMD_ABNORMAL_FLAG") == false && Convert.ToString(DR["LTMD_ABNORMAL_FLAG"]) != "")
                                    {
                                        AbnormalFlag = Convert.ToString(DR["LTMD_ABNORMAL_FLAG"]);
                                    }

                                    string OrganismResultStatus = ResultStatus;

                                    if (DR.IsNull("LTMD_STATUS") == false && Convert.ToString(DR["LTMD_STATUS"]) != "")
                                    {

                                        OrganismResultStatus = Convert.ToString(DR["LTMD_STATUS"]);
                                    }

                                    //BindObservationResult_OBX(intSlNo, Convert.ToString(DR["LTMD_ANTIBIOTICS_TYPE_CODE"]), Convert.ToString(DR["LTMD_ANTIBIOTICS_TYPE_NAME"]), "LOINC", Convert.ToString(MicroGroupSLNo), strLabValue, ObsUnit, TestDateTime, AbnormalFlag, "", ResultStatus);
                                    BindObservationResult_OBX(true, intSlNo, Convert.ToString(DR["LTMD_ANTIBIOTICS_TYPE_CODE"]), Convert.ToString(DR["LTMD_ANTIBIOTICS_TYPE_NAME"]), SystemCode, "", strLabValue, ObsUnit, TestDateTime, AbnormalFlag, "", OrganismResultStatus, OrderingProviderID, OrderingProviderName);

                                }
                            }

                            MicroGroupSLNo = MicroGroupSLNo + 1;
                            ObRSubID = ObRSubID + 1;
                        }
                    }
                }


                string FileName = MessageEvent + "_" + MessageControlID;
                MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

                string ReturnMessage = "";
                Int32 MessageSent = 0;


                Boolean SendToMalaffi = false;
                string[] arrMessageTypes = UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
                for (Int32 intSegType = 0; intSegType < arrMessageTypes.Length; intSegType++)
                {
                    if (arrMessageTypes[intSegType] == MessageEvent) //MESSAGE_TYPE
                    {
                        SendToMalaffi = true;
                    }
                }

                if (SendToMalaffi == true)
                {
                    SendHL7Message(testHl7MessageToTransmit.ToString(), MESSAGE_TYPE, out  ReturnMessage, out MessageSent);
                }
                else
                {
                    MessageSent = 3;
                }


                LogFileWriting("HL7File Created file name is  " + FileName);

                // MessageID = MessageControlID;
                // HL7FileName = FileName;
                // MessageCode = MessageEvent; // "ORU-R01";
                // MessageDesc = "Result ORU Lab";
                // MESSAGE_TYPE = "RESULT";
                RETURN_MESSAGE = ReturnMessage.Replace("'", "");
                // MalaffiHL7MessageMasterAdd();


                objCom = new CommonBAL();
                string Criteria1 = " HMHM_UPLOAD_STATUS ='PENDING' ";
                Criteria1 += " AND HMHM_BRANCH_ID='" + BRANCH_ID + "' AND HMHM_PT_ID='" + PT_ID + "'";
                Criteria1 += " AND HMHM_LAB_TEST_REPORT_ID='" + LAB_TEST_REPORT_ID + "'";
                Criteria1 += " AND HMHM_MESSAGE_TYPE='" + MESSAGE_TYPE + "' and HMHM_MESSAGE_CODE='" + MessageEvent + "'";

                string FieldNameWithValues = "HMHM_MESSAGE_ID='" + MessageControlID + "', HMHM_FILE_NAME='" + FileName + "' ";
                if (MessageSent == 1)
                {
                    FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'UPLOADED' ";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria1);

                }
                if (MessageSent == 2)
                {
                    FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'ERROR' ";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria1);

                }
                if (MessageSent == 3)
                {
                    FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'FILE_CREATED' ";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria1);

                }

                string FieldNameWithValues1 = "HMHM_RETURN_MESSAGE='" + RETURN_MESSAGE + "'";
                objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            LoopEnd: ;
            }

        FunEnd: ;
            if (isError == true)
            {
                return false;
            }


            //}
            //catch (Exception ex)
            //{
            //    LogFileWriting("-----------------------------------------------");
            //    LogFileWriting(System.DateTime.Now.ToString() + "LabMessageGenerate_ORU_R01");
            //    LogFileWriting(ex.Message.ToString());
            //}

            return true;

        }


        public Boolean VitalMessageGenerate_ORU_R01()
        {
            Boolean isError = false;
            MessageType = "ORU^R01";
            MessageEvent = "ORU-R01";
            MESSAGE_TYPE = "RESULT_VITAL";

            LogFileWriting("****************** ORU-R01 VITAL_SIGN" + System.DateTime.Now.ToString() + "******************");

            GetPatientDetails();
            // GetPatientVisitDtls();

            if (ValidateNL7Message() == false)
            {
                isError = true;
                goto FunEnd;
            }

            testHl7MessageToTransmit = new StringBuilder();

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND EPV_ID = '" + EMR_ID + "' AND EPV_RESULT_STATUS IN ('F','C','X')";

            //\r

            DS = objCom.fnGetFieldValue("*", "EMR_PT_VITALS", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {

                    string RadCode = "8716-3";
                    string RadName = "", CodeTypeDesc = "";
                    //GetServiceMasterName("HSM_HAAD_CODE", RadCode, out  RadName, out CodeTypeDesc);   // Convert.ToString(DS.Tables[0].Rows[i]["RTR_SERV_DESCRIPTION"]);

                    RadName = "Vital Signs Panel";
                    CodeTypeDesc = "LOINC";
                    //string RawObsValue = Convert.ToString(DS.Tables[0].Rows[0]["RTR_REPORTDesc"]);

                    //RawObsValue = RawObsValue.Replace("&nbsp;", string.Empty);
                    //string ObsValue = Regex.Replace(RawObsValue, "<.*?>", String.Empty);

                    //ObsValue = RemoveSplChar(Convert.ToString(ObsValue.Trim()));

                    //ObsValue = ObsValue.Replace("&nbsp;", string.Empty);
                    //ObsValue = ObsValue.Replace("\n", string.Empty);
                    //ObsValue = ObsValue.Replace(Environment.NewLine, string.Empty);

                    string ResultStatus = Convert.ToString(DS.Tables[0].Rows[0]["EPV_RESULT_STATUS"]);
                    //string DiagnosticService;

                    //if (DS.Tables[0].Rows[0].IsNull("RTR_DIAGNOSTIC_SERVICE") == false && Convert.ToString(DS.Tables[0].Rows[0]["RTR_DIAGNOSTIC_SERVICE"]) != "")
                    //{
                    //    DiagnosticService = Convert.ToString(DS.Tables[0].Rows[0]["RTR_DIAGNOSTIC_SERVICE"]);
                    //}
                    //else
                    //{
                    //    DiagnosticService = "RAD";
                    //}

                    //RAD_TEST_REPORT_ID = Convert.ToString(DS.Tables[0].Rows[0]["RTR_TRANS_NO"]);

                    //Int32 SLNo = i + 1;

                    string RequestedDateTime = "", ObservStart = "", ObservEnd = "", ObservDate = "", OrderTransDate = "";

                    if (DS.Tables[0].Rows[0].IsNull("EPV_CREATED_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPV_CREATED_DATE"]) != "")
                    {
                        DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_CREATED_DATE"]);

                        string strMonth = dtStartDate.Month.ToString();
                        if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                        string strDay = dtStartDate.Day.ToString();
                        if (strDay.Length == 1) { strDay = "0" + strDay; }

                        string strHour = dtStartDate.Hour.ToString();
                        if (strHour.Length == 1) { strHour = "0" + strHour; }

                        string strMinute = dtStartDate.Minute.ToString();
                        if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                        RequestedDateTime = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";
                    }

                    if (DS.Tables[0].Rows[0].IsNull("EPV_CREATED_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPV_CREATED_DATE"]) != "")
                    {
                        DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_CREATED_DATE"]);

                        string strMonth = dtStartDate.Month.ToString();
                        if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                        string strDay = dtStartDate.Day.ToString();
                        if (strDay.Length == 1) { strDay = "0" + strDay; }

                        string strHour = dtStartDate.Hour.ToString();
                        if (strHour.Length == 1) { strHour = "0" + strHour; }

                        string strMinute = dtStartDate.Minute.ToString();
                        if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                        ObservStart = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";
                    }

                    if (DS.Tables[0].Rows[0].IsNull("EPV_CREATED_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPV_CREATED_DATE"]) != "")
                    {
                        DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_CREATED_DATE"]);

                        string strMonth = dtStartDate.Month.ToString();
                        if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                        string strDay = dtStartDate.Day.ToString();
                        if (strDay.Length == 1) { strDay = "0" + strDay; }

                        string strHour = dtStartDate.Hour.ToString();
                        if (strHour.Length == 1) { strHour = "0" + strHour; }

                        string strMinute = dtStartDate.Minute.ToString();
                        if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                        ObservEnd = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";
                    }

                    if (DS.Tables[0].Rows[0].IsNull("EPV_CREATED_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPV_CREATED_DATE"]) != "")
                    {
                        DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_CREATED_DATE"]);

                        string strMonth = dtStartDate.Month.ToString();
                        if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                        string strDay = dtStartDate.Day.ToString();
                        if (strDay.Length == 1) { strDay = "0" + strDay; }

                        string strHour = dtStartDate.Hour.ToString();
                        if (strHour.Length == 1) { strHour = "0" + strHour; }

                        string strMinute = dtStartDate.Minute.ToString();
                        if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                        ObservDate = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                    }

                    if (DS.Tables[0].Rows[0].IsNull("EPV_CREATED_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPV_CREATED_DATE"]) != "")
                    {
                        DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_CREATED_DATE"]);

                        string strMonth = dtStartDate.Month.ToString();
                        if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                        string strDay = dtStartDate.Day.ToString();
                        if (strDay.Length == 1) { strDay = "0" + strDay; }

                        string strHour = dtStartDate.Hour.ToString();
                        if (strHour.Length == 1) { strHour = "0" + strHour; }

                        string strMinute = dtStartDate.Minute.ToString();
                        if (strMinute.Length == 1) { strMinute = "0" + strMinute; }
                        OrderTransDate = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";
                    }

                    //if (ObsValue == "")
                    //{
                    //    goto LoopEnd;
                    //}
                    DataSet DC = new DataSet();
                    CommonBAL objComs = new CommonBAL();
                    string Criterias = " 1=1 ";

<<<<<<< HEAD
                    string OrderingProviderID, OrderingProviderName;
                    string DR_ID = Convert.ToString(DS.Tables[0].Rows[0]["EPM_DR_CODE"]).Trim(); //LTRM_DR_CODE

                    string DrFirstName, DrMiddleName, DrLastName, LicenseNo, DrDeptName;
                    GetStaffDetails(DR_ID, out   DrFirstName, out DrMiddleName, out   DrLastName, out   LicenseNo, out DrDeptName);
=======
                    Criterias += " AND EPM_ID = '" + EMR_ID + "'";

                    DC = objCom.fnGetFieldValue("*", "EMR_PT_MASTER", Criterias, "EPM_ID");
>>>>>>> vitalsignHL7Add

                    string OrderingProviderID, OrderingProviderName;
                    string DR_ID = Convert.ToString(DC.Tables[0].Rows[0]["EPM_DR_CODE"]).Trim(); //LTRM_DR_CODE

<<<<<<< HEAD
                    if (LicenseNo == "")
                    {
                        GetRefDrfDetails(DR_ID, out   DrFirstName, out DrMiddleName, out   DrLastName, out   LicenseNo, out DrDeptName);
                    }

                    LogFileWriting("VitalSignGenerate_ORU_R01 3");

                    if (LicenseNo != "")
                    {
                        OrderingProviderID = LicenseNo;
                    }
                    else
                    {
                        OrderingProviderID = DR_ID;
                    }

                    if (DrFirstName.Length > 20)
                    {
                        DrFirstName = DrFirstName.Substring(0, 15);
                    }
                    if (DrLastName.Length > 20)
                    {
                        DrLastName = DrLastName.Substring(0, 15);
                    }
=======
                    string DrFirstName, DrMiddleName, DrLastName, LicenseNo, DrDeptName;
                    GetStaffDetails(DR_ID, out   DrFirstName, out DrMiddleName, out   DrLastName, out   LicenseNo, out DrDeptName);


                    if (LicenseNo == "")
                    {
                        GetRefDrfDetails(DR_ID, out   DrFirstName, out DrMiddleName, out   DrLastName, out   LicenseNo, out DrDeptName);
                    }

                    LogFileWriting("VitalSignGenerate_ORU_R01 3");
>>>>>>> vitalsignHL7Add

                    if (LicenseNo != "")
                    {
                        OrderingProviderID = LicenseNo;
                    }
                    else
                    {
                        OrderingProviderID = DR_ID;
                    }

<<<<<<< HEAD
=======
                    if (DrFirstName.Length > 20)
                    {
                        DrFirstName = DrFirstName.Substring(0, 15);
                    }
                    if (DrLastName.Length > 20)
                    {
                        DrLastName = DrLastName.Substring(0, 15);
                    }


>>>>>>> vitalsignHL7Add
                    if (DrLastName != "")
                    {
                        OrderingProviderName = DrLastName + "^" + DrFirstName + "^" + DrMiddleName;      // DrFirstName + "^" + DrLastName;
                    }
                    else
                    {
                        OrderingProviderName = DrFirstName + "^" + DrFirstName;
                    }



                    //LogFileWriting("VitalSignGenerate_ORU_R01 4");

                    string ResultInterpreterID, ResultInterpreterName;
                    string RefDR_ID = Convert.ToString(DC.Tables[0].Rows[0]["EPM_DR_CODE"]).Trim(); //LTRM_REF_DR
                    // string 
                    string RefDrFirstName, RefDrMiddleName, RefDrLastName, RefLicenseNo, RefDrSpecialtyName;
                    RAD_TEST_REPORT_ID = "3334731-27";
                    GetStaffDetails(RefDR_ID, out   RefDrFirstName, out RefDrMiddleName, out   RefDrLastName, out   RefLicenseNo, out RefDrSpecialtyName);

                    LogFileWriting("VitalSignGenerate_ORU_R01 5");
                    if (RefLicenseNo != "")
                    {
                        ResultInterpreterID = RefLicenseNo;
                    }
                    else
                    {
                        ResultInterpreterID = RefDR_ID;
                    }

                    if (RefDrFirstName.Length > 20)
                    {
                        RefDrFirstName = RefDrFirstName.Substring(0, 15);
                    }
                    if (RefDrLastName.Length > 20)
                    {
                        RefDrLastName = RefDrLastName.Substring(0, 15);
                    }


                    // if (RefDrLastName != "")
                    // {
                    ResultInterpreterName = RefDrLastName + "&" + RefDrFirstName + "&" + RefDrMiddleName;  // RefDrFirstName + "&" + RefDrLastName;
                    //}
                    //else
                    //{
                    //    ResultInterpreterName = RefDrFirstName + "&" + RefDrFirstName;
                    //}





                    MessageHeader();
                    BindPatientIdentification_PID();
                    string RefDrSpecialty = GetCommnMasterCode(RefDrSpecialtyName, "Specialty");

                    BindPatientLabVisitAdditionalInformation_PV1(RefDrSpecialty, RefDrSpecialtyName, ResultInterpreterID, ResultInterpreterName, OrderingProviderID, OrderingProviderName, RAD_TEST_REPORT_ID, RequestedDateTime);


                    string ActionCode = "NW";
                    if (ResultStatus == "C")
                    {
                        ActionCode = "RU";
                    }
                    else if (ResultStatus == "X")
                    {
                        ActionCode = "DE";
                    }
                    else
                    {
                        ActionCode = "NW";
                    }

                    BindOrderControl_ORC(ActionCode, 1, RAD_TEST_REPORT_ID, ConsultingDr, ConsultingDrName, OrderTransDate, "", "active", OrderTransDate);


                    if (RadCode == "" || RadName == "" || RequestedDateTime == "" || ObservStart == "" || ObservEnd == "" || ObservDate == "" || OrderTransDate == "" || OrderingProviderID == "" || OrderingProviderName == "" || ResultInterpreterID == "" || ResultInterpreterName == "")
                    {
                        LogFileWriting("Vital_SignMessageGenerate_ORU_R01 function Some details missing   RTR_TRANS_NO =" + RAD_TEST_REPORT_ID);
                        goto LoopEnd;

                    }
                    // BindLabObservationRequest_OBR(RAD_TEST_REPORT_ID, RadCode, RadName, RequestedDateTime, ObservStart, ObservEnd, DeliveryDateTime, AuthorizDate, DiagnosticService, ResultStatus, OrderingProviderID, OrderingProviderName, ResultInterpreterID, ResultInterpreterName);
                    BindObservationRequest_OBR(1, RAD_TEST_REPORT_ID, RadCode, RadName, CodeTypeDesc, "", ObservStart, ObservEnd, RequestedDateTime, "", OrderTransDate, "", ResultStatus, OrderingProviderID, OrderingProviderName, ResultInterpreterID, ResultInterpreterName, "");
<<<<<<< HEAD
=======

                    //CommonBAL objCom = new CommonBAL();
                    string ObsValue = "", ObsUnit = "";
                    Int32 intSlNo = 0;
                    DataSet CM = new DataSet();
                    Criteria = " 1=1 AND  HCM_TYPE='VITAL_SIGN'  ";
                   CM = objCom.fnGetFieldValue("* ", "HMS_COMMON_MASTERS", Criteria, "");
                    if (CM.Tables[0].Rows.Count > 0) {
                        //foreach (DataRow Dr in CM.Tables[0].Rows) {
                        //   // if(Dr.Equals("HCM_CODE") == 9279-1)
                        //    if (Dr.ItemArray.Equals("9279-1")) // GetValue(2).Equals(9279 - 1))
                        //    {
                        //        LogFileWriting("Foeach Testing");
                        //    }

                        //    LogFileWriting("Foeach Testing 1");
                        //}
                        int k; string ObservationType = "", CollectionMethod = "";
                        for ( k = 0; k < CM.Tables[0].Rows.Count; k++) {
                            RadCode  = CM.Tables[0].Rows[k]["HCM_CODE"].ToString();
                            if ((RadCode == "9279-1") && (DS.Tables[0].Rows[0].IsNull("EPV_RESPIRATION") != null || DS.Tables[0].Rows[0].IsNull("EPV_RESPIRATION") != false))
                            {
                                RadName = Convert.ToString(CM.Tables[0].Rows[k]["HCM_DESC"]);
                                ObsUnit = Convert.ToString(CM.Tables[0].Rows[k]["HCM_UNIT_CODE"]);
                                ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                                BindObservationResult_OBX(false, k, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                            } if ((RadCode == "8867-4") && (DS.Tables[0].Rows[0].IsNull("EPV_HEART_RATE") != null || DS.Tables[0].Rows[0].IsNull("EPV_HEART_RATE") != false))
                            {
                                RadName = Convert.ToString(CM.Tables[0].Rows[k]["HCM_DESC"]);
                                ObsUnit = Convert.ToString(CM.Tables[0].Rows[k]["HCM_UNIT_CODE"]);
                                ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_HEART_RATE"]);
                                BindObservationResult_OBX(false, k, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                            }
                            if ((RadCode == "8885-6") && (DS.Tables[0].Rows[0].IsNull("EPV_HEART_RATE_MEAS_SITE") != null || DS.Tables[0].Rows[0].IsNull("EPV_HEART_RATE_MEAS_SITE") != false))
                            {
                                RadName = Convert.ToString(CM.Tables[0].Rows[k]["HCM_DESC"]);
                                ObsUnit = Convert.ToString(CM.Tables[0].Rows[k]["HCM_UNIT_CODE"]);
                                ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_HEART_RATE_MEAS_SITE"]);
                                BindObservationResult_OBX(false, k, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                            }
                            if ((RadCode == "59408-5") && (DS.Tables[0].Rows[0].IsNull("EPV_SPO2") != null || DS.Tables[0].Rows[0].IsNull("EPV_SPO2") != false))
                            {
                                RadName = Convert.ToString(CM.Tables[0].Rows[k]["HCM_DESC"]);
                                ObsUnit = Convert.ToString(CM.Tables[0].Rows[k]["HCM_UNIT_CODE"]);
                                ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_SPO2"]);
                                BindObservationResult_OBX(false, k, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                            }
                            if ((RadCode == "8310-5") && (DS.Tables[0].Rows[0].IsNull("EPV_TEMPERATURE") != null || DS.Tables[0].Rows[0].IsNull("EPV_TEMPERATURE") != false))
                            {
                                RadName = Convert.ToString(CM.Tables[0].Rows[k]["HCM_DESC"]);
                                ObsUnit = Convert.ToString(CM.Tables[0].Rows[k]["HCM_UNIT_CODE"]);
                                ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_TEMPERATURE"]);
                                BindObservationResult_OBX(false, k, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                            }
                            if ((RadCode == "8327-9") && (DS.Tables[0].Rows[0].IsNull("EPV_BODY_TEMP_MEAS_SITE") != null || DS.Tables[0].Rows[0].IsNull("EPV_BODY_TEMP_MEAS_SITE") != false))
                            {
                                RadName = Convert.ToString(CM.Tables[0].Rows[k]["HCM_DESC"]);
                                ObsUnit = Convert.ToString(CM.Tables[0].Rows[k]["HCM_UNIT_CODE"]);
                                ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BODY_TEMP_MEAS_SITE"]);
                                BindObservationResult_OBX(false, k, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                                ObservationType = "Body Temperature Measurement Site";
                                CollectionMethod = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BODY_TEMP_MEAS_SITE"]);
                                BindNotesAndCommentsForVitalsign_NTE(ObservationType, CollectionMethod);
                            }
                            if ((RadCode == "8480-6") && (DS.Tables[0].Rows[0].IsNull("EPV_BP_SYSTOLIC") != null || DS.Tables[0].Rows[0].IsNull("EPV_BP_SYSTOLIC") != false))
                            {
                                RadName = Convert.ToString(CM.Tables[0].Rows[k]["HCM_DESC"]);
                                ObsUnit = Convert.ToString(CM.Tables[0].Rows[k]["HCM_UNIT_CODE"]);
                                ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BP_SYSTOLIC"]);
                                BindObservationResult_OBX(false, k, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                            }
                            if ((RadCode == "8462-4") && (DS.Tables[0].Rows[0].IsNull("EPV_BP_DIASTOLIC") != null || DS.Tables[0].Rows[0].IsNull("EPV_BP_DIASTOLIC") != false))
                            {
                                RadName = Convert.ToString(CM.Tables[0].Rows[k]["HCM_DESC"]);
                                ObsUnit = Convert.ToString(CM.Tables[0].Rows[k]["HCM_UNIT_CODE"]);
                                ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BP_DIASTOLIC"]);
                                BindObservationResult_OBX(false, k, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                            }
                            if ((RadCode == "8487-0") && (DS.Tables[0].Rows[0].IsNull("EPV_MEAN_BP") != null || DS.Tables[0].Rows[0].IsNull("EPV_MEAN_BP") != false))
                            {
                                RadName = Convert.ToString(CM.Tables[0].Rows[k]["HCM_DESC"]);
                                ObsUnit = Convert.ToString(CM.Tables[0].Rows[k]["HCM_UNIT_CODE"]);
                                ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_MEAN_BP"]);
                                BindObservationResult_OBX(false, k, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                            }
                            if ((RadCode == "41904-4") && (DS.Tables[0].Rows[0].IsNull("EPV_PRESSURE_MEAS_SITE") != null || DS.Tables[0].Rows[0].IsNull("EPV_PRESSURE_MEAS_SITE") != false))
                            {
                                RadName = Convert.ToString(CM.Tables[0].Rows[k]["HCM_DESC"]);
                                ObsUnit = Convert.ToString(CM.Tables[0].Rows[k]["HCM_UNIT_CODE"]);
                                ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_PRESSURE_MEAS_SITE"]);
                                BindObservationResult_OBX(false, k, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                                ObservationType = "Blood Pressure Measurement Site";
                                CollectionMethod = Convert.ToString(DS.Tables[0].Rows[0]["EPV_PRESSURE_MEAS_SITE"]);
                                BindNotesAndCommentsForVitalsign_NTE(ObservationType, CollectionMethod);
                            }
                            if ((RadCode == "8302-2") && (DS.Tables[0].Rows[0].IsNull("EPV_HEIGHT") != null || DS.Tables[0].Rows[0].IsNull("EPV_HEIGHT") != false))
                            {
                                RadName = Convert.ToString(CM.Tables[0].Rows[k]["HCM_DESC"]);
                                ObsUnit = Convert.ToString(CM.Tables[0].Rows[k]["HCM_UNIT_CODE"]);
                                ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_HEIGHT"]);
                                BindObservationResult_OBX(false, k, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                            }
                            if ((RadCode == "8306-3") && (DS.Tables[0].Rows[0].IsNull("EPV_BODY_HEIGHT_LYING") != null || DS.Tables[0].Rows[0].IsNull("EPV_BODY_HEIGHT_LYING") != false))
                            {
                                RadName = Convert.ToString(CM.Tables[0].Rows[k]["HCM_DESC"]);
                                ObsUnit = Convert.ToString(CM.Tables[0].Rows[k]["HCM_UNIT_CODE"]);
                                ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BODY_HEIGHT_LYING"]);
                                BindObservationResult_OBX(false, k, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                            }
                            if ((RadCode == "8287-5") && (DS.Tables[0].Rows[0].IsNull("EMR_PT_HEADCIRC") != null || DS.Tables[0].Rows[0].IsNull("EMR_PT_HEADCIRC") != false))
                            {
                                RadName = Convert.ToString(CM.Tables[0].Rows[k]["HCM_DESC"]);
                                ObsUnit = Convert.ToString(CM.Tables[0].Rows[k]["HCM_UNIT_CODE"]);
                                ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_HEADCIRC"]);
                                BindObservationResult_OBX(false, k, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                            }
                            if ((RadCode == "3141-9") && (DS.Tables[0].Rows[0].IsNull("EPV_WEIGHT") != null || DS.Tables[0].Rows[0].IsNull("EPV_WEIGHT") != false))
                            {
                                RadName = Convert.ToString(CM.Tables[0].Rows[k]["HCM_DESC"]);
                                ObsUnit = Convert.ToString(CM.Tables[0].Rows[k]["HCM_UNIT_CODE"]);
                                ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_WEIGHT"]);
                                BindObservationResult_OBX(false, k, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                            }
                            if ((RadCode == "39156-5") && (DS.Tables[0].Rows[0].IsNull("EPV_BMI") != null || DS.Tables[0].Rows[0].IsNull("EPV_BMI") != false))
                            {
                                RadName = Convert.ToString(CM.Tables[0].Rows[k]["HCM_DESC"]);
                                ObsUnit = Convert.ToString(CM.Tables[0].Rows[k]["HCM_UNIT_CODE"]);
                                ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BMI"]);
                                BindObservationResult_OBX(false, k, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                            }
                            
                        }
                    }
>>>>>>> vitalsignHL7Add

                    // BindRadObservationRequest_OBR(RadCode, RadName, ObsDateTime, SLNo, RequestedDateTime, ObservStart, ObservEnd, AuthorizDate);
                    //string ObsValue = "", ObsUnit = "" ;
                    //Int32 intSlNo = 0;


                    //if (DS.Tables[0].Rows[0].IsNull("EPV_RESPIRATION") != null || DS.Tables[0].Rows[0].IsNull("EPV_RESPIRATION") != false)
                    //{
                    //    intSlNo = 1;
                    //    RadCode = "9279-1";
                    //    //Criteria += " AND HCM_CODE='" + RadCode + "'";
                    //    //CM = objCom.fnGetFieldValue("  TOP 1  * ", "HMS_COMMON_MASTERS", Criteria, "");
                    //    //if (CM.Tables[0].Rows[0].IsNull("HCM_CODE") == 9279-1) { 
                        
                    //    //}
                    //    if (CM.Tables[0].Rows.Count > 0)
                    //    {
                    //        RadName = Convert.ToString(CM.Tables[0].Rows[0]["HCM_DESC"]);
                    //        ObsUnit = Convert.ToString(CM.Tables[0].Rows[0]["HCM_UNIT_CODE"]);
                    //    }
                    //    ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                    //    //ObservDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                    //    BindObservationResult_OBX(false, intSlNo, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                    //    // BindObservationResult_OBX(Boolean MmicrobiologyValues, Int32 intSlNo, string LabCode, string LabDesc, string strSysCode, string ObsSubID, string ObsValue, string ObsUnit, string TestDateTime, string AbnormalFlag, string Range, string ResultStatus, string OrderingProviderID, string OrderingProviderName)
                    //    intSlNo = intSlNo + 1;
                    
                    //}
                    //if (DS.Tables[0].Rows[0].IsNull("EPV_HEART_RATE") != null || DS.Tables[0].Rows[0].IsNull("EPV_HEART_RATE") != false)
                    //{
                    //    RadCode = "8867-4";
                    //    Criteria += " AND HCM_CODE='" + RadCode + "'";
                    //    CM = objCom.fnGetFieldValue("  TOP 1  * ", "HMS_COMMON_MASTERS", Criteria, "");
                    //    if (CM.Tables[0].Rows.Count > 0)
                    //    {
                    //        RadName = Convert.ToString(CM.Tables[0].Rows[0]["HCM_DESC"]);
                    //        ObsUnit = Convert.ToString(CM.Tables[0].Rows[0]["HCM_UNIT_CODE"]);
                    //    }
                    //    ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_HEART_RATE"]);
                    //    //ObservDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                    //    BindObservationResult_OBX(false, intSlNo, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                    //    // BindObservationResult_OBX(Boolean MmicrobiologyValues, Int32 intSlNo, string LabCode, string LabDesc, string strSysCode, string ObsSubID, string ObsValue, string ObsUnit, string TestDateTime, string AbnormalFlag, string Range, string ResultStatus, string OrderingProviderID, string OrderingProviderName)
                    //    intSlNo = intSlNo + 1;
                    //}
                    //if (DS.Tables[0].Rows[0].IsNull("EPV_HEART_RATE_MEAS_SITE") != null || DS.Tables[0].Rows[0].IsNull("EPV_HEART_RATE_MEAS_SITE") != false)
                    //{
                    //    RadCode = "8885-6";
                    //    Criteria += " AND HCM_CODE='" + RadCode + "'";
                    //    CM = objCom.fnGetFieldValue("  TOP 1  * ", "HMS_COMMON_MASTERS", Criteria, "");
                    //    if (CM.Tables[0].Rows.Count > 0)
                    //    {
                    //        RadName = Convert.ToString(CM.Tables[0].Rows[0]["HCM_DESC"]);
                    //        ObsUnit = Convert.ToString(CM.Tables[0].Rows[0]["HCM_UNIT_CODE"]);
                    //    }
                    //    ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_HEART_RATE_MEAS_SITE"]);
                    //    //ObservDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                    //    BindObservationResult_OBX(false, intSlNo, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                    //    // BindObservationResult_OBX(Boolean MmicrobiologyValues, Int32 intSlNo, string LabCode, string LabDesc, string strSysCode, string ObsSubID, string ObsValue, string ObsUnit, string TestDateTime, string AbnormalFlag, string Range, string ResultStatus, string OrderingProviderID, string OrderingProviderName)
                    //    intSlNo = intSlNo + 1;
                    //}
                    //if (DS.Tables[0].Rows[0].IsNull("EPV_SPO2") != null || DS.Tables[0].Rows[0].IsNull("EPV_SPO2") != false)
                    //{
                    //    RadCode = "59408-5";
                    //    Criteria += " AND HCM_CODE='" + RadCode + "'";
                    //    CM = objCom.fnGetFieldValue("  TOP 1  * ", "HMS_COMMON_MASTERS", Criteria, "");
                    //    if (CM.Tables[0].Rows.Count > 0)
                    //    {
                    //        RadName = Convert.ToString(CM.Tables[0].Rows[0]["HCM_DESC"]);
                    //        ObsUnit = Convert.ToString(CM.Tables[0].Rows[0]["HCM_UNIT_CODE"]);
                    //    }
                    //    ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_SPO2"]);
                    //    //ObservDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                    //    BindObservationResult_OBX(false, intSlNo, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                    //    // BindObservationResult_OBX(Boolean MmicrobiologyValues, Int32 intSlNo, string LabCode, string LabDesc, string strSysCode, string ObsSubID, string ObsValue, string ObsUnit, string TestDateTime, string AbnormalFlag, string Range, string ResultStatus, string OrderingProviderID, string OrderingProviderName)
                    //    intSlNo = intSlNo + 1;
                    //}
                    //if (DS.Tables[0].Rows[0].IsNull("EPV_TEMPERATURE") != null || DS.Tables[0].Rows[0].IsNull("EPV_TEMPERATURE") != false)
                    //{
                    //    RadCode = "8310-5";
                    //    Criteria += " AND HCM_CODE='" + RadCode + "'";
                    //    CM = objCom.fnGetFieldValue("  TOP 1  * ", "HMS_COMMON_MASTERS", Criteria, "");
                    //    if (CM.Tables[0].Rows.Count > 0)
                    //    {
                    //        RadName = Convert.ToString(CM.Tables[0].Rows[0]["HCM_DESC"]);
                    //        ObsUnit = Convert.ToString(CM.Tables[0].Rows[0]["HCM_UNIT_CODE"]);
                    //    }
                    //    ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_TEMPERATURE"]);
                    //    //ObservDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                    //    BindObservationResult_OBX(false, intSlNo, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                    //    // BindObservationResult_OBX(Boolean MmicrobiologyValues, Int32 intSlNo, string LabCode, string LabDesc, string strSysCode, string ObsSubID, string ObsValue, string ObsUnit, string TestDateTime, string AbnormalFlag, string Range, string ResultStatus, string OrderingProviderID, string OrderingProviderName)
                    //    intSlNo = intSlNo + 1;
                    //}
                    //if (DS.Tables[0].Rows[0].IsNull("EPV_BODY_TEMP_MEAS_SITE") != null || DS.Tables[0].Rows[0].IsNull("EPV_BODY_TEMP_MEAS_SITE") != false)
                    //{
                    //    RadCode = "8327-9";
                    //    Criteria += " AND HCM_CODE='" + RadCode + "'";
                    //    CM = objCom.fnGetFieldValue("  TOP 1  * ", "HMS_COMMON_MASTERS", Criteria, "");
                    //    if (CM.Tables[0].Rows.Count > 0)
                    //    {
                    //        RadName = Convert.ToString(CM.Tables[0].Rows[0]["HCM_DESC"]);
                    //        ObsUnit = Convert.ToString(CM.Tables[0].Rows[0]["HCM_UNIT_CODE"]);
                    //    }
                    //    ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BODY_TEMP_MEAS_SITE"]);
                    //    //ObservDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                    //    BindObservationResult_OBX(false, intSlNo, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                    //    // BindObservationResult_OBX(Boolean MmicrobiologyValues, Int32 intSlNo, string LabCode, string LabDesc, string strSysCode, string ObsSubID, string ObsValue, string ObsUnit, string TestDateTime, string AbnormalFlag, string Range, string ResultStatus, string OrderingProviderID, string OrderingProviderName)
                    //    intSlNo = intSlNo + 1;
                    //}

                    //string CollectionMethod = "", ObservationType = "";

                    //if (DS.Tables[0].Rows[0].IsNull("EPV_BODY_TEMP_MEAS_SITE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPV_BODY_TEMP_MEAS_SITE"]) != "")
                    //{
                    //    ObservationType = "Body Temperature Measurement Site";
                    //    CollectionMethod = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BODY_TEMP_MEAS_SITE"]);
                    //}
                    //if (CollectionMethod != "")
                    //{
                    //    BindNotesAndCommentsForVitalsign_NTE(ObservationType, CollectionMethod);
                    //}
                    //if (DS.Tables[0].Rows[0].IsNull("EPV_BP_SYSTOLIC") != null || DS.Tables[0].Rows[0].IsNull("EPV_BP_SYSTOLIC") != false)
                    //{
                    //    RadCode = "8480-6";
                    //    Criteria += " AND HCM_CODE='" + RadCode + "'";
                    //    CM = objCom.fnGetFieldValue("  TOP 1  * ", "HMS_COMMON_MASTERS", Criteria, "");
                    //    if (CM.Tables[0].Rows.Count > 0)
                    //    {
                    //        RadName = Convert.ToString(CM.Tables[0].Rows[0]["HCM_DESC"]);
                    //        ObsUnit = Convert.ToString(CM.Tables[0].Rows[0]["HCM_UNIT_CODE"]);
                    //    }
                    //    ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_HEART_RATE"]);
                    //    //ObservDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                    //    BindObservationResult_OBX(false, intSlNo, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                    //    // BindObservationResult_OBX(Boolean MmicrobiologyValues, Int32 intSlNo, string LabCode, string LabDesc, string strSysCode, string ObsSubID, string ObsValue, string ObsUnit, string TestDateTime, string AbnormalFlag, string Range, string ResultStatus, string OrderingProviderID, string OrderingProviderName)
                    //    intSlNo = intSlNo + 1;
                    //}
                    //if (DS.Tables[0].Rows[0].IsNull("EPV_BP_DIASTOLIC") != null || DS.Tables[0].Rows[0].IsNull("EPV_BP_SYSTOLIC") != false)
                    //{
                    //    RadCode = "8462-4";
                    //    Criteria += " AND HCM_CODE='" + RadCode + "'";
                    //    CM = objCom.fnGetFieldValue("  TOP 1  * ", "HMS_COMMON_MASTERS", Criteria, "");
                    //    if (CM.Tables[0].Rows.Count > 0)
                    //    {
                    //        RadName = Convert.ToString(CM.Tables[0].Rows[0]["HCM_DESC"]);
                    //        ObsUnit = Convert.ToString(CM.Tables[0].Rows[0]["HCM_UNIT_CODE"]);
                    //    }
                    //    ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BP_DIASTOLIC"]);
                    //    //ObservDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                    //    BindObservationResult_OBX(false, intSlNo, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                    //    // BindObservationResult_OBX(Boolean MmicrobiologyValues, Int32 intSlNo, string LabCode, string LabDesc, string strSysCode, string ObsSubID, string ObsValue, string ObsUnit, string TestDateTime, string AbnormalFlag, string Range, string ResultStatus, string OrderingProviderID, string OrderingProviderName)
                    //    intSlNo = intSlNo + 1;
                    //}
                    //if (DS.Tables[0].Rows[0].IsNull("EPV_MEAN_BP") != null || DS.Tables[0].Rows[0].IsNull("EPV_MEAN_BP") != false)
                    //{
                    //    RadCode = "8487-0";
                    //    Criteria += " AND HCM_CODE='" + RadCode + "'";
                    //    CM = objCom.fnGetFieldValue("  TOP 1  * ", "HMS_COMMON_MASTERS", Criteria, "");
                    //    if (CM.Tables[0].Rows.Count > 0)
                    //    {
                    //        RadName = Convert.ToString(CM.Tables[0].Rows[0]["HCM_DESC"]);
                    //        ObsUnit = Convert.ToString(CM.Tables[0].Rows[0]["HCM_UNIT_CODE"]);
                    //    }
                    //    ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_MEAN_BP"]);
                    //    //ObservDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                    //    BindObservationResult_OBX(false, intSlNo, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                    //    // BindObservationResult_OBX(Boolean MmicrobiologyValues, Int32 intSlNo, string LabCode, string LabDesc, string strSysCode, string ObsSubID, string ObsValue, string ObsUnit, string TestDateTime, string AbnormalFlag, string Range, string ResultStatus, string OrderingProviderID, string OrderingProviderName)
                    //    intSlNo = intSlNo + 1;
                    //}
                    //if (DS.Tables[0].Rows[0].IsNull("EPV_PRESSURE_MEAS_SITE") != null || DS.Tables[0].Rows[0].IsNull("EPV_PRESSURE_MEAS_SITE") != false)
                    //{
                    //    RadCode = "41904-4";
                    //    Criteria += " AND HCM_CODE='" + RadCode + "'";
                    //    CM = objCom.fnGetFieldValue("  TOP 1  * ", "HMS_COMMON_MASTERS", Criteria, "");
                    //    if (CM.Tables[0].Rows.Count > 0)
                    //    {
                    //        RadName = Convert.ToString(CM.Tables[0].Rows[0]["HCM_DESC"]);
                    //        ObsUnit = Convert.ToString(CM.Tables[0].Rows[0]["HCM_UNIT_CODE"]);
                    //    }
                    //    ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_PRESSURE_MEAS_SITE"]);
                    //    //ObservDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                    //    BindObservationResult_OBX(false, intSlNo, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                    //    // BindObservationResult_OBX(Boolean MmicrobiologyValues, Int32 intSlNo, string LabCode, string LabDesc, string strSysCode, string ObsSubID, string ObsValue, string ObsUnit, string TestDateTime, string AbnormalFlag, string Range, string ResultStatus, string OrderingProviderID, string OrderingProviderName)
                    //    intSlNo = intSlNo + 1;
                    //}
                    //if (DS.Tables[0].Rows[0].IsNull("EPV_PRESSURE_MEAS_SITE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPV_BODY_TEMP_MEAS_SITE"]) != "")
                    //{
                    //    ObservationType = "Blood Pressure Measurement Site";
                    //    CollectionMethod = Convert.ToString(DS.Tables[0].Rows[0]["EPV_PRESSURE_MEAS_SITE"]);
                    //}
                    //if (CollectionMethod != "")
                    //{
                    //    BindNotesAndCommentsForVitalsign_NTE(ObservationType, CollectionMethod);
                    //}
                    //if (DS.Tables[0].Rows[0].IsNull("EPV_HEIGHT") != null || DS.Tables[0].Rows[0].IsNull("EPV_HEIGHT") != false)
                    //{
                    //    RadCode = "8302-2";
                    //    Criteria += " AND HCM_CODE='" + RadCode + "'";
                    //    CM = objCom.fnGetFieldValue("  TOP 1  * ", "HMS_COMMON_MASTERS", Criteria, "");
                    //    if (CM.Tables[0].Rows.Count > 0)
                    //    {
                    //        RadName = Convert.ToString(CM.Tables[0].Rows[0]["HCM_DESC"]);
                    //        ObsUnit = Convert.ToString(CM.Tables[0].Rows[0]["HCM_UNIT_CODE"]);
                    //    }
                    //    ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_HEIGHT"]);
                    //    //ObservDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                    //    BindObservationResult_OBX(false, intSlNo, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                    //    // BindObservationResult_OBX(Boolean MmicrobiologyValues, Int32 intSlNo, string LabCode, string LabDesc, string strSysCode, string ObsSubID, string ObsValue, string ObsUnit, string TestDateTime, string AbnormalFlag, string Range, string ResultStatus, string OrderingProviderID, string OrderingProviderName)
                    //    intSlNo = intSlNo + 1;
                    //}
                    //if (DS.Tables[0].Rows[0].IsNull("EPV_BODY_HEIGHT_LYING") != null || DS.Tables[0].Rows[0].IsNull("EPV_BODY_HEIGHT_LYING") != false)
                    //{
                    //    RadCode = "8306-3";
                    //    Criteria += " AND HCM_CODE='" + RadCode + "'";
                    //    CM = objCom.fnGetFieldValue("  TOP 1  * ", "HMS_COMMON_MASTERS", Criteria, "");
                    //    if (CM.Tables[0].Rows.Count > 0)
                    //    {
                    //        RadName = Convert.ToString(CM.Tables[0].Rows[0]["HCM_DESC"]);
                    //        ObsUnit = Convert.ToString(CM.Tables[0].Rows[0]["HCM_UNIT_CODE"]);
                    //    }
                    //    ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BODY_HEIGHT_LYING"]);
                    //    //ObservDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                    //    BindObservationResult_OBX(false, intSlNo, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                    //    // BindObservationResult_OBX(Boolean MmicrobiologyValues, Int32 intSlNo, string LabCode, string LabDesc, string strSysCode, string ObsSubID, string ObsValue, string ObsUnit, string TestDateTime, string AbnormalFlag, string Range, string ResultStatus, string OrderingProviderID, string OrderingProviderName)
                    //    intSlNo = intSlNo + 1;
                    //}
                    //if (DS.Tables[0].Rows[0].IsNull("EMR_PT_HEADCIRC") != null || DS.Tables[0].Rows[0].IsNull("EMR_PT_HEADCIRC") != false)
                    //{
                    //    RadCode = "8287-5";
                    //    Criteria += " AND HCM_CODE='" + RadCode + "'";
                    //    CM = objCom.fnGetFieldValue("  TOP 1  * ", "HMS_COMMON_MASTERS", Criteria, "");
                    //    if (CM.Tables[0].Rows.Count > 0)
                    //    {
                    //        RadName = Convert.ToString(CM.Tables[0].Rows[0]["HCM_DESC"]);
                    //        ObsUnit = Convert.ToString(CM.Tables[0].Rows[0]["HCM_UNIT_CODE"]);
                    //    }
                    //    ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_HEADCIRC"]);
                    //    //ObservDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                    //    BindObservationResult_OBX(false, intSlNo, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                    //    // BindObservationResult_OBX(Boolean MmicrobiologyValues, Int32 intSlNo, string LabCode, string LabDesc, string strSysCode, string ObsSubID, string ObsValue, string ObsUnit, string TestDateTime, string AbnormalFlag, string Range, string ResultStatus, string OrderingProviderID, string OrderingProviderName)
                    //    intSlNo = intSlNo + 1;
                    //}
                    //if (DS.Tables[0].Rows[0].IsNull("EPV_WEIGHT") != null || DS.Tables[0].Rows[0].IsNull("EPV_WEIGHT") != false)
                    //{
                    //    RadCode = "3141-9";
                    //    Criteria += " AND HCM_CODE='" + RadCode + "'";
                    //    CM = objCom.fnGetFieldValue("  TOP 1  * ", "HMS_COMMON_MASTERS", Criteria, "");
                    //    if (CM.Tables[0].Rows.Count > 0)
                    //    {
                    //        RadName = Convert.ToString(CM.Tables[0].Rows[0]["HCM_DESC"]);
                    //        ObsUnit = Convert.ToString(CM.Tables[0].Rows[0]["HCM_UNIT_CODE"]);
                    //    }
                    //    ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_WEIGHT"]);
                    //    //ObservDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                    //    BindObservationResult_OBX(false, intSlNo, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                    //    // BindObservationResult_OBX(Boolean MmicrobiologyValues, Int32 intSlNo, string LabCode, string LabDesc, string strSysCode, string ObsSubID, string ObsValue, string ObsUnit, string TestDateTime, string AbnormalFlag, string Range, string ResultStatus, string OrderingProviderID, string OrderingProviderName)
                    //    intSlNo = intSlNo + 1;
                    //}
                    //if (DS.Tables[0].Rows[0].IsNull("EPV_BMI") != null || DS.Tables[0].Rows[0].IsNull("EPV_BMI") != false)
                    //{
                    //    RadCode = "39156-5";
                    //    Criteria += " AND HCM_CODE='" + RadCode + "'";
                    //    CM = objCom.fnGetFieldValue("  TOP 1  * ", "HMS_COMMON_MASTERS", Criteria, "");
                    //    if (CM.Tables[0].Rows.Count > 0)
                    //    {
                    //        RadName = Convert.ToString(CM.Tables[0].Rows[0]["HCM_DESC"]);
                    //        ObsUnit = Convert.ToString(CM.Tables[0].Rows[0]["HCM_UNIT_CODE"]);
                    //    }
                    //    ObsValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BMI"]);
                    //    //ObservDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                    //    BindObservationResult_OBX(false, intSlNo, RadCode, RadName, SystemCode, "", ObsValue, ObsUnit, ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                    //    // BindObservationResult_OBX(Boolean MmicrobiologyValues, Int32 intSlNo, string LabCode, string LabDesc, string strSysCode, string ObsSubID, string ObsValue, string ObsUnit, string TestDateTime, string AbnormalFlag, string Range, string ResultStatus, string OrderingProviderID, string OrderingProviderName)
                    //    intSlNo = intSlNo + 1;
                    //}


                    //BindObservationResult_OBX(false, 1, RadCode, RadName, SystemCode, "", ObsValue, "", ObservDate, "", "", ResultStatus, OrderingProviderID, OrderingProviderName);
                    //BindRadObservationResult_OBX(RadCode, RadName, ObsValue, ObsDateTime);



                    string FileName = MessageEvent + "_" + MessageControlID;
                    MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

                    string ReturnMessage = "";
                    Int32 MessageSent = 0;

                    Boolean SendToMalaffi = false;
                    string[] arrMessageTypes = UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
                    for (Int32 intSegType = 0; intSegType < arrMessageTypes.Length; intSegType++)
                    {
                        if (arrMessageTypes[intSegType] == MessageEvent) //MESSAGE_TYPE
                        {
                            SendToMalaffi = true;
                        }
                    }

                    if (SendToMalaffi == true)
                    {
                        SendHL7Message(testHl7MessageToTransmit.ToString(), MESSAGE_TYPE, out  ReturnMessage, out  MessageSent);
                    }
                    else
                    {
                        MessageSent = 3;
                    }

                    LogFileWriting("HL7File Created file name is  " + FileName);

                    // MessageID = MessageControlID;
                    //HL7FileName = FileName;
                    // MessageCode = MessageEvent; // "ORU-R01";
                    // MessageDesc = "Result ORU Rad";
                    // MESSAGE_TYPE = "RESULT";
                    RETURN_MESSAGE = ReturnMessage.Replace("'", "");
                    // MalaffiHL7MessageMasterAdd();

                    objCom = new CommonBAL();
                    string Criteria1 = " HMHM_UPLOAD_STATUS ='PENDING' ";
                    Criteria1 += " AND HMHM_BRANCH_ID='" + BRANCH_ID + "' AND HMHM_PT_ID='" + PT_ID + "'";
                   // Criteria1 += " AND HMHM_RAD_TEST_REPORT_ID='" + RAD_TEST_REPORT_ID + "'";
                    Criteria1 += " AND HMHM_MESSAGE_TYPE='" + MESSAGE_TYPE + "' and HMHM_MESSAGE_CODE='" + MessageEvent + "'";

                    string FieldNameWithValues = "HMHM_MESSAGE_ID='" + MessageControlID + "', HMHM_FILE_NAME='" + FileName + "' ";
                    if (MessageSent == 1)
                    {
                        FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'UPLOADED' ";
                        objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria1);

                    }
                    if (MessageSent == 2)
                    {
                        FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'ERROR' ";
                        objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria1);

                    }
                    if (MessageSent == 3)
                    {
                        FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'FILE_CREATED' ";
                        objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria1);

                    }

                    string FieldNameWithValues1 = "HMHM_RETURN_MESSAGE='" + RETURN_MESSAGE + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria1);

                LoopEnd: ;
                }





            }


        FunEnd: ;
            if (isError == true)
            {
                return false;
            }


            return true;

        }

        public Boolean PhyOrderMessagGenerate_OMP_O09()
        {
            // LogFileWriting("PhyOrderMessagGenerate_OMP_O09()  1");

            Boolean isError = false;
            MessageType = "OMP^O09";
            MessageEvent = "OMP-O09";
            MESSAGE_TYPE = "MEDS";

            LogFileWriting("****************** OMP-O09 " + System.DateTime.Now.ToString() + "******************");

            GetPatientDetails();
            GetPatientVisitDtls();
            BindEMRPTMaster();
            //GetPatientClass();
            // LogFileWriting("PhyOrderMessagGenerate_OMP_O09()  2");
            if (ValidateNL7Message() == false)
            {
                isError = true;
            }

            //  LogFileWriting("PhyOrderMessagGenerate_OMP_O09()  3");
            testHl7MessageToTransmit = new StringBuilder();

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  AND EPP_SEND_TO_MALAFFI IS NULL ";

            //Criteria += " AND EPP_ID = '" + EMR_ID + "'  AND EPP_GENERIC_CODE IS NOT NULL AND EPP_QTY IS NOT NULL  AND EPP_NURSE_ORDER IS NULL     ";

            Criteria += " AND EPP_ID = '" + EMR_ID + "'   AND EPP_QTY IS NOT NULL  AND ( EPP_NURSE_ORDER IS NULL OR EPP_STATUS='D' )  AND EPP_GENERIC_CODE IS NOT NULL  ";
            DS = objCom.fnGetFieldValue("*,CASE EPP_DURATION_TYPE  WHEN  0 THEN 'Days' WHEN  3 THEN 'Weeks'  WHEN  1 THEN 'Months' WHEN  2 THEN 'Years'  ELSE ''  END  AS EPP_DURATION_TYPEDesc " +
             ",format(EPP_START_DATE ,'dd') + '-' +datename(month,EPP_START_DATE) + '-' + datename(Year,EPP_START_DATE) as EPP_START_DATELongDesc ", "EMR_PT_PHARMACY", Criteria, "EPP_POSITION");

            string strDeletedCode = "";
            if (DS.Tables[0].Rows.Count > 0)
            {
                ///LogFileWriting("PhyOrderMessagGenerate_OMP_O09()  4");

                MessageHeader();
                BindPatientIdentification_PID();
                BindPatientVisitAdditionalInformation_PV1();


                // LogFileWriting("PhyOrderMessagGenerate_OMP_O09()  5");


                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    string PhyCode = Convert.ToString(DS.Tables[0].Rows[i]["EPP_PHY_CODE"]);
                    string PhyName = Convert.ToString(DS.Tables[0].Rows[i]["EPP_PHY_NAME"]);



                    PhyName = RemoveSplChar(Convert.ToString(PhyName.Trim()));
                    PhyName = PhyName.Replace("\n", string.Empty);
                    PhyName = PhyName.Replace(Environment.NewLine, string.Empty);



                    string PhyQty = Convert.ToString(DS.Tables[0].Rows[i]["EPP_QTY"]);




                    //  phyRemarks = RemoveSplChar(Convert.ToString(phyRemarks.Trim()));

                    string phyRoute = "ORAL";
                    if (DS.Tables[0].Rows[i].IsNull("EPP_UNIT") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPP_UNIT"]) != "")
                    {
                        phyRoute = Convert.ToString(DS.Tables[0].Rows[i]["EPP_ROUTE"]);
                    }







                    //  string GenericCode = Convert.ToString(DS.Tables[0].Rows[i]["EPP_GENERIC_CODE"]);
                    string Unit = "1", UnitType = "Tablet(s)";

                    if (DS.Tables[0].Rows[i].IsNull("EPP_UNIT") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPP_UNIT"]) != "")
                    {
                        Unit = Convert.ToString(DS.Tables[0].Rows[i]["EPP_UNIT"]);
                    }
                    if (DS.Tables[0].Rows[i].IsNull("EPP_UNITTYPE") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPP_UNITTYPE"]) != "")
                    {
                        UnitType = Convert.ToString(DS.Tables[0].Rows[i]["EPP_UNITTYPE"]);
                    }

                    string Frequency = "1", FrequencyType = "Per Day";

                    if (DS.Tables[0].Rows[i].IsNull("EPP_FREQUENCY") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPP_FREQUENCY"]) != "")
                    {
                        Frequency = Convert.ToString(DS.Tables[0].Rows[i]["EPP_FREQUENCY"]);
                    }
                    if (DS.Tables[0].Rows[i].IsNull("EPP_FREQUENCYTYPE") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPP_FREQUENCYTYPE"]) != "")
                    {
                        FrequencyType = Convert.ToString(DS.Tables[0].Rows[i]["EPP_FREQUENCYTYPE"]);
                    }

                    string phyDuration = "1", phyDurationDesc = "Days", phyRemarks = "";

                    if (DS.Tables[0].Rows[i].IsNull("EPP_DURATION") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPP_DURATION"]) != "")
                    {
                        phyDuration = Convert.ToString(DS.Tables[0].Rows[i]["EPP_DURATION"]);
                    }
                    if (DS.Tables[0].Rows[i].IsNull("EPP_DURATION_TYPEDesc") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPP_DURATION_TYPEDesc"]) != "")
                    {
                        phyDurationDesc = Convert.ToString(DS.Tables[0].Rows[i]["EPP_DURATION_TYPEDesc"]);
                    }

                    if (DS.Tables[0].Rows[i].IsNull("EPP_REMARKS") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPP_REMARKS"]) != "")
                    {
                        phyRemarks = Convert.ToString(DS.Tables[0].Rows[i]["EPP_REMARKS"]);
                    }

                    string OrderStatus = "active";
                    if (DS.Tables[0].Rows[i].IsNull("EPP_ORDER_STATUS") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPP_ORDER_STATUS"]) != "")
                    {
                        OrderStatus = Convert.ToString(DS.Tables[0].Rows[i]["EPP_ORDER_STATUS"]);
                    }

                    string Refills = "";

                    if (DS.Tables[0].Rows[i].IsNull("EPP_REFILL") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPP_REFILL"]) != "")
                    {
                        string strRefillData = Convert.ToString(DS.Tables[0].Rows[i]["EPP_REFILL"]);
                        int vNu;
                        bool isNumeric = int.TryParse(strRefillData, out vNu);
                        if (isNumeric == true)
                        {
                            Refills = Convert.ToString(DS.Tables[0].Rows[i]["EPP_REFILL"]);
                            if (Refills.Length > 3)
                            {
                                Refills = Refills.Substring(0, 3);
                            }

                        }

                    }

                    string PhyTaken = "";
                    if (DS.Tables[0].Rows[i].IsNull("EPP_TIME") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPP_TIME"]) != "")
                    {
                        PhyTaken = Convert.ToString(DS.Tables[0].Rows[i]["EPP_TIME"]);
                    }

                    string PhyStatus = "";
                    if (DS.Tables[0].Rows[i].IsNull("EPP_STATUS") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPP_STATUS"]) != "")
                    {
                        PhyStatus = Convert.ToString(DS.Tables[0].Rows[i]["EPP_STATUS"]);
                    }

                    if (PhyStatus == "D")
                    {
                        if (strDeletedCode != "")
                        {
                            strDeletedCode += ",'" + PhyCode + "'";
                        }
                        else
                        {
                            strDeletedCode += "'" + PhyCode + "'";
                        }
                    }

                    string phyTransDate = "", phyStart = "", phyEnd = "";

                    if (DS.Tables[0].Rows[i].IsNull("EPP_CREATED_DATE") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPP_CREATED_DATE"]) != "")
                    {
                        DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[i]["EPP_CREATED_DATE"]);

                        string strMonth = dtStartDate.Month.ToString();
                        if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                        string strDay = dtStartDate.Day.ToString();
                        if (strDay.Length == 1) { strDay = "0" + strDay; }

                        string strHour = dtStartDate.Hour.ToString();
                        if (strHour.Length == 1) { strHour = "0" + strHour; }

                        string strMinute = dtStartDate.Minute.ToString();
                        if (strMinute.Length == 1) { strMinute = "0" + strMinute; }


                        phyTransDate = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                    }
                    else
                    {
                        phyTransDate = EMR_START_DATE_TIME;
                    }

                    if (DS.Tables[0].Rows[0].IsNull("EPP_START_DATE") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPP_START_DATE"]) != "")
                    {
                        DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[i]["EPP_START_DATE"]);

                        string strMonth = dtStartDate.Month.ToString();
                        if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                        string strDay = dtStartDate.Day.ToString();
                        if (strDay.Length == 1) { strDay = "0" + strDay; }

                        string strHour = dtStartDate.Hour.ToString();
                        if (strHour.Length == 1) { strHour = "0" + strHour; }

                        string strMinute = dtStartDate.Minute.ToString();
                        if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                        phyStart = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                    }


                    if (DS.Tables[0].Rows[i].IsNull("EPP_END_DATE") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPP_END_DATE"]) != "")
                    {
                        DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[i]["EPP_END_DATE"]);

                        string strMonth = dtStartDate.Month.ToString();
                        if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                        string strDay = dtStartDate.Day.ToString();
                        if (strDay.Length == 1) { strDay = "0" + strDay; }

                        string strHour = dtStartDate.Hour.ToString();
                        if (strHour.Length == 1) { strHour = "0" + strHour; }

                        string strMinute = dtStartDate.Minute.ToString();
                        if (strMinute.Length == 1) { strMinute = "0" + strMinute; }


                        phyEnd = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                    }





                    string phyAmount = "0", Strength = "", Dosage_Form = "", GenericCode = "", GenericName = "", PackageName = "";

                    if (PHARMACY_DB_NAME != "")
                    {
                        GetInventoryPhyDrugList(PhyCode, out   phyAmount, out Strength, out Dosage_Form, out GenericCode, out GenericName, out PackageName);

                        Strength = RemoveSplChar(Convert.ToString(Strength.Trim()));



                    }
                    else
                    {
                        GetPhyAmount(PhyCode, out   phyAmount, out Strength, out Dosage_Form, out GenericCode, out GenericName, out PackageName);
                        Strength = RemoveSplChar(Convert.ToString(Strength.Trim()));

                    }

                    // LogFileWriting("PhyOrderMessagGenerate_OMP_O09()  6 PhyCode " + PhyCode);

                    if (PhyCode == "" || PhyName == "" || PhyQty == "" || phyRoute == "" || phyDuration == "" || GenericCode == "" || Unit == "" || UnitType == "" || Frequency == "" || FrequencyType == "" || phyStart == "" || phyEnd == "")
                    {
                        LogFileWriting(" PhyOrderMessagGenerate function Some details missing   EMR_ID = " + EMR_ID +
                            " PhyCode = " + PhyCode + "| PhyName = " + PhyName + "| PhyQty = " + PhyQty + "| phyRoute =" + phyRoute + "| phyDuration = " + phyDuration + "| GenericCode = " + GenericCode +
                            "|Unit = " + Unit + "| UnitType = " + UnitType + "|Frequency = " + Frequency + "|FrequencyType = " + FrequencyType + "| phyStart = " + phyStart + "| phyEnd = " + phyEnd);

                        goto FunEnd;
                    }


                    Int32 SLNo;

                    if (DS.Tables[0].Rows[i].IsNull("EPP_POSITION") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPP_POSITION"]) != "")
                    {
                        SLNo = Convert.ToInt32(DS.Tables[0].Rows[i]["EPP_POSITION"]);

                    }
                    else
                    {
                        SLNo = i + 1;
                    }



                    ////if (Unit != "" && UnitType != "" && Frequency != "" && FrequencyType != "" && phyDuration != "" && phyDurationDesc != "")
                    ////{
                    ////    phyRemarks = Unit + " " + UnitType + ", " + Frequency + " " + FrequencyType + ", " + phyDuration + " " + phyDurationDesc + ", From: " + Convert.ToString(DS.Tables[0].Rows[0]["EPP_START_DATELongDesc"]);
                    ////}




                    string OrderQtyTiming = "";

                    // Qunatity^Interval^Duration^Start Date/Time^End Date/Time^Prioity   //
                    //  OrderQtyTiming = PhyQty + "^" + Frequency + " " + FrequencyType + "^" + phyDuration + " " + phyDurationDesc + "^" + phyStart + "^" + phyEnd;//  +"^";
                    OrderQtyTiming = Unit + "^" + Frequency + " " + FrequencyType + "^" + phyDuration + " " + phyDurationDesc + "^" + phyStart + "^" + phyEnd;//  +"^";
                    // LogFileWriting("PhyOrderMessagGenerate_OMP_O09()  7 ");

                    string ActionCode = "NW";
                    if (PhyStatus == "M")
                    {
                        ActionCode = "RU";
                    }
                    else if (PhyStatus == "D")
                    {
                        ActionCode = "DE";
                    }
                    else
                    {
                        ActionCode = "NW";
                    }

                    /*   The expected format is “Generic Name, Strength, Dosage Form”
                         For example:
                         “00097^Acetylcysteine600 mgEffervescent Tablets^DOHDRUG”
                         “04793^Pseudoephedrine Hydrochloride,Triprolidine Hydrochloride30 mg/5ml,1.25 mg/5mlSyrup^DOHDRUG” etc.
                      */


                    BindOrderControl_ORC(ActionCode, SLNo, EMR_ID, ConsultingDr, ConsultingDrName, phyTransDate, OrderQtyTiming, OrderStatus, phyStart); //"active"

                    //  LogFileWriting("PhyOrderMessagGenerate_OMP_O09()  8 ");
                    BindPharmacyTreatmentOrder_RXO(GenericCode, GenericName, PhyQty, phyRemarks, Unit, UnitType, Strength, Dosage_Form, Refills);
                    //  LogFileWriting("PhyOrderMessagGenerate_OMP_O09()  9 ");

                    string MalaffiCode, MalaffiName;
                    GetRouteCode(phyRoute, out  MalaffiCode, out  MalaffiName);


                    if (MalaffiCode == "")
                    {
                        BindPharmacyTreatmentRoute_RXR("NA^Not Available");
                    }
                    else
                    {
                        BindPharmacyTreatmentRoute_RXR(MalaffiCode + "^" + MalaffiName);
                    }


                }



            }
            else
            {
                goto FunEnd;
            }


            //  LogFileWriting("PhyOrderMessagGenerate_OMP_O09()  10 ");
            string strDate = "", strTime = "";
            strDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            string[] arrDate;
            arrDate = strDate.Split(' ');
            string strDate1 = arrDate[0];
            strTime = arrDate[1];

            string[] arrDate1 = strDate1.Split('/');
            string strTimeStamp = "";
            strTimeStamp = arrDate1[2] + arrDate1[1] + arrDate1[0] + strTime.Replace(":", "");



            string FileName = MessageEvent + "_" + MessageControlID;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            // LogFileWriting("PhyOrderMessagGenerate_OMP_O09() 11 ");

            string ReturnMessage = "";
            Int32 MessageSent = 0;

            Boolean SendToMalaffi = false;
            string[] arrMessageTypes = UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrMessageTypes.Length; intSegType++)
            {
                if (arrMessageTypes[intSegType] == MessageEvent) //MESSAGE_TYPE
                {
                    SendToMalaffi = true;
                }
            }

            if (SendToMalaffi == true)
            {
                SendHL7Message(testHl7MessageToTransmit.ToString(), MESSAGE_TYPE, out  ReturnMessage, out  MessageSent);
            }
            else
            {
                MessageSent = 3;
            }



            LogFileWriting("HL7File Created file name is  " + FileName);


            RETURN_MESSAGE = ReturnMessage.Replace("'", "");


            objCom = new CommonBAL();
            Criteria = " HMHM_UPLOAD_STATUS ='PENDING' ";
            Criteria += " AND HMHM_BRANCH_ID='" + BRANCH_ID + "' AND HMHM_PT_ID='" + PT_ID + "' AND HMHM_EMR_ID='" + EMR_ID + "'";
            Criteria += " AND HMHM_MESSAGE_TYPE='" + MESSAGE_TYPE + "' and HMHM_MESSAGE_CODE='" + MessageEvent + "'";

            string FieldNameWithValues = "HMHM_MESSAGE_ID='" + MessageControlID + "', HMHM_FILE_NAME='" + FileName + "' ";


            if (MessageSent == 1)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'UPLOADED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

                if (strDeletedCode != "")
                {
                    string PhyCriteria = " EPP_ID = '" + EMR_ID + "' AND EPP_PHY_CODE IN (" + strDeletedCode + ")";
                    string PhyFieldNameWithValues = "EPP_SEND_TO_MALAFFI=1";
                    objCom.fnUpdateTableData(PhyFieldNameWithValues, "EMR_PT_PHARMACY", PhyCriteria);
                }

            }

            if (MessageSent == 2)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'ERROR' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            if (MessageSent == 3)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'FILE_CREATED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

                if (strDeletedCode != "")
                {
                    string PhyCriteria = " EPP_ID = '" + EMR_ID + "' AND EPP_PHY_CODE IN (" + strDeletedCode + ")";
                    string PhyFieldNameWithValues = "EPP_SEND_TO_MALAFFI=1";
                    objCom.fnUpdateTableData(PhyFieldNameWithValues, "EMR_PT_PHARMACY", PhyCriteria);
                }

            }

            string FieldNameWithValues1 = "HMHM_RETURN_MESSAGE='" + RETURN_MESSAGE + "'";
            objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

        FunEnd: ;
            if (isError == true)
            {
                return false;
            }

            return true;

        }


        public Boolean PhyAdministrationMessagGenerate_RAS_O17()
        {
            Boolean isError = false;
            MessageType = "RAS^O17";
            MessageEvent = "RAS-O17";
            MESSAGE_TYPE = "MEDS";

            LogFileWriting("****************** RAS-O17 " + System.DateTime.Now.ToString() + "******************");

            GetPatientDetails();
            GetPatientVisitDtls();
            BindEMRPTMaster();
            // GetPatientClass();

            if (ValidateNL7Message() == false)
            {
                isError = true;
            }


            testHl7MessageToTransmit = new StringBuilder();

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND EPC_ID = '" + EMR_ID + "' AND EPC_SERV_NAME IS NOT NULL   AND EPC_QTY IS NOT NULL  AND EPC_SERV_CODE IS NOT NULL AND EPC_STATUS IS NOT NULL AND EPC_STATUS IN ('Completed','Partially Administered') AND  EPC_SERV_TYPE ='DRG' "; //AND EPP_GENERIC_CODE IS NOT NULL

            DS = objCom.fnGetFieldValue("*,CASE EPC_DURATION_TYPE  WHEN  0 THEN 'Days' WHEN  3 THEN 'Weeks'  WHEN  1 THEN 'Months' WHEN  2 THEN 'Years'  ELSE ''  END  AS EPC_DURATION_TYPEDesc," +
            " format(EPC_TIME ,'dd') + '-' +datename(month,EPC_TIME) + '-' + datename(Year,EPC_TIME) as EPC_TIMELongDesc", "EMR_PT_INSTRUCTION", Criteria, "EPC_SERV_NAME");

            if (DS.Tables[0].Rows.Count > 0)
            {


                MessageHeader();
                BindPatientIdentification_PID();
                BindPatientVisitAdditionalInformation_PV1();



                Int32 intSlNo = 1;

                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {



                    string PhyCode = Convert.ToString(DS.Tables[0].Rows[i]["EPC_SERV_CODE"]).Trim();
                    string PhyName = Convert.ToString(DS.Tables[0].Rows[i]["EPC_SERV_NAME"]).Trim();
                    PhyName = RemoveSplChar(Convert.ToString(PhyName.Trim()));

                    string PhyQty = Convert.ToString(DS.Tables[0].Rows[i]["EPC_QTY"]).Trim();



                    string phyRemarks = Convert.ToString(DS.Tables[0].Rows[i]["EPC_COMMENT"]).Trim();
                    phyRemarks = RemoveSplChar(Convert.ToString(phyRemarks.Trim()));
                    phyRemarks = phyRemarks.Replace("\n", string.Empty);
                    phyRemarks = phyRemarks.Replace(Environment.NewLine, string.Empty);

                    string phyRoute = "ORAL";

                    if (DS.Tables[0].Rows[i].IsNull("EPC_ROUTE") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPC_ROUTE"]) != "")
                    {
                        phyRoute = Convert.ToString(DS.Tables[0].Rows[i]["EPC_ROUTE"]).Trim();
                    }

                    //  string GenericCode = Convert.ToString(DS.Tables[0].Rows[i]["EPP_GENERIC_CODE"]);
                    string Unit = "1", UnitType = "Tablet(s)";

                    if (DS.Tables[0].Rows[i].IsNull("EPC_UNIT") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPC_UNIT"]) != "")
                    {
                        Unit = Convert.ToString(DS.Tables[0].Rows[i]["EPC_UNIT"]).Trim();
                    }
                    if (DS.Tables[0].Rows[i].IsNull("EPC_UNITTYPE") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPC_UNITTYPE"]) != "")
                    {
                        UnitType = Convert.ToString(DS.Tables[0].Rows[i]["EPC_UNITTYPE"]).Trim();
                    }

                    string Frequency = "1", FrequencyType = "Per Day";

                    if (DS.Tables[0].Rows[i].IsNull("EPC_FREQUENCY") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPC_FREQUENCY"]) != "")
                    {
                        Frequency = Convert.ToString(DS.Tables[0].Rows[i]["EPC_FREQUENCY"]).Trim();
                    }
                    if (DS.Tables[0].Rows[i].IsNull("EPC_FREQUENCYTYPE") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPC_FREQUENCYTYPE"]) != "")
                    {
                        FrequencyType = Convert.ToString(DS.Tables[0].Rows[i]["EPC_FREQUENCYTYPE"]).Trim();
                    }

                    string phyDuration = "1", phyDurationDesc = "Days";
                    if (DS.Tables[0].Rows[i].IsNull("EPC_DURATION") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPC_DURATION"]) != "")
                    {
                        phyDuration = Convert.ToString(DS.Tables[0].Rows[i]["EPC_DURATION"]).Trim();
                    }
                    if (DS.Tables[0].Rows[i].IsNull("EPC_DURATION_TYPEDesc") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPC_DURATION_TYPEDesc"]) != "")
                    {
                        phyDurationDesc = Convert.ToString(DS.Tables[0].Rows[i]["EPC_DURATION_TYPEDesc"]);
                    }


                    string ServiceType = Convert.ToString(DS.Tables[0].Rows[i]["EPC_SERV_TYPE"]).Trim();

                    string Status = Convert.ToString(DS.Tables[0].Rows[i]["EPC_STATUS"]).Trim();
                    string OrderStatus = "active";

                    if (DS.Tables[0].Rows[i].IsNull("EPC_TIME") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPC_TIME"]) != "")
                    {
                        OrderStatus = Convert.ToString(DS.Tables[0].Rows[i]["EPC_ORDER_STATUS"]).Trim();
                    }
                    int AdminSub_ID = 1;

                    if (DS.Tables[0].Rows[i].IsNull("EPC_SUB_ID") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPC_SUB_ID"]) != "")
                    {
                        AdminSub_ID = Convert.ToInt32(DS.Tables[0].Rows[i]["EPC_SUB_ID"]);
                    }
                    string CompletedStatus = "CP";
                    switch (Status)
                    {
                        case "Not Administered":
                            CompletedStatus = "NA";
                            break;
                        case "Partially Administered":
                            CompletedStatus = "PA";
                            break;
                        case "Refused":
                            CompletedStatus = "RE";
                            break;
                        case "Completed":
                            CompletedStatus = "CP";
                            break;
                    }


                    string AdministeringProviderID, AdministeringProviderName;
                    string DR_ID = Convert.ToString(DS.Tables[0].Rows[i]["EPC_USERNAME"]).Trim();

                    string DrFirstName, DrMiddleName, DrLastName, LicenseNo, DrSpecialityName;
                    GetStaffDetails(DR_ID, out   DrFirstName, out DrMiddleName, out   DrLastName, out   LicenseNo, out DrSpecialityName);


                    //  LogFileWriting("LabMessageGenerate_ORU_R01 3");
                    if (LicenseNo != "")
                    {
                        AdministeringProviderID = LicenseNo;
                    }
                    else
                    {
                        AdministeringProviderID = DR_ID;
                    }

                    if (DrFirstName.Length > 20)
                    {
                        DrFirstName = DrFirstName.Substring(0, 15);
                    }
                    if (DrLastName.Length > 20)
                    {
                        DrLastName = DrLastName.Substring(0, 15);
                    }




                    //if (DrLastName != "")
                    // {
                    AdministeringProviderName = DrLastName + "^" + DrFirstName + "^" + DrMiddleName; //DrFirstName + "^" + DrLastName;


                    string phyTransDate, phyStart = "", phyEnd = "";

                    if (DS.Tables[0].Rows[i].IsNull("EPC_ORDER_DATE") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPC_ORDER_DATE"]) != "")
                    {
                        DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[i]["EPC_ORDER_DATE"]);

                        string strMonth = dtStartDate.Month.ToString();
                        if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                        string strDay = dtStartDate.Day.ToString();
                        if (strDay.Length == 1) { strDay = "0" + strDay; }

                        string strHour = dtStartDate.Hour.ToString();
                        if (strHour.Length == 1) { strHour = "0" + strHour; }

                        string strMinute = dtStartDate.Minute.ToString();
                        if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                        phyTransDate = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                    }
                    else
                    {
                        phyTransDate = EMR_START_DATE_TIME;
                    }

                    if (DS.Tables[0].Rows[i].IsNull("EPC_TIME") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPC_TIME"]) != "")
                    {
                        DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[i]["EPC_TIME"]);

                        string strMonth = dtStartDate.Month.ToString();
                        if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                        string strDay = dtStartDate.Day.ToString();
                        if (strDay.Length == 1) { strDay = "0" + strDay; }

                        string strHour = dtStartDate.Hour.ToString();
                        if (strHour.Length == 1) { strHour = "0" + strHour; }

                        string strMinute = dtStartDate.Minute.ToString();
                        if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                        phyStart = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                    }


                    if (DS.Tables[0].Rows[i].IsNull("EPC_END_TIME") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPC_END_TIME"]) != "")
                    {
                        DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[i]["EPC_END_TIME"]);

                        string strMonth = dtStartDate.Month.ToString();
                        if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                        string strDay = dtStartDate.Day.ToString();
                        if (strDay.Length == 1) { strDay = "0" + strDay; }

                        string strHour = dtStartDate.Hour.ToString();
                        if (strHour.Length == 1) { strHour = "0" + strHour; }

                        string strMinute = dtStartDate.Minute.ToString();
                        if (strMinute.Length == 1) { strMinute = "0" + strMinute; }


                        phyEnd = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                    }

                    string phyAmount = "0", Strength = "", Dosage_Form = "", GenericCode = "", GenericName = "", PackageName = "";

                    if (PHARMACY_DB_NAME != "")
                    {
                        GetInventoryPhyDrugList(PhyCode, out   phyAmount, out Strength, out Dosage_Form, out GenericCode, out GenericName, out PackageName);

                        Strength = RemoveSplChar(Convert.ToString(Strength.Trim()));
                        PackageName = RemoveSplChar(Convert.ToString(PackageName.Trim()));
                    }
                    else
                    {
                        GetPhyAmount(PhyCode, out   phyAmount, out Strength, out Dosage_Form, out GenericCode, out GenericName, out PackageName);

                        Strength = RemoveSplChar(Convert.ToString(Strength.Trim()));
                        PackageName = RemoveSplChar(Convert.ToString(PackageName.Trim()));
                    }

                    //if (ServiceType != "DRG")
                    //{
                    //    GenericCode = PhyCode;
                    //    PackageName = PhyName;
                    //}

                    Int32 SLNo;

                    if (DS.Tables[0].Rows[i].IsNull("EPC_INSTRUCTION_ID") == false && Convert.ToString(DS.Tables[0].Rows[i]["EPC_INSTRUCTION_ID"]) != "")
                    {
                        SLNo = Convert.ToInt32(DS.Tables[0].Rows[i]["EPC_INSTRUCTION_ID"]);

                    }
                    else
                    {
                        SLNo = i + 1;
                    }


                    if (Unit != "" && UnitType != "" && Frequency != "" && FrequencyType != "" && phyDuration != "" && phyDurationDesc != "")
                    {
                        phyRemarks = Unit + " " + UnitType + ", " + Frequency + " " + FrequencyType + ", " + phyDuration + " " + phyDurationDesc + ", From: " + Convert.ToString(DS.Tables[0].Rows[i]["EPC_TIMELongDesc"]);
                    }

                    if (PhyCode == "" || PackageName == "" || Strength == "" || Dosage_Form == "" || PhyQty == "" || phyRoute == "" || phyDuration == "" || Unit == "" || UnitType == "" || Frequency == "" || FrequencyType == "" || phyStart == "" || phyEnd == "")
                    {

                        LogFileWriting(" PhyAdministrationMessagGenerate function Some details missing   EMR_ID = " + EMR_ID +
                           " PhyCode = " + PhyCode + "| PackageName = " + PackageName + "| PhyQty = " + PhyQty + "| phyStart = " + phyStart + "| phyEnd = " + phyEnd);

                        goto FunEnd;
                    }

                    //  }


                    //if (phyRemarks.Length > 20)
                    //{
                    //  phyRemarks =   phyRemarks.Substring(0, 20);
                    //}



                    /*  The expected format is “Package Name, Strength, Dosage Form”
                      For example:
                      “K34-0869-01106-02^CELEBREX 200mg Capsule^DOHDRUG”

                     FOR RAS should send Drug Code and Package Name not Generic Code
                  */

                    // Qunatity^Interval^Duration^Start Date/Time^End Date/Time^Prioity
                    // OrderQtyTiming = PhyQty + "^" + Frequency + "^" + phyDuration + "^" + phyStart + "^" + phyEnd + "^";


                    string OrderQtyTiming = "";

                    // Qunatity^Interval^Duration^Start Date/Time^End Date/Time^Prioity  
                    //  OrderQtyTiming = PhyQty + "^" + Frequency + " " + FrequencyType + "^" + phyDuration + " " + phyDurationDesc + "^" + phyStart + "^" + phyEnd;//  +"^";
                    OrderQtyTiming = Unit + "^" + Frequency + " " + FrequencyType + "^" + phyDuration + " " + phyDurationDesc + "^" + phyStart + "^" + phyEnd;//  +"^";


                    BindOrderControl_ORC("NW", SLNo, EMR_ID, ConsultingDr, ConsultingDrName, phyTransDate, OrderQtyTiming, OrderStatus, phyStart);

                    BindPharmacyTreatmentAdmi_RXA(AdminSub_ID, phyStart, phyEnd, PhyCode, PackageName, PhyQty, phyRemarks, Unit, UnitType, Strength, Dosage_Form, AdministeringProviderID, AdministeringProviderName, CompletedStatus);


                    string MalaffiCode, MalaffiName;
                    GetRouteCode(phyRoute, out  MalaffiCode, out  MalaffiName);


                    if (MalaffiCode == "")
                    {
                        BindPharmacyTreatmentRoute_RXR("NA^Not Available");
                    }
                    else
                    {
                        BindPharmacyTreatmentRoute_RXR(MalaffiCode + "^" + MalaffiName);
                    }

                    intSlNo = intSlNo + 1;
                }



            }
            else
            {
                goto FunEnd;
            }



            string strDate = "", strTime = "";
            strDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            string[] arrDate;
            arrDate = strDate.Split(' ');
            string strDate1 = arrDate[0];
            strTime = arrDate[1];

            string[] arrDate1 = strDate1.Split('/');
            string strTimeStamp = "";
            strTimeStamp = arrDate1[2] + arrDate1[1] + arrDate1[0] + strTime.Replace(":", "");



            string FileName = MessageEvent + "_" + MessageControlID;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);

            string ReturnMessage = "";
            Int32 MessageSent = 0;

            Boolean SendToMalaffi = false;
            string[] arrMessageTypes = UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrMessageTypes.Length; intSegType++)
            {
                if (arrMessageTypes[intSegType] == MessageEvent) //MESSAGE_TYPE
                {
                    SendToMalaffi = true;
                }
            }

            if (SendToMalaffi == true)
            {
                SendHL7Message(testHl7MessageToTransmit.ToString(), MESSAGE_TYPE, out  ReturnMessage, out  MessageSent);
            }
            else
            {
                MessageSent = 3;
            }

            LogFileWriting("HL7File Created file name is  " + FileName);


            RETURN_MESSAGE = ReturnMessage.Replace("'", "");


            objCom = new CommonBAL();
            Criteria = " HMHM_UPLOAD_STATUS ='PENDING' ";
            Criteria += " AND HMHM_BRANCH_ID='" + BRANCH_ID + "' AND HMHM_PT_ID='" + PT_ID + "' AND HMHM_EMR_ID='" + EMR_ID + "'";
            Criteria += " AND HMHM_MESSAGE_TYPE='" + MESSAGE_TYPE + "' and HMHM_MESSAGE_CODE='" + MessageEvent + "'";

            string FieldNameWithValues = "HMHM_MESSAGE_ID='" + MessageControlID + "', HMHM_FILE_NAME='" + FileName + "' ";


            if (MessageSent == 1)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'UPLOADED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            if (MessageSent == 2)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'ERROR' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            if (MessageSent == 3)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'FILE_CREATED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            string FieldNameWithValues1 = "HMHM_RETURN_MESSAGE='" + RETURN_MESSAGE + "'";
            objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);


        FunEnd: ;
            if (isError == true)
            {
                return false;
            }

            return true;

        }


        void GetPhyAmount(string PhyCode, out string phyAmount, out string Strength, out string Dosage_Form, out string GenericCode, out string GenericName, out string PackageName)
        {
            phyAmount = "0";
            Strength = "";
            Dosage_Form = "";
            GenericCode = "";
            GenericName = "";
            PackageName = "";

            CommonBAL objCom = new CommonBAL();
            DataSet DSPhy = new DataSet();
            string Criteria = " 1=1 AND HDL_STATUS='A' ";
            Criteria += " AND HDL_DRUG_CODE='" + PhyCode.Trim().Replace("'", "''") + "'";
            DSPhy = objCom.fnGetFieldValue("  TOP 1 HDL_PACKAGE_PRICE,HDL_STRENGTH,HDL_DOSAGE_FORM,HDL_GENERIC_CODE,HDL_GENERIC_NAME,HDL_PACKAGE_NAME ", "HMS_DRUG_LIST", Criteria, "HDL_DRUG_CODE desc");

            if (DSPhy.Tables[0].Rows.Count > 0)
            {
                if (DSPhy.Tables[0].Rows[0].IsNull("HDL_PACKAGE_PRICE") == false && Convert.ToString(DSPhy.Tables[0].Rows[0]["HDL_PACKAGE_PRICE"]) != "")
                {
                    phyAmount = Convert.ToString(DSPhy.Tables[0].Rows[0]["HDL_PACKAGE_PRICE"]);
                }

                if (DSPhy.Tables[0].Rows[0].IsNull("HDL_STRENGTH") == false && Convert.ToString(DSPhy.Tables[0].Rows[0]["HDL_STRENGTH"]) != "")
                {
                    Strength = Convert.ToString(DSPhy.Tables[0].Rows[0]["HDL_STRENGTH"]);

                    Strength = RemoveSplChar(Convert.ToString(Strength.Trim()));
                    Strength = Strength.Replace("\n", string.Empty);
                    Strength = Strength.Replace(Environment.NewLine, string.Empty);
                }

                if (DSPhy.Tables[0].Rows[0].IsNull("HDL_DOSAGE_FORM") == false && Convert.ToString(DSPhy.Tables[0].Rows[0]["HDL_DOSAGE_FORM"]) != "")
                {
                    Dosage_Form = Convert.ToString(DSPhy.Tables[0].Rows[0]["HDL_DOSAGE_FORM"]);

                    Dosage_Form = RemoveSplChar(Convert.ToString(Dosage_Form.Trim()));
                    Dosage_Form = Dosage_Form.Replace("\n", string.Empty);
                    Dosage_Form = Dosage_Form.Replace(Environment.NewLine, string.Empty);
                }

                if (DSPhy.Tables[0].Rows[0].IsNull("HDL_GENERIC_CODE") == false && Convert.ToString(DSPhy.Tables[0].Rows[0]["HDL_GENERIC_CODE"]) != "")
                {
                    GenericCode = Convert.ToString(DSPhy.Tables[0].Rows[0]["HDL_GENERIC_CODE"]);
                }

                if (DSPhy.Tables[0].Rows[0].IsNull("HDL_GENERIC_NAME") == false && Convert.ToString(DSPhy.Tables[0].Rows[0]["HDL_GENERIC_NAME"]) != "")
                {
                    GenericName = Convert.ToString(DSPhy.Tables[0].Rows[0]["HDL_GENERIC_NAME"]);

                    GenericName = RemoveSplChar(Convert.ToString(GenericName.Trim()));
                    GenericName = GenericName.Replace("\n", string.Empty);
                    GenericName = GenericName.Replace(Environment.NewLine, string.Empty);
                }

                if (DSPhy.Tables[0].Rows[0].IsNull("HDL_PACKAGE_NAME") == false && Convert.ToString(DSPhy.Tables[0].Rows[0]["HDL_PACKAGE_NAME"]) != "")
                {
                    PackageName = Convert.ToString(DSPhy.Tables[0].Rows[0]["HDL_PACKAGE_NAME"]);

                    PackageName = RemoveSplChar(Convert.ToString(PackageName.Trim()));
                    PackageName = PackageName.Replace("\n", string.Empty);
                    PackageName = PackageName.Replace(Environment.NewLine, string.Empty);
                }

            }
        }

        void GetInventoryPhyDrugList(string PhyCode, out string phyAmount, out string Strength, out string Dosage_Form, out string GenericCode, out string GenericName, out string PackageName)
        {
            phyAmount = "0";
            Strength = "";
            Dosage_Form = "";
            GenericCode = "";
            GenericName = "";
            PackageName = "";

            CommonBAL objCom = new CommonBAL();
            DataSet DSPhy = new DataSet();
            string Criteria = " 1=1 AND STATUS='A' "; // AND item_code IS NOT NULL AND item_code <> '' 
            Criteria += " AND DRUG_CODE='" + PhyCode.Trim().Replace("'", "''") + "'";
            DSPhy = objCom.fnGetFieldValue("  TOP 1 Package_Price,Strength,Dosage_Form,GenericCode,GenericName,Package_Name ", PHARMACY_DB_NAME + ".DBO.DRUGLIST", Criteria, "DRUG_CODE desc");

            if (DSPhy.Tables[0].Rows.Count > 0)
            {
                if (DSPhy.Tables[0].Rows[0].IsNull("Package_Price") == false && Convert.ToString(DSPhy.Tables[0].Rows[0]["Package_Price"]) != "")
                {
                    phyAmount = Convert.ToString(DSPhy.Tables[0].Rows[0]["Package_Price"]);
                }

                if (DSPhy.Tables[0].Rows[0].IsNull("Strength") == false && Convert.ToString(DSPhy.Tables[0].Rows[0]["Strength"]) != "")
                {
                    Strength = Convert.ToString(DSPhy.Tables[0].Rows[0]["Strength"]);

                    Strength = RemoveSplChar(Convert.ToString(Strength.Trim()));
                    Strength = Strength.Replace("\n", string.Empty);
                    Strength = Strength.Replace(Environment.NewLine, string.Empty);
                }

                if (DSPhy.Tables[0].Rows[0].IsNull("Dosage_Form") == false && Convert.ToString(DSPhy.Tables[0].Rows[0]["Dosage_Form"]) != "")
                {
                    Dosage_Form = Convert.ToString(DSPhy.Tables[0].Rows[0]["Dosage_Form"]);

                    Dosage_Form = RemoveSplChar(Convert.ToString(Dosage_Form.Trim()));
                    Dosage_Form = Dosage_Form.Replace("\n", string.Empty);
                    Dosage_Form = Dosage_Form.Replace(Environment.NewLine, string.Empty);

                }

                if (DSPhy.Tables[0].Rows[0].IsNull("GenericCode") == false && Convert.ToString(DSPhy.Tables[0].Rows[0]["GenericCode"]) != "")
                {
                    GenericCode = Convert.ToString(DSPhy.Tables[0].Rows[0]["GenericCode"]);
                }

                if (DSPhy.Tables[0].Rows[0].IsNull("GenericName") == false && Convert.ToString(DSPhy.Tables[0].Rows[0]["GenericName"]) != "")
                {
                    GenericName = Convert.ToString(DSPhy.Tables[0].Rows[0]["GenericName"]);

                    GenericName = RemoveSplChar(Convert.ToString(GenericName.Trim()));
                    GenericName = GenericName.Replace("\n", string.Empty);
                    GenericName = GenericName.Replace(Environment.NewLine, string.Empty);

                }

                if (DSPhy.Tables[0].Rows[0].IsNull("Package_Name") == false && Convert.ToString(DSPhy.Tables[0].Rows[0]["Package_Name"]) != "")
                {
                    PackageName = Convert.ToString(DSPhy.Tables[0].Rows[0]["Package_Name"]);

                    PackageName = RemoveSplChar(Convert.ToString(PackageName.Trim()));
                    PackageName = PackageName.Replace("\n", string.Empty);
                    PackageName = PackageName.Replace(Environment.NewLine, string.Empty);
                }


            }
        }

        void GetEMRPhyDtls(string PhyCode, out string OrderQtyTiming, out string Unit, out string UnitType, out string phyRemarks, out string Route, out string phyPrescDate)
        {
            OrderQtyTiming = "";

            phyRemarks = "";

            Unit = "1";
            UnitType = "Tablet(s)";
            Route = "ORAL";
            phyPrescDate = "";

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND EPP_ID = '" + EMR_ID + "' AND EPP_PHY_CODE='" + PhyCode + "' AND EPP_QTY IS NOT NULL  AND EPP_NURSE_ORDER IS NULL  ";

            DS = objCom.fnGetFieldValue("  TOP 1 *,CASE EPP_DURATION_TYPE  WHEN  0 THEN 'Days' WHEN  3 THEN 'Weeks'  WHEN  1 THEN 'Months' WHEN  2 THEN 'Years'  ELSE ''  END  AS EPP_DURATION_TYPEDesc ," +
            "format(EPP_START_DATE ,'dd') + '-' +datename(month,EPP_START_DATE) + '-' + datename(Year,EPP_START_DATE) as EPP_START_DATELongDesc ", "EMR_PT_PHARMACY", Criteria, "EPP_PHY_CODE desc");

            if (DS.Tables[0].Rows.Count > 0)
            {

                string PhyQty = Convert.ToString(DS.Tables[0].Rows[0]["EPP_QTY"]);


                Route = Convert.ToString(DS.Tables[0].Rows[0]["EPP_ROUTE"]);

                ////string GenericCode = Convert.ToString(DS.Tables[0].Rows[0]["EPP_GENERIC_CODE"]);




                if (DS.Tables[0].Rows[0].IsNull("EPP_UNIT") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPP_UNIT"]) != "")
                {
                    Unit = Convert.ToString(DS.Tables[0].Rows[0]["EPP_UNIT"]);
                }
                if (DS.Tables[0].Rows[0].IsNull("EPP_UNITTYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPP_UNITTYPE"]) != "")
                {
                    UnitType = Convert.ToString(DS.Tables[0].Rows[0]["EPP_UNITTYPE"]);
                }


                string Frequency = "1";
                string FrequencyType = "Per Day";

                if (DS.Tables[0].Rows[0].IsNull("EPP_FREQUENCY") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPP_FREQUENCY"]) != "")
                {
                    Frequency = Convert.ToString(DS.Tables[0].Rows[0]["EPP_FREQUENCY"]);
                }
                if (DS.Tables[0].Rows[0].IsNull("EPP_FREQUENCYTYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPP_FREQUENCYTYPE"]) != "")
                {
                    FrequencyType = Convert.ToString(DS.Tables[0].Rows[0]["EPP_FREQUENCYTYPE"]);
                }

                string phyDuration = "1";
                string phyDurationDesc = "Days";

                if (DS.Tables[0].Rows[0].IsNull("EPP_DURATION") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPP_DURATION"]) != "")
                {
                    phyDuration = Convert.ToString(DS.Tables[0].Rows[0]["EPP_DURATION"]);
                }
                if (DS.Tables[0].Rows[0].IsNull("EPP_DURATION_TYPEDesc") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPP_DURATION_TYPEDesc"]) != "")
                {
                    phyDurationDesc = Convert.ToString(DS.Tables[0].Rows[0]["EPP_DURATION_TYPEDesc"]);
                }



                string phyStart = "", phyEnd = "";

                if (DS.Tables[0].Rows[0].IsNull("EPP_CREATED_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPP_CREATED_DATE"]) != "")
                {
                    DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPP_CREATED_DATE"]);

                    string strMonth = dtStartDate.Month.ToString();
                    if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                    string strDay = dtStartDate.Day.ToString();
                    if (strDay.Length == 1) { strDay = "0" + strDay; }

                    string strHour = dtStartDate.Hour.ToString();
                    if (strHour.Length == 1) { strHour = "0" + strHour; }

                    string strMinute = dtStartDate.Minute.ToString();
                    if (strMinute.Length == 1) { strMinute = "0" + strMinute; }


                    phyPrescDate = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                }
                else
                {
                    phyPrescDate = EMR_START_DATE_TIME;
                }


                if (DS.Tables[0].Rows[0].IsNull("EPP_START_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPP_START_DATE"]) != "")
                {
                    DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPP_START_DATE"]);

                    string strMonth = dtStartDate.Month.ToString();
                    if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                    string strDay = dtStartDate.Day.ToString();
                    if (strDay.Length == 1) { strDay = "0" + strDay; }

                    string strHour = dtStartDate.Hour.ToString();
                    if (strHour.Length == 1) { strHour = "0" + strHour; }

                    string strMinute = dtStartDate.Minute.ToString();
                    if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                    phyStart = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                }


                if (DS.Tables[0].Rows[0].IsNull("EPP_END_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPP_END_DATE"]) != "")
                {
                    DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPP_END_DATE"]);

                    string strMonth = dtStartDate.Month.ToString();
                    if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                    string strDay = dtStartDate.Day.ToString();
                    if (strDay.Length == 1) { strDay = "0" + strDay; }

                    string strHour = dtStartDate.Hour.ToString();
                    if (strHour.Length == 1) { strHour = "0" + strHour; }

                    string strMinute = dtStartDate.Minute.ToString();
                    if (strMinute.Length == 1) { strMinute = "0" + strMinute; }


                    phyEnd = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                }

                ////if (DS.Tables[0].Rows[0].IsNull("EPP_REMARKS") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPP_REMARKS"]) != "")
                ////{
                ////    phyRemarks = Convert.ToString(DS.Tables[0].Rows[0]["EPP_REMARKS"]);

                ////    phyRemarks = RemoveSplChar(Convert.ToString(phyRemarks.Trim()));
                ////    phyRemarks = phyRemarks.Replace("\n", string.Empty);
                ////    phyRemarks = phyRemarks.Replace(Environment.NewLine, string.Empty);
                ////}
                ////else
                ////{
                //// phyRemarks = Unit + " " + UnitType + ", " + Frequency + " " + FrequencyType + ", " + phyDuration + " " + phyDurationDesc;
                //// }

                if (Unit != "" && UnitType != "" && Frequency != "" && FrequencyType != "" && phyDuration != "" && phyDurationDesc != "")
                {
                    phyRemarks = Unit + " " + UnitType + ", " + Frequency + " " + FrequencyType + ", " + phyDuration + " " + phyDurationDesc + ", From: " + Convert.ToString(DS.Tables[0].Rows[0]["EPP_START_DATELongDesc"]);
                }

                Int32 SLNo;

                if (DS.Tables[0].Rows[0].IsNull("EPP_POSITION") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPP_POSITION"]) != "")
                {
                    SLNo = Convert.ToInt32(DS.Tables[0].Rows[0]["EPP_POSITION"]);

                }


                //if (phyRemarks.Length > 20)
                //{
                //  phyRemarks =  phyRemarks.Substring(0, 20);
                //}

                OrderQtyTiming = "";

                // Qunatity^Interval^Duration^Start Date/Time^End Date/Time^Prioity
                // OrderQtyTiming = PhyQty + "^" + Frequency + "^" + phyDuration + "^" + phyStart + "^" + phyEnd + "^";
                //  OrderQtyTiming = PhyQty + "^" + Frequency + " " + FrequencyType + "^" + phyDuration + " " + phyDurationDesc + "^" + phyStart + "^" + phyEnd;//  +"^";
                OrderQtyTiming = Unit + "^" + Frequency + " " + FrequencyType + "^" + phyDuration + " " + phyDurationDesc + "^" + phyStart + "^" + phyEnd;//  +"^";

            }

        }

        void GetInventoryUserDetails(string UserID, out string UserFirstName, out  string UserLastName, out  string LicenseNo)
        {
            UserFirstName = "";
            UserLastName = "";
            LicenseNo = "";

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND  User_Status='A'  ";
            Criteria += " AND UserID='" + UserID + "'";
            DS = objCom.fnGetFieldValue("  TOP 1  * ", PHARMACY_DB_NAME + ".DBO.[USER]", Criteria, "");


            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("Name") == false && Convert.ToString(DS.Tables[0].Rows[0]["Name"]) != "")
                {
                    UserFirstName = Convert.ToString(DS.Tables[0].Rows[0]["Name"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("LastName") == false && Convert.ToString(DS.Tables[0].Rows[0]["LastName"]) != "")
                {
                    UserLastName = Convert.ToString(DS.Tables[0].Rows[0]["LastName"]);
                }
                if (DS.Tables[0].Rows[0].IsNull("MOH_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["MOH_NO"]) != "")
                {
                    LicenseNo = Convert.ToString(DS.Tables[0].Rows[0]["MOH_NO"]);
                }
            }


        }


        void GetInventoryPhyIvItem(string ItemCode, out Int32 PurConv, out string InvCode)
        {
            PurConv = 1;
            InvCode = "PCS";


            CommonBAL objCom = new CommonBAL();
            DataSet DSPhy = new DataSet();
            string Criteria = " 1=1  "; // AND item_code IS NOT NULL AND item_code <> '' 
            Criteria += " AND Item_Code = '" + ItemCode.Trim().Replace("'", "''") + "'";
            DSPhy = objCom.fnGetFieldValue("  TOP 1 Item_Code,PurConv,InvCode ", PHARMACY_DB_NAME + ".DBO.ivitem", Criteria, "Item_Code desc");

            if (DSPhy.Tables[0].Rows.Count > 0)
            {
                if (DSPhy.Tables[0].Rows[0].IsNull("PurConv") == false && Convert.ToString(DSPhy.Tables[0].Rows[0]["PurConv"]) != "")
                {
                    PurConv = Convert.ToInt32(DSPhy.Tables[0].Rows[0]["PurConv"]);
                }

                if (DSPhy.Tables[0].Rows[0].IsNull("InvCode") == false && Convert.ToString(DSPhy.Tables[0].Rows[0]["InvCode"]) != "")
                {
                    InvCode = Convert.ToString(DSPhy.Tables[0].Rows[0]["InvCode"]);


                }




            }
        }


        public Boolean PhyDispenseMessagGenerate_RDS_O13()
        {
            Boolean isError = false;
            MessageType = "RDS^O13";
            MessageEvent = "RDS-O13";
            MESSAGE_TYPE = "MEDS";

            LogFileWriting("****************** RDS-O13 " + System.DateTime.Now.ToString() + "******************");


            GetPatientDetails();
            GetPatientVisitDtls();
            BindEMRPTMaster();
            //GetPatientClass();


            if (ValidateNL7Message() == false)
            {
                isError = true;
            }


            testHl7MessageToTransmit = new StringBuilder();

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND HAA_APPROVED_QTY >0 ";

            // Criteria += " AND HAA_HAR_ID = '" + HAR_ID + "'";
            Criteria += " AND HAA_EMR_ID = '" + EMR_ID + "'";


            DS = objCom.fnGetFieldValue("*", PHARMACY_DB_NAME + ".DBO.HMS_AUTHORIZATION_ACTIVITES", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {

                MessageHeader();
                BindPatientIdentification_PID();
                BindPatientVisitAdditionalInformation_PV1();

                Criteria = " 1=1   ";


                // Criteria += " AND HAR_EMR_ID = '" + EMR_ID + "'";

                DataSet DSAuthReq = new DataSet();
                // DSAuthReq = objCom.fnGetFieldValue("TOP 1 *", PHARMACY_DB_NAME + ".DBO.HMS_AUTHORIZATION_REQUESTS", Criteria, "");

                Criteria += " AND HAR_ID = '" + Convert.ToString(DS.Tables[0].Rows[0]["HAA_HAR_ID"]) + "'";
                DSAuthReq = objCom.fnGetFieldValue("TOP 1 *", PHARMACY_DB_NAME + ".DBO.IV_ECLAIM_XML_DATA_MASTER", Criteria, "");


                string DispensingProviderID = "", DispensingProviderName = "";

                if (DSAuthReq.Tables[0].Rows.Count > 0)
                {
                    //string User_ID = Convert.ToString(DSAuthReq.Tables[0].Rows[0]["HAR_CREATED_USER"]).Trim();
                    string User_ID = Convert.ToString(DSAuthReq.Tables[0].Rows[0]["CreatedUser"]).Trim();

                    string UserFirstName, UserLastName, LicenseNo;

                    GetInventoryUserDetails(User_ID, out   UserFirstName, out   UserLastName, out   LicenseNo);

                    if (LicenseNo != "")
                    {
                        DispensingProviderID = LicenseNo;
                    }


                    if (UserFirstName.Length > 20)
                    {
                        UserFirstName = UserFirstName.Substring(0, 15);
                    }
                    if (UserLastName.Length > 20)
                    {
                        UserLastName = UserLastName.Substring(0, 15);
                    }

                    DispensingProviderName = UserLastName + "^" + UserFirstName + "^";
                }
                else
                {
                    Criteria += " AND HAR_ID = '" + Convert.ToString(DS.Tables[0].Rows[0]["HAA_HAR_ID"]) + "'";
                    DSAuthReq = objCom.fnGetFieldValue("TOP 1 *", PHARMACY_DB_NAME + ".DBO.HMS_AUTHORIZATION_REQUESTS", Criteria, "");

                    if (DSAuthReq.Tables[0].Rows.Count > 0)
                    {
                        string User_ID = Convert.ToString(DSAuthReq.Tables[0].Rows[0]["HAR_CREATED_USER"]).Trim();

                        string UserFirstName, UserLastName, LicenseNo;

                        GetInventoryUserDetails(User_ID, out   UserFirstName, out   UserLastName, out   LicenseNo);

                        if (LicenseNo != "")
                        {
                            DispensingProviderID = LicenseNo;
                        }


                        if (UserFirstName.Length > 20)
                        {
                            UserFirstName = UserFirstName.Substring(0, 15);
                        }
                        if (UserLastName.Length > 20)
                        {
                            UserLastName = UserLastName.Substring(0, 15);
                        }

                        DispensingProviderName = UserLastName + "^" + UserFirstName + "^";
                    }
                }


                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    string ActivityID = Convert.ToString(DS.Tables[0].Rows[i]["HAA_ACTIVITY_ID"]);
                    string HAA_HAR_ID = Convert.ToString(DS.Tables[0].Rows[i]["HAA_HAR_ID"]);
                    string PhyCode = Convert.ToString(DS.Tables[0].Rows[i]["HAA_SERV_CODE"]);
                    string PhyName = Convert.ToString(DS.Tables[0].Rows[i]["HAA_SERV_NAME"]);
                    string PhyQty = Convert.ToString(DS.Tables[0].Rows[i]["HAA_APPROVED_QTY"]);
                    string PhyDossage = Convert.ToString(DS.Tables[0].Rows[i]["HAA_DOSAGE"]);
                    string DispensingInstruction = Convert.ToString(DS.Tables[0].Rows[i]["HAA_INSTRUCTION"]);

                    string PaymentAmt = Convert.ToString(DS.Tables[0].Rows[i]["HAA_PAYMENT_AMOUNT"]);
                    string ItemCode = Convert.ToString(DS.Tables[0].Rows[i]["HAA_ITEM_CODE"]);

                    string ActivityStart = "";


                    DateTime dtStartDate = Convert.ToDateTime(DS.Tables[0].Rows[0]["HAA_ACTIVITY_START"]);

                    if (DS.Tables[0].Rows[0].IsNull("HAA_ACTIVITY_START") == false && Convert.ToString(DS.Tables[0].Rows[0]["HAA_ACTIVITY_START"]) != "")
                    {
                        string strMonth = dtStartDate.Month.ToString();
                        if (strMonth.Length == 1) { strMonth = "0" + strMonth; }

                        string strDay = dtStartDate.Day.ToString();
                        if (strDay.Length == 1) { strDay = "0" + strDay; }

                        string strHour = dtStartDate.Hour.ToString();
                        if (strHour.Length == 1) { strHour = "0" + strHour; }

                        string strMinute = dtStartDate.Minute.ToString();
                        if (strMinute.Length == 1) { strMinute = "0" + strMinute; }

                        // EMR_START_DATE = dtStartDate.Year.ToString() + strMonth + strDay;
                        ActivityStart = dtStartDate.Year.ToString() + strMonth + strDay + strHour + strMinute + "00";

                    }

                    Int32 SLNo;

                    if (DS.Tables[0].Rows[0].IsNull("HAA_ACTIVITY_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["HAA_ACTIVITY_ID"]) != "")
                    {
                        SLNo = Convert.ToInt32(DS.Tables[0].Rows[i]["HAA_ACTIVITY_ID"]);

                    }
                    else
                    {
                        SLNo = i + 1;
                    }




                    LogFileWriting("BindPharmacyTreatmentDispense Start ");

                    string OrderQtyTiming = "", Unit = "", UnitType = "", phyRemarks = "", phyRoute = "", phyPrescDate = "";
                    GetEMRPhyDtls(PhyCode, out OrderQtyTiming, out Unit, out  UnitType, out phyRemarks, out phyRoute, out phyPrescDate);




                    string phyAmount = "0", Strength = "", Dosage_Form = "", GenericCode = "", GenericName = "", PackageName = "", InvCode = "PCS";
                    Int32 PurConv = 1;
                    string DispenseUnit = "", DispenseUnitType = "";

                    if (PHARMACY_DB_NAME != "")
                    {
                        GetInventoryPhyDrugList(PhyCode, out   phyAmount, out Strength, out Dosage_Form, out GenericCode, out GenericName, out PackageName);

                        Strength = RemoveSplChar(Convert.ToString(Strength.Trim()));
                        PackageName = RemoveSplChar(Convert.ToString(PackageName.Trim()));


                        GetInventoryPhyIvItem(ItemCode, out  PurConv, out InvCode);
                        Decimal decAppPhyQty = Convert.ToDecimal(PhyQty);

                        DispenseUnit = (decAppPhyQty * PurConv).ToString("#0");
                        DispenseUnitType = InvCode;

                    }
                    else
                    {
                        GetPhyAmount(PhyCode, out   phyAmount, out Strength, out Dosage_Form, out GenericCode, out GenericName, out PackageName);

                        Strength = RemoveSplChar(Convert.ToString(Strength.Trim()));
                        PackageName = RemoveSplChar(Convert.ToString(PackageName.Trim()));

                        GetInventoryPhyIvItem(ItemCode, out  PurConv, out InvCode);
                        Decimal decAppPhyQty = Convert.ToDecimal(PhyQty);

                        DispenseUnit = Convert.ToString(decAppPhyQty * PurConv); ;
                        DispenseUnitType = InvCode;
                    }

                    if (PhyCode == "" || PackageName == "" || PhyQty == "" || phyRoute == "" || Unit == "" || UnitType == "" || phyRemarks == "" || DispensingProviderID == "")
                    {
                        LogFileWriting(" PhyDispenseMessagGenerate function Some details missing   EMR_ID = " + EMR_ID +
                         " PhyCode = " + PhyCode + "| PackageName = " + PackageName + "| PhyQty = " + PhyQty + "| phyRoute = " + phyRoute + "| Unit = " + Unit + "| UnitType = " + UnitType + "| Remarks = " + phyRemarks + "| DispensingProviderID = " + DispensingProviderID);

                        goto FunEnd;

                    }


                    LogFileWriting("BindOrderControl Start ");
                    BindOrderControl_ORC("NW", SLNo, HAA_HAR_ID, ConsultingDr, ConsultingDrName, phyPrescDate, OrderQtyTiming, "active", ActivityStart);

                    LogFileWriting("BindOrderControl End ");



                    //BindPharmacyTreatmentDispense_RXD(ActivityID, PhyCode, PackageName, PhyQty, PhyDossage, phyRemarks, ActivityStart, Unit, UnitType, Strength, Dosage_Form, DispensingProviderID, DispensingProviderName);//RXD

                    BindPharmacyTreatmentDispense_RXD(ActivityID, PhyCode, PackageName, PhyQty, PhyDossage, phyRemarks, ActivityStart, DispenseUnit, DispenseUnitType, Strength, Dosage_Form, DispensingProviderID, DispensingProviderName, DispensingInstruction);//RXD

                    LogFileWriting("BindPharmacyTreatmentDispense End ");

                    string MalaffiCode, MalaffiName;
                    GetRouteCode(phyRoute, out  MalaffiCode, out  MalaffiName);

                    LogFileWriting("BindPharmacyTreatmentRoute Start ");
                    if (MalaffiCode == "")
                    {
                        BindPharmacyTreatmentRoute_RXR("NA^Not Available");
                    }
                    else
                    {
                        BindPharmacyTreatmentRoute_RXR(MalaffiCode + "^" + MalaffiName);
                    }
                    LogFileWriting("BindPharmacyTreatmentRoute End ");

                }



            }
            else
            {
                goto FunEnd;
            }


            string FileName = MessageEvent + "_" + MessageControlID; //  + PROVIDER_ID + "-" + PT_ID.Replace("/", "_") + "-" + VISIT_ID + "-" + EMR_ID + '-' + strTimeStamp;
            MalaffiMessageWriting(testHl7MessageToTransmit.ToString(), MessageEvent, FileName);



            string ReturnMessage = "";
            Int32 MessageSent = 0;

            Boolean SendToMalaffi = false;
            string[] arrMessageTypes = UPLOAD_MESSAGE_TYPE_SEND_TO_MALAFFI.Split('|');// hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrMessageTypes.Length; intSegType++)
            {
                if (arrMessageTypes[intSegType] == MessageEvent) //MESSAGE_TYPE
                {
                    SendToMalaffi = true;
                }
            }

            if (SendToMalaffi == true)
            {
                SendHL7Message(testHl7MessageToTransmit.ToString(), MESSAGE_TYPE, out  ReturnMessage, out  MessageSent);
            }
            else
            {
                MessageSent = 3;
            }


            LogFileWriting("HL7File Created file name is  " + FileName);


            RETURN_MESSAGE = ReturnMessage.Replace("'", "");


            objCom = new CommonBAL();
            Criteria = " HMHM_UPLOAD_STATUS ='PENDING' ";
            Criteria += " AND HMHM_BRANCH_ID='" + BRANCH_ID + "' AND HMHM_PT_ID='" + PT_ID + "' AND HMHM_EMR_ID='" + EMR_ID + "'";
            Criteria += " AND HMHM_MESSAGE_TYPE='" + MESSAGE_TYPE + "' and HMHM_MESSAGE_CODE='" + MessageEvent + "'";

            string FieldNameWithValues = "HMHM_MESSAGE_ID='" + MessageControlID + "', HMHM_FILE_NAME='" + FileName + "' ";


            if (MessageSent == 1)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'UPLOADED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            if (MessageSent == 2)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'ERROR' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            if (MessageSent == 3)
            {
                FieldNameWithValues += " ,HMHM_UPLOAD_STATUS = 'FILE_CREATED' ";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

            }

            string FieldNameWithValues1 = "HMHM_RETURN_MESSAGE='" + RETURN_MESSAGE + "'";
            objCom.fnUpdateTableData(FieldNameWithValues1, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

        FunEnd: ;
            if (isError == true)
            {
                return false;
            }

            return true;

        }





        public void MalaffiHL7MessageMasterAdd()
        {

            IDictionary<string, string> Param = new Dictionary<string, string>();
            Param.Add("BRANCH_ID", BRANCH_ID);
            Param.Add("PT_ID", PT_ID);
            Param.Add("VISIT_ID", VISIT_ID);
            Param.Add("EMR_ID", EMR_ID);
            Param.Add("MessageID", MessageID);
            Param.Add("FileName", HL7FileName);
            Param.Add("MessageCode", MessageCode);
            Param.Add("MessageDesc", MessageDesc);
            Param.Add("MESSAGE_TYPE", MESSAGE_TYPE);
            Param.Add("RETURN_MESSAGE", RETURN_MESSAGE);
            Param.Add("UPLOAD_STATUS", UPLOAD_STATUS);
            Param.Add("UserID", UserID);
            objDB.ExecuteNonQuery("HMS_SP_MalaffiHL7MessageMasterAdd", Param);
        }
        #endregion
    }
}
